<?php

class Comm_class
{
    private $CI;
 //构造方法初始化
    function __construct($config = array() ){
		$this->CI =& get_instance();

    }


	function verify_code($timestamp=""){

		$imgwidth =76; //图片宽度
		$imgheight =32; //图片高度
		$codelen =4; //验证码长度
		$fontsize =14; //字体大小
		$charset = 'abcdefghkmnprstuvwxyzABCDEFGHKMNPRSTUVWXYZ23456789';
		$font = $_SERVER['DOCUMENT_ROOT'].'/assets/css/elephant.ttf';

		$im=imagecreatetruecolor($imgwidth,$imgheight);

		$while=imageColorAllocate($im,255,255,255);
		imagefill($im,0,0,$while); //填充图像

		//取得字符串
		$authstr='';
		$_len = strlen($charset)-1;
		for ($i=0;$i<$codelen;$i++) {
		 $authstr .= $charset[mt_rand(0,$_len)];
		}

		$_SESSION['scode']=strtolower($authstr);//全部转为小写，主要是为了不区分大小写

		//随机画点,已经改为划星星了
		for ($i=0;$i<$imgwidth;$i++){
		    $randcolor=imageColorallocate($im,mt_rand(200,255),mt_rand(200,255),mt_rand(200,255));
		 imagestring($im,mt_rand(1,5), mt_rand(0,$imgwidth),mt_rand(0,$imgheight), '*',$randcolor);
		    //imagesetpixel($im,mt_rand(0,$imgwidth),mt_rand(0,$imgheight),$randcolor);
		}
		/*
		//随机画线,线条数量=字符数量（随便）
		for($i=0;$i<$codelen;$i++)
		{
		 $randcolor=imagecolorallocate($im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
		 imageline($im,0,mt_rand(0,$imgheight),$imgwidth,mt_rand(0,$imgheight),$randcolor);
		}
		*/

		$_x=intval($imgwidth/$codelen); //计算字符距离
		$_y=intval($imgheight*0.7); //字符显示在图片70%的位置
		for($i=0;$i<strlen($authstr);$i++){

		 $randcolor=imagecolorallocate($im,mt_rand(0,150),mt_rand(0,150),mt_rand(0,150));
		 //imagestring($im,5,$j,5,$imgstr[$i],$color3);
		 // imagettftext ( resource $image , float $size , float $angle , int $x , int $y , int $color , string $fontfile , string $text )
		 imagettftext($im,$fontsize,mt_rand(-30,30),$i*$_x+3,$_y,$randcolor,$font,$authstr[$i]);

		}

		//生成图像
		header('Content-type: image/png');
		imagePNG($im);
		imageDestroy($im);

	}

   /***图像裁剪/
    *
    */
	function sliceBanner($UserName){
		$x = (int)$_POST['x'];
		$y = (int)$_POST['y'];
		$w = (int)$_POST['w'];
		$h = (int)$_POST['h'];
		$pic = $_POST['src'];

		//剪切后小图片的名字
		$str = explode(".",$pic);//图片的格式
		$type = $str[1]; //图片的格式
		$filename = $UserName."_".date("YmdHis").".". $type; //重新生成图片的名字
		$uploadBanner = $pic;
		$sliceBanner = "upload/face/".$filename;//剪切后的图片存放的位置

		//创建图片
		$src_pic = getImageHander($uploadBanner);
		$dst_pic = imagecreatetruecolor($w, $h);
		imagecopyresampled($dst_pic,$src_pic,0,0,$x,$y,$w,$h,$w,$h);
		imagejpeg($dst_pic, $sliceBanner);
		imagedestroy($src_pic);
		imagedestroy($dst_pic);

		//删除已上传未裁切的图片
		if(file_exists($uploadBanner)) {
			unlink($uploadBanner);
		}
		//返回新图片的位置
		return $sliceBanner;
	}
	//初始化图片
	function getImageHander ($url) {
		$size=@getimagesize($url);
		switch($size['mime']){
			case 'image/jpeg': $im = imagecreatefromjpeg($url);break;
			case 'image/gif' : $im = imagecreatefromgif($url);break;
			case 'image/png' : $im = imagecreatefrompng($url);break;
			default: $im=false;break;
		}
		return $im;
	}


}

?>