<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/third_party/wx/wechat.class.php');
class MY_Controller extends CI_Controller {

    public $uid=1;
    public $department_id=1;

    public function __construct(){
         parent::__construct();
    }

     /**
	 * @功能  功能权限访问验证
	 * @param $rule 数组，（空,验证当前控制器/方法，格式 array("add"=>"user/add","edit"="user/edit")
	 * @return 返回 true,flase
	 */
     public function check_access($rule = array()){
       $result =  array();
       if(empty($rule)) {
       		$ac 				= $this->router->class."/".$this->router->method;
       		$result  			= $this->auth->check($this->uid,$ac);
       }else{
       	   foreach($rule  as $key => $row){
       	     	$result[$key]  	= $this->auth->check($this->uid,$row);
       	   }
       }
       return $result;
    }


    /**
	 * @功能   验证是否有访问权限
	 * @参数   $ac格式："控制器/方法"
	 * @return 没有权限，返回"非法访问"
	 */
   public function check_access_ac($ac=""){
       	if(!$this->auth->check($this->uid,$ac)){
       		echo "非法访问";
   	    	die;
       	}
    }


     /**
	 * @功能  验证当前控制器是否有访问权限
	 * @param
	 * @return 返回 true,flase
	 */
   public function check_access_c(){
       	$ac 		= $this->router->class;
       	$result  	= $this->auth->check($this->uid,$ac);
        return $result;
    }



 	public function check_app_access()
    {
    	$this->data 	= $this->input->post(NULL,TRUE);
    	if(!isset($this->data["uid"]) || !isset($this->data["security"]) || !isset($this->data["device_md5"])) {
    		$this->error("参数错误",1);
    	}
    	$condition	= array(
							"uid" 			=> $this->data["uid"],
							"device_md5" 	=> $this->data["device_md5"],
							);


        $result =  $this->table_model->get_array_one("login_device",$condition);
	    if(!empty($result) && $result["security"] == $this->data["security"]){

	    }else{
	    	$this->error("非法访问",2);
	    }

    }

     /**
	 * @功能  表单输入后台验证
	 * @param $validation 验证条件，数组
	 * @param
	 * @return 出错有返回值
	 */

   public function check_validation($validation)
    {

        $this->data = $this->input->post();
        $this->form_validation->set_data($this->data);
        $this->request = new stdClass();
        $fileds = array();
        foreach ($validation as $field => $value) {
            $fileds[] = $field;
            $rules = $value['rules'];
            $label = $field;
            $this->form_validation->set_rules($field, $label, $rules);
        }


        $this->form_validation->set_message('required', '{field} 不能为空.');
        $this->form_validation->set_message('min_length', '{field}  最少 {param}字符.');
        $this->form_validation->set_message('max_length', '{field}  最多 {param}字符.');
        $this->form_validation->set_message('valid_email', '{field}  邮箱格式不对.');
        $this->form_validation->set_message('integer', '{field}  只能输入整数.');
        $this->form_validation->set_message('numeric', '{field}  只能输入数字.');
        $this->form_validation->set_message('regex_match', '{field} 手机格式不对.');
		$temp_data  = array();
		foreach($this->data  as $key=>$row){
			$temp_data[$key]   = $this->security->xss_clean(trim($row));
		}
        $this->data = $temp_data;

        if (!$this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $this->error($errors);
        }
    }

    public function success($msg = "", $result = "")
    {
        $this->result['status'] 	= "1";
        $this->result['error_code'] = "0";

        if ($msg != '') {
            $this->result['info'] = $msg;
        }

        if (is_array($result)) {
            $this->result['result'] = $result;
        }
        $this->show_result();
    }

    public function error($error, $error_code = -1)
    {
        $this->result['status'] = "0";
        $this->result['error_code'] = $error_code;
        if (is_array($error)) {
            $this->result['info'] = strip_tags(implode($error, "\n"));
        } else {
            $this->result['info'] = strip_tags($error);
        }

        $this->show_result();
    }

   public function show_result()
    {
        $jsonStr = json_encode($this->result);
        echo $jsonStr;
        exit(0);
    }


   /**
     * 信息提示页面
     * @$content 要提示的文字
     * @$continue 即将跳往的页面，返回用“-1”
     * @$status 提示状态，值为0,1或其它，0为错误提示(红色)，1为正常提示（绿色）
     * @author theNbsp
     */
    function showmessage($content, $continue, $status = 1) {
        $continue = ($continue == 'back') ? 'history.back()' : 'window.location="'.site_url($continue).'"';
        $waits = ($status == 0) ? 'setTimeout("thisUrl()", 3000)' : 'setTimeout("thisUrl()", 3000)';
        $status = ($status == 0) ? 'color:#FF0000' : 'color:#009900';
        $string = "<div class='box'>\n<h5>提示信息</h5>\n<p class='content'>".$content."</p>\n<p class='clickUrl'>
        如果浏览器没有自动跳转，请 <a href='javascript:;' onClick='".$continue."'>点击这里</a></p>\n</div>";
        $style = "<style>\nbody,h5,p,a{font:12px Verdana,Tahoma;text-align:center;text-decoration:none;margin:0;padding:0;}
        \nh5{color:#555;font-size:24px;height:28px;text-align:center;line-height:28px;font-weight:bold;background:
        #EEE;margin:1px;padding:0 10px;} \n.box{width:480px;border:1px solid #DDD;margin:120px auto;-moz-box-shadow:
        3px 4px 5px #EEE;-webkit-box-shadow:3px 4px 5px #EEE;}\n.content{".$status.";font-size:24px;font-weight:bold;
        line-height:24px;padding:30px 10px;}\n.clickUrl{color:#888;margin-bottom:15px;padding:0 10px;}\n</style>";
        $html = "<!DOCTYPE html>\n<html>\n<head>\n<title>提示信息</title>\n<meta http-equiv='Content-Type' content='text/html;
        charset=utf-8'/>\n".$style."\n<script type='text/javascript'>\nfunction thisUrl(){".$continue."}\n".$waits."\n
        </script>\n</head>\n<body>\n".$string."\n</body>\n</html>";
        exit($html);
    }

    public function jsuccess($msg="",$url="/"){

    	 $data['msg']		= $msg;
    	 $data['jumpurl']	= urldecode($url);
    	 $this->load->view('wx/common/msg',$data);

    }

     public function security($uid=""){
    	  $site_key = get_my_config('myconfig/site_config','site_key');
    	  $result 	= md5($site_key.$uid.time().rand(100000,999999));
          return  $result;
    }
}



class Admin_Controller extends MY_Controller
{

	function __construct()
    {
         parent::__construct();
          $this->user_info  	= $this->session->userdata('user_info');

//          p($this->user_info);
         if(isset($this->user_info) && !empty($this->user_info)){
         	$this->department_id 	= $this->user_info["department_id"];
         	$this->account 	    = $this->user_info["account"];
         	$this->uid 	    	= $this->user_info["id"];
         	$this->group_id    	= $this->user_info["group_id"];
    	 }else{
			 echo "<script>";
			 echo 'parent.location.href="'.site_url('/admin').'";';
			 echo "</script>";
         }
    }
}
