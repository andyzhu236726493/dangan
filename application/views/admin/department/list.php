<script>
$(function(){

	var url  ="<?php echo site_url("/admin/department/get_department_tree_limit_json")?>";
	var ht= $(window).height()-16;

	$('#tt').datagrid({
		    height:ht,
            url:url,
			toolbar: "#tb",
			fit:'true',
			striped:true,
			method: 'post',
			rownumbers: true,
			idField: 'id',
			singleSelect:true,
			fitColumns: true,
			pagination: true,
			pageSize: 20,
			pageList:[20,50,100,200],
			columns:[[
				{field:'name',title:'名称',width:$(this).width() * 0.1,align:'center',editor:'text'},
				{field:'edit',title:'编辑',width:40,align:'center',fixed:'true',
					formatter:function(value,row,index){
						 	var a = '<a   class="myedit" href="javascript:void(0)" onclick="edit('+row.id+')"></a>';
						 return a;
					}
				},{field:'del',title:'删除',width:40,align:'center',fixed:'true',
					formatter:function(value,row,index){
						   		var b = '<a   class="mydel" href="javascript:void(0)"  onClick="del('+row.id+',\''+row.name+'\')"></a>';
						 return b;
					}
				}
				]],
				onLoadSuccess:function(data){
						$('.myedit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
						$('.mydel').linkbutton({plain:true,iconCls:'icon-mydelete',height:20});
				},
				onDblClickRow: function(index,row){
					 var url  = "<?php echo site_url('/admin/user/detail') ?>/"+row.id;
					parent.layer.open({
				        type: 2,
				        title: '管理员详细',
				        maxmin: true,
				        shadeClose: false,  // 点击空白页不关闭窗口
				        area : ['510px' , '410px'],
				        shade: [0.2,'#fff'],
				        content: url
				    });
				}
	   });


});



function add() {
	var url  = "<?php echo site_url('/admin/department/add') ?>";
	parent.layer.open({
        type: 2,
        title: '添加公证处',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['610px' , '450px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function edit(id) {
	var url  = "<?php echo site_url('/admin/department/edit') ?>/"+id;
	parent.layer.open({
        type: 2,
        title: '修改公证处',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['610px' , '450px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function del(id,name){

		$.messager.confirm('确认提示','你确定要删除名称为 '+name+' 的记录吗？',function(r){
			if (r){
				var url="<?php echo site_url('/admin/department/del') ?>";
				$.ajax({
				   type: "POST",
				   url:url ,
				   data: "id="+id,
				   dataType:"json",
				   success: function(msg){
					   layer.msg(msg.info, {
										    icon: 1,  //1 成功 ,0 失败
										    time: 2000
										  });
				   }
				});

			}
		});
}

function my_search() {
	var url = "<?php echo site_url('/home/user/search') ?>/?"+$("#form_search").serialize();
	$('#tt').datagrid({
			url:url,
			pageNumber:1
	}).datagrid("reload");

}


</script>
<table id="tt"></table>
<div id="tb" style="padding:10px;">
	<table width="100%" border="0" cellspacing="3" cellpadding="0">
	  <tr>
	     <td width="85%" align="left">
		  	<form  id="form_search" >
		  	名称：<input class="easyui-textbox"  type="text" name="name"  id  ="name" data-options="" value=""></input>

			&nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-mysearch'"    onclick="my_search()" >搜       索</a>
			</form>
		  </td>
		  <td width="200" align="right">

		  		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="add()">添加公证处</a>

		  </td>

	  </tr>
	</table>
</div>

</body>
</html>