<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>

	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/default/easyui.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">
	<script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/assets/js/comm.js?t=<?php echo time()?>"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>
	<script type="text/javascript" src="/assets/js/ajaxfileupload.js"></script>

</head>
<body>