 <!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/gray/easyui.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">
    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
   	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
   	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
   	<script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>
   	<script type="text/javascript" src="/assets/js/comm.js?t=<?php echo rand(10000,99999)?>"></script>
	<link type="text/css" rel="stylesheet" href="/assets/css/admin/css.css?t=<?php echo rand(10000,99999)?>">

	<style>
	.tabs li.tabs-selected a.tabs-inner {
	    font-weight: bold;
	    outline: none;
	    color: #b70808;
	}
	</style>
</head>
<body class="easyui-layout" style="overflow-y: hidden;padding:7px;"   scroll="no">