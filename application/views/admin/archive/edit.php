	<script type="text/javascript" src="/assets/plugins/workerman/js/web_socket.js"></script>
	<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
    <p style="margin:20px">
	<form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
		<table cellpadding="5" border="0">
			 <tr>
				<td align="right" style="width:150px" >公证书字号:</td>

				<td>
		   			  <input type="hidden"  name="id"    value="<?php echo $info['id'] ?>"  >

		   			   <input class="easyui-textbox easyui-validatebox"  type="text" id="word_no" name="word_no"  style="width:320px"  data-options="required:true,novalidate:true"  missingMessage="请输入字号"    value="<?php echo $info['word_no'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?> >
                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>
    		<tr >
			<td align="right" >类&emsp;&emsp;别:</td>
			<td align="left" width="400">

				<input  class="easyui-combobox" id="category_id"  name="category_id" style="width:320px" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>
						data-options="
										url:'<?php echo site_url('/admin/category/get_category_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $info['category_id'] ?>',
										panelHeight:'auto',
										editable:false,
										required:true,
										novalidate:true
									"
				>
                <span style="color:#ff2e2e">*</span>
             </td>
	    	</tr>
	    	<tr >
			<td align="right" >公证事项:</td>
			<td align="left" width="400">
				<input class="easyui-textbox easyui-validatebox org ishidden"  name="notary_matter_name"  id="notary_matter_name"  style="width:320px" type="text" data-options="required:true,novalidate:true" missingMessage="请输入公证事项"  value="<?php echo $info['notary_matter_name'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>

				<span style="color:#ff2e2e">*</span>
<!--				<input  class="easyui-combobox" id="notary_matter_id"  name="notary_matter_id" style="width:320px"  --><?php //if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>
<!--						data-options="-->
<!--										url:'--><?php //echo site_url('/admin/notary_matter/get_notary_matter_json') ?><!--',-->
<!--										method:'get',-->
<!--										valueField:'id',-->
<!--										textField:'text',-->
<!--										value:'--><?php //echo $info['notary_matter_id'] ?><!--',-->
<!--										panelHeight:'auto',-->
<!--										editable:false,-->
<!--										required:true,-->
<!--										novalidate:true,-->
<!--										panelHeight:'auto',-->
<!--										panelMaxHeight:200-->
<!--									"-->
<!--				>-->
<!--                <span style="color:#ff2e2e">*</span>-->
             </td>
	    	</tr>
	    	<tr>
				<td align="right">当事人类型:</td>
				<td>
		   			  <input   type="radio" name="litigant_type"    value="1" <?php if($info['litigant_type'] ==1) echo "checked" ?>  onclick="litigant_select(1)"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>自然人
		   			  <input   type="radio" name="litigant_type"    value="2" <?php if($info['litigant_type'] ==2) echo "checked" ?> onclick="litigant_select(2)"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>法人或其它组织机构
					  <span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
	    	<tr class="org ishidden">
				<td align="right">组织机构名称:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox org ishidden"  name="litigant_org"  id="litigant_org"  style="width:320px" type="text" data-options="required:true,novalidate:true"
  					 				  missingMessage="请输入组织机构名称"  value="<?php echo $info['litigant_org'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>

                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>
	    	<tr >
				<td align="right">当事人姓名:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox"  name="litigant_name"  id="litigant_name"  style="width:320px" type="text" data-options="required:true,novalidate:true"
  					 				  missingMessage="请输入当事人姓名"  value="<?php echo $info['litigant_name'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?> >

                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>
	    	 <tr>

	    		<td align="right">当事人身份证号码:</td>
	    		 <td>  <input class="easyui-textbox easyui-validatebox"  type="text" name="litigant_card" id="litigant_card"  style="width:320px"
	    		 	data-options="required:true,novalidate:true"  missingMessage="请输入人身份证"     value="<?php echo $info['litigant_card'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?> >
                <span style="color:#ff2e2e">*</span>
                </td>
	    	</tr>

	    	<tr>
				<td align="right">当事人性别:</td>
				<td>
		   			  <input   type="radio" name="litigant_sex"    value="1" <?php if($info['litigant_sex']==1) echo "checked" ?> <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>男
		   			  <input   type="radio" name="litigant_sex"    value="0" <?php if($info['litigant_sex']==0) echo "checked" ?> <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>> 女
<span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
	    	<tr>
				<td align="right">当事人出生日期:</td>
				<td>
	    	   			  <input class="easyui-datebox" name="litigant_date"  id="litigant_date"   data-options="required:true,novalidate:true"  style="width:320px"   value="<?php echo $info['litigant_date'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
	            <span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
			<tr >
				<td align="right">承办人姓名:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox"  name="undertaker"  id="undertaker" type="text" style="width:320px"
  					 data-options="required:true,novalidate:true"   missingMessage="请输入承办人姓名"   value="<?php echo $info['undertaker'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>
	    	<tr >
				<td align="right">受理日期:</td>
				<td align="left">
  					   <input class="easyui-datebox" name="receive_time"  id="receive_time"   data-options="required:true,novalidate:true"  style="width:320px"   value="<?php echo $info['receive_time'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?> >
<span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
	    	<tr >
				<td align="right">归档日期:</td>
				<td align="left">
	                   <input class="easyui-datebox" name="archive_time"  id="archive_time"   data-options="required:true,novalidate:true"  style="width:320px"   value="<?php echo $info['archive_time'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
	             <span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>

	    	<tr>
	    		<td align="right">公&ensp;证&ensp;员:</td>
	    		<td>
	               <input class="easyui-textbox easyui-validatebox"  name="greffier"  id="greffier" type="text" style="width:320px"  data-options="required:true,novalidate:true"   value="<?php echo $info['greffier'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e">*</span>
			  </td>
	    	</tr>
	    	<tr>
	    		<td align="right">公证事由:</td>
	    		<td>
	               <input class="easyui-textbox easyui-validatebox"  name="notarial_reason"  id="notarial_reason" type="text" style="width:320px"  data-options="required:true,novalidate:true"   value="<?php echo $info['notarial_reason'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e">*</span>
			  </td>
	    	</tr>
	    	<tr>
	    		<td align="right">所属公证处:</td>
	    		<td>
	              <input  class="easyui-combobox" id="department_id"  name="department_id" style="width:320px" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>
						data-options="
										url:'<?php echo site_url('/admin/department/get_all_department_list_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $info['department_id'] ?>',
										panelHeight:'auto',
										editable:false,
										required:true,
										novalidate:true
									"
				>
                <span style="color:#ff2e2e">*</span>
			  </td>
	    	</tr>
	    		<tr >
				<td align="right">盒号:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox"  name="box_number"  id="box_number" type="text"  style="width:320px"
  					        data-options="required:true,novalidate:true"   value="<?php echo $info['box_number'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
	    		    	<tr >
				<td align="right">卷号:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox"  name="volume_number"  id="volume_number" type="text" style="width:320px"
  					     data-options="required:true,novalidate:true"   value="<?php echo $info['volume_number'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                	<span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>

	    	<tr >
				<td align="right">办结日期:</td>
				<td align="left">
  					  <input class="easyui-datebox" name="over_time"  id="over_time"   data-options="required:true,showSeconds:false,novalidate:true"  style="width:320px"   value="<?php echo $info['over_time'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?> >
	             <span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>
	    	<tr >
				<td align="right">保存期限:</td>
				<td align="left">
  					 <input  class="easyui-combobox" id="storage_time_id"  name="storage_time_id" style="width:320px" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>
						data-options="
										url:'<?php echo site_url('/admin/archive/get_storage_time_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $info['storage_time_id'] ?>',
										panelHeight:'auto',
										editable:false,
										required:true,
										novalidate:true
									"
						><span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>

			 <tr>
				<td></td>
				<td style="text-align:center;padding:15px">
		   				<span id="msg"></span>
	             </td>
	    	</tr>
		    <tr style="<?php if($is_readonly == '1'){ echo 'display:none;';} ?>">
				<td  colspan="2" style="text-align:center;padding:5px">
	   	   				<a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">提交</a>
	             </td>
	    	</tr>
	    	 <tr>
				<td></td>
				<td style="text-align:center;padding:5px">
		   				<span id="msg"></span>

	             </td>
	    	</tr>

		</table>
   </form>

	<script type="text/javascript">

	$(function(){

	<?php if($info["litigant_type"] ==2){  ?>
			litigant_select(2);
	<?php } ?>

   });

	function submitForm(){
		$('#form1').form('submit',{
			onSubmit:function(){
				var arr=new Array();
				arr.push(new Array('word_no','textbox'));
				arr.push(new Array('category_id','combobox'));
				arr.push(new Array('notary_matter_name','textbox'));
				arr.push(new Array('litigant_name','textbox'));
				arr.push(new Array('litigant_card','textbox'));
				arr.push(new Array('litigant_date','datetimebox'));
				arr.push(new Array('undertaker','textbox'));
				arr.push(new Array('receive_time','datetimebox'));
				arr.push(new Array('archive_time','datetimebox'));
				arr.push(new Array('greffier','textbox'));
				arr.push(new Array('notarial_reason','textbox'));
				arr.push(new Array('department_id','combobox'));
				arr.push(new Array('box_number','textbox'));
				arr.push(new Array('volume_number','textbox'));
				arr.push(new Array('over_time','datetimebox'));
				arr.push(new Array('storage_time_id','combobox'));

				if(validate_arr(arr)){
					var url = "<?php echo $action_url ?>";
					$.ajax({
						type: "POST",
						url:url,
						dataType:"json",
						data:$("#form1").serialize(),
						success: function(rt){
							parent.layer.msg(rt.msg);
							if(rt.status=='ok'){
								var index = parent.layer.getFrameIndex(window.name);
							   	$("#iframe_src", parent.document)[0].contentWindow.reload();
								parent.layer.close(index);
							}
						}
					});
				}
			}
		});
	}

  function litigant_select(type){
	  	if(type ==1){
		 	$(".org").each(function(){
	    		$(this).addClass("ishidden");
	    		$(this).attr("disabled","disabled");
	  		});
		}else{
	  	  	 $(".org").each(function(){
	    		$(this).removeClass("ishidden");
	    		$(this).removeAttr("disabled");
	  		});
	  	}
  }

</script>

</body>
</html>