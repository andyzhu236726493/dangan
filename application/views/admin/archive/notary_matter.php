<script>
    $(function(){
        var ht= $(window).height()-16;

        $('#tt').datagrid({
            height:ht,
            data:jQuery.parseJSON('<?php echo $notary_matter;?>'),
            striped:true,
            method: 'post',
            rownumbers: true,
            idField: 'id',
            singleSelect:true,
            fitColumns: true,
            columns:[[
                {field:'name',title:'公证事由',width:$(this).width() * 0.1,align:'center',editor:'text'},
                {field:'total',title:'档案数',width:$(this).width() * 0.05,align:'center',editor:'text'},
            ]]
        });
    });
</script>
<table id="tt"></table>