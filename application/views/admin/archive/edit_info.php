<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
<style>
.ishidden{
display:none;
}

</style>


<p style="margin:20px">
<form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
<input   type="hidden"  name="id" id="id"  value="<?php  echo $id ?>" >
<table id="mytable" cellpadding="5" border="0">
<tr><td align="right" >
<input name="person_select" 		value="1" type="radio" <?php if($info["related_type"] ==1) echo "checked" ?> onclick="myselect(1)" />本人
<input name="person_select" 		value="2" type="radio" <?php if($info["related_type"] ==2) echo "checked" ?> onclick="myselect(2)" />相关人
</td></tr>
<tr class="person ishidden">
<td align="right" >与当事人关系:</td><td>
<input  class="easyui-combobox class="person ishidden"" id="person_relation_id"  name="person_relation_id" style="width:200px"
						data-options="
										url:'<?php echo site_url('admin/archive/get_person_relation_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $info["person_relation_id"] ?>',
										panelHeight:'auto',
										editable:false,
										required:true
									"
				><span style="color:#ff2e2e">*</span>
</td></tr>
<?php
foreach($info["content"]["person"]  as $key=>$val){

			echo '<tr   class="person ishidden" ><td align="right" >'.$key.':</td><td><input  class="person ishidden" name="person|'.$key.'"  value="'.$val.'" type ="text" ></td></tr>';
}
?>


<tr   class="person ishidden" ><td align="right" >是否在世:</td><td align="right" >
<input class="person ishidden" name="person_bealive" 		value="1" type="radio"  <?php if($info["related_status"] ==1) echo "checked" ?>  />是
<input class="person ishidden" name="person_bealive" 		value="0" type="radio"  <?php if($info["related_status"] ==2) echo "checked" ?> />否
</td></tr>
</table>

<table id="mytable" cellpadding="5" border="0">
<tr><td align="right"  >
<?php
$i	=	0;
foreach($notary_matter_para  as $row) {
	if($row["single_select"] ==0){
		if($i == 0){
			echo '<input name="muti_select" 		value="'.$row["id"].'" type="checkbox" checked onclick="mutiselect(this,'.$row["id"].')"/>'.$row["name"];
		}else{
			echo '<input name="muti_select" 		value="'.$row["id"].'" type="checkbox" onclick="mutiselect(this,'.$row["id"].')"/>'.$row["name"];
		}
		$i++;
	}

}
?>
</td></tr>
<?php
$i	=	0;
foreach($notary_matter_para  as $row) {
	if($row["single_select"] !=1){

		$para = array();
			if($row["field"] !=""){
				$para = unserialize($row["field"]);
			}
			foreach($para  as $arr){
					if($i ==0)  {
						echo '<tr id="m'.$row["id"].'"  class="m'.$row["id"].'"><td align="right" >'.$arr["text"].':</td><td><input  class="m'.$row["id"].'" name="'.$row["name"].'|'.$arr["text"].'" type ="text"></td></tr>';
					}else{
						echo '<tr  id="m'.$row["id"].'" class="m'.$row["id"].' ishidden"><td align="right" >'.$arr["text"].':</td><td><input  class="m'.$row["id"].' ishidden" name="'.$row["name"].'|'.$arr["text"].'" type ="text" disabled="disabled"></td></tr>';
					}

			}
			if($notary_matter["is_person"] ==0){
				foreach($person  as $key=>$val){
					if($i ==0)  {
						echo '<tr id="'.$row["id"].'"  class=""><td align="right" >'.$val.':</td><td><input  class="m'.$row["id"].'" name="'.$row["name"].'|'.$val.'" type ="text" ></td></tr>';
					}else{
						echo '<tr id="'.$row["id"].'"  class=" ishidden" ><td align="right" >'.$val.':</td><td><input  class="m'.$row["id"].' ishidden" name="'.$row["name"].'|'.$val.'" type ="text"  disabled="disabled"></td></tr>';
					}

				}
			}

		$i++;
	}
}
?>

</table>


</form>

<table id="mytable" cellpadding="5" border="0">
<tr><td style="width:300px">
<a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">添 加</a>
</td></tr>
</table>
<script>

$(function(){

<?php if($info["related_type"] ==2){  ?>
 myselect(2);
<?php } ?>

});

<?php foreach($info_value as $key=>$val) {?>
   $("input[name='<?php echo $key ?>']").attr("value","<?php echo $val?>");
<?php }?>
function myselect(type) {
		if(type ==1){
		 	$(".person").each(function(){
	    		$(this).addClass("ishidden");
	    		$(this).attr("disabled","disabled");
	  		});
		}else{
	  	  	 $(".person").each(function(){
	    		$(this).removeClass("ishidden");
	    		$(this).removeAttr("disabled");
	  		});
	  	}


}

function mutiselect(obj,id) {

	$(".m"+id).toggle();
    if(!$(obj).is(":checked")){
	    $(".m"+id).each(function(){
	    		$(this).attr("disabled","disabled");
	  	});
    }else{
    	 $(".m"+id).each(function(){
	    		$(this).removeAttr("disabled");
	  	});
    }
}

	function submitForm(){

       					$('#form1').form('submit',{
							onSubmit:function(){
									var url = "<?php echo $action_url ?>";
									var arr	= new Array();
						        	//arr.push(new Array('person_relation_id','combobox'));
							        //if(validate_arr(arr)){
									     $.ajax({
											   type: "POST",
											   url:url,
											   dataType:"json",
											   data:$("#form1").serialize(),
											   success: function(msg){
												  var index = parent.layer.getFrameIndex(window.name);
												  parent.layer.msg(msg.info, {
														    icon: 1,  //1 成功 ,0 失败
														    time: 2000　//２　秒
														});
												  $("#iframe_src", parent.document)[0].contentWindow.info_reload();
												  parent.layer.close(index);
											   }
										});
							        //}


							}
					 	});


		}

</script>