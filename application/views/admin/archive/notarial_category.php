<script src="/assets/js/echarts.min.js"></script>
<style>
    html,body{
        width:100%;
        height:100%;
    }
</style>
<div id="main" style="width: 90%;height:90%;"></div>
<script>
    var myChart = echarts.init(document.getElementById('main'));
    var json=jQuery.parseJSON('<?php echo $category;?>');
    var data1=new Array();
    var data2=new Array();
    for(var i =0;i<json.length;i++){
        data1.push(json[i]['category']);
        data2.push({name:json[i]['category'],value:json[i]['total']});
    }
    var option = {
        title : {
            text: '公证类别分析',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: data1
        },
        series : [
            {
                name: '公证类别',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:data2,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    myChart.setOption(option);
</script>