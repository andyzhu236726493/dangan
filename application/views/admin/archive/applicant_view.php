<!DOCTYPE html>
<html>
<!--<head>-->
<!--    <meta charset="UTF-8">-->
<!--    <title></title>-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/default/easyui.css">-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/icon.css">-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/color.css">-->
<!--    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>-->
<!--    <script type="text/javascript" src="/assets/js/comm.js?t=--><?php //echo rand(10000,99999)?><!--"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>-->
<!--</head>-->
<body class="easyui-layout" style="overflow-y: hidden;"   scroll="no">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'west'" style="width:300px;padding:7px;height:100%">
        选择:<input title="全选/反选" type="checkbox" onClick="treeChecked(this)"/>全选/全不选
        <ul id="mytree" style="margin-top:5px;" class="easyui-tree" data-options="animate:true,checkbox:true"></ul>
    </div>
    <div data-options="region:'center'" style="width:400px;overflow: hidden;padding:5px;text-align: center">
        <br>
        <span style="font-size:16px;">申请查阅理由:</span>
        <br><br>
        <input id="reason" class="easyui-textbox" label="申请查阅理由:" labelPosition="top" multiline="true" value="<?php echo $reason?>" style="width:100%;height:300px;margin-bottom:20px;">
        <br><br>
        <a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:50%" onclick="sub()">提交申请</a>
    </div>
</div>
<script>
    var tree_data='<?php echo $tree;?>';
    var archive_id="<?php echo $archive_id?>";
    $(function(){
//        parent.layer.style(parent.layer.index,{
////            width:'100%',
//        });
        parent.layer.title('档案查阅申请', parent.layer.index);
//        var msg="<?php //echo $msg?>//";
//        if(msg!=''){
//            alert(msg);
//        }
    });

    $('#mytree').tree({
        data:JSON.parse(tree_data),
        onClick: function(node){
            node.checked?$('#mytree').tree('uncheck',node.target):$('#mytree').tree('check',node.target);
        }
    });

    function treeChecked(selected) {
        if(selected.checked){
            //全部选中
            var nodes = $('#mytree').tree('getChecked', 'unchecked');
            var s='check';
        }else{
            //全部不选中
            var nodes = $('#mytree').tree('getChecked','checked');
            var s='uncheck';
        }
        for(var i=0;i<nodes.length;i++){
            $('#mytree').tree(s,nodes[i].target)
        }
    }

    /*
    * 提交申请
    * */
    function sub(){
        var nodes = $('#mytree').tree('getChecked','checked');
        if(nodes.length==0){
            alert('您尚未选择任何需要查阅的档案');
            return false;
        }
        var n_arr=new Array();
        for(var i =0;i<nodes.length;i++){
            n_arr.push(nodes[i].attributes.value);
        }
        $.post('<?php echo site_url("admin/archive/sub_applicant")?>',{archive_id:archive_id,files:n_arr,reason:$('#reason').val()},function(rt){
            alert(rt.msg);
            if(rt.status=='ok'){
                var uidstr='';
                var uids=rt.rows['uids'];
                for(var i =0;i<uids.length;i++){
                    uidstr+=uids[i]['id']+',';
                }
                uidstr=uidstr.substr(0,uidstr.length-1);
                var str='{"type":"apply","uids":"'+uidstr+'","department_name":"'+rt.rows['department_name']+'","nickname":"'+rt.rows['nickname']+'","apply_id":"'+rt.rows['apply_id']+'"}';
                parent.send_apply(str);//推送申请
                parent.layer.close(parent.layer.index);
            }
        },'json');
    }
</script>

</body>
</html>
