 <!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/gray/easyui.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">
	<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">
    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
   	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
   	<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>
   	<script type="text/javascript" src="/assets/js/comm.js?t=<?php echo rand(10000,99999)?>"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/plugins/datagrid-detailview.js"></script>
	<script src="/assets/js/my.js"></script>
</head>
<body class="easyui-layout" style="overflow-y: hidden;"   scroll="no">
<div class="easyui-layout" style="overflow-y: hidden;"  fit="true"   scroll="no">
            <div data-options="region:'north',title:'',iconCls:'icon-ok'" style="height:60%">
					<table id="dg"></table>
					<div id="tb" style="padding:10px;">
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
						  <tr>
						     <td width="550" align="left">
							  	<form  id="form_search" >
									公证书字号：<input class="easyui-textbox"  type="text" name="word_no"  id  ="word_no" data-options="width:210" value=""></input>&nbsp;
							  		当事人姓名：<input class="easyui-textbox"  type="text" name="litigant_name"  id  ="litigant_name" data-options="width:120" value=""></input>&nbsp;
									当事人身份证号码:<input class="easyui-textbox"  type="text" name="litigant_card" id  = "litigant_card" data-options="width:150" value=""></input>

								&nbsp;&nbsp;<a href="javascript:void(0);" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-mysearch'"    onclick="my_search()" >搜       索</a>
								</form>
							  </td>
							  </tr>
							<tr>
							  <td id="btn_action">


							  		<input type="file" id="uploads_file_anjuan" style="display: none;">
							  		<input type="file" id="uploads_file_mulu" style="display: none;">

								  	选择导入的公证处：<input class="easyui-combobox" style="width:120px" value="请选择"  id="department_id" name="department_id" data-options="panelHeight:'auto',panelMaxHeight:200,editable:false,valueField:'id',textField:'name',url:'<?php echo site_url('admin/department/get_all_department_json')?>'">
							  		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="uploads_anjuan()">批量上传案卷信息</a>
							  		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="uploads_mulu()">批量上传目录信息</a>
<!--							  		<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="add()">添加档案</a>-->
								  	<span id="show_percent" style=""></span>
									<span id="show_error" style="color:red;"></span>
							  </td>

						  </tr>
						</table>
					</div>
            </div>
			<div data-options="region:'center',title:'',iconCls:'icon-ok'"   style="height:40%">
				<table id="tg"></table>
					<div id="tgb" style="padding:10px;">
					<table width="100%" border="0" cellspacing="3" cellpadding="0">
						<tr>
							<td width="85%" align="left">
								<input  type="hidden"  id="archive_id"  value="">
							</td>
							<td width="200" align="right"  id="tb_btn"></td>
						</tr>
					</table>
				</div>
            </div>
</div>
<script type="text/javascript">
	WEB_SOCKET_SWF_LOCATION = "/assets/plugins/workerman/swf/WebSocketMain.swf";
	var ws;
	$(function(){
		ws = new WebSocket("ws://<?php echo $_SERVER["SERVER_ADDR"]?>:2348");
		ws.onerror = function() {
			console.log("实时后台监控未打开,请联系系统管理员");
		};
		ws.onopen=function(){
			var uid="<?php echo $_SESSION['user_info']['id']?>";
			var str='{"uid":"'+uid+'","type":"real_time_login"}';
			ws.send(str);
		};
	});

	/*
	* 发送数据给socket
	* */
	function send_data(pre,arr){
		if(pre=='anjuan'){
			for(var i =0;i<arr.length;i++){
				var d='{"type":"real_time_send","add_type":'+arr[i]+',"count":1,"pdf":0}';
				ws.send(d);
			}
		}else if(pre=='mulu'){
			for(var i =0;i<arr.length;i++){
				var d='{"type":"real_time_send","add_type":'+arr[i][0]+',"count":0,"pdf":'+arr[i][1]+'}';
				ws.send(d);
			}
			var d='{"type":"real_time_send","add_type":'+arr[i]+',"count":0,"pdf":0}';
			ws.send(d);
		}else{
			return false;
		}
	}


	var department_id=0;
	var uploads_cover;
	/*
	* 上传操作
	* */
	function uploads(pre,e){
		var index = parent.layer.load(1);
		var formData = new FormData();
		formData.append('file', e[0].files[0]);
		formData.append( "pre", pre);
		var oReq = new XMLHttpRequest();
		oReq.open( "POST", "<?php echo site_url('/admin/archive/uploads')?>" , true );
		oReq.onload = function(oEvent) {
			if (oReq.status == 200) {
				var j=JSON.parse(oReq.responseText);
				if(j.status=='error'){
					parent.layer.msg(j.msg);
					parent.layer.close(index);
				}else if(j.status=='ok'){
					//上传成功
					//询问是否批量处理
					var file= j.rows['filepath'];
					parent.layer.close(index);
					parent.layer.confirm('是否确定批量写入？', {icon: 2, title:'提示'}, function(ind){
						uploads_cover=parent.layer.load(1);
						$('#show_error').html('');
						$('#show_error').hide();
						$('#show_percent').show();
						$('#show_percent').html('当前进度：0%');
						get_excel_len(file,pre);
						parent.layer.close(ind);
					});
				}else{
					parent.layer.msg('未知错误');
					parent.layer.close(index);
				}
			} else {
				parent.layer.msg('上传失败');
				parent.layer.close(index);
			}
		};
		oReq.timeout = 30000;
		oReq.ontimeout = function(e) {
			parent.layer.msg('上传超时,请稍候再试');
			parent.layer.close(index);
		};
		oReq.send(formData);
	}

	/*
	* 获取excel长度
	* */
	function get_excel_len(file,pre){
		$.post('<?php echo site_url("/admin/archive/get_excel_length")?>',{file:file},function(rt){
			if(rt.status=='ok'&&rt.rows['len']>1){
				var len= rt.rows['len'];
				do_insert(file,pre,1,len);
			}else{
				parent.layer.msg('获取文件长度失败');
				return false;
			}
		},'json');
	}

	/*
	* 写入操作
	* file 文件名
	* pre 文件前缀,用来区分是pdf还是主档案,start是开始行数,
	* */
	function do_insert(file,pre,start,length){
		//写入操作
		var url='<?php echo site_url("/admin/archive/")?>/'+pre+'_insert';
		$.post(url,{file:file,department_id:department_id,start:start,length:length},function(rt){
			if(rt.status=='continue'){
				//获取到长度
				send_data(pre,rt['rows']['real_time']);
				$('#show_percent').html('当前进度：'+(rt.rows['percent']*100).toFixed(2)+'%');
				do_insert(file,pre,start+50,length);
			}else if(rt.status=='error'){
				$('#show_error').html(rt.rows['error_data']);
				$('#show_error').show();
				parent.layer.close(uploads_cover);
				parent.layer.msg(rt.msg);
			}else if(rt.status=='ok'){
				parent.layer.msg('导入完成');
				send_data(pre,rt['rows']['real_time']);
				$('#show_percent').html('当前进度：100%');
				parent.layer.close(uploads_cover);
			}else{
				parent.layer.msg('未知错误');
				parent.layer.close(uploads_cover);
			}
		},'json');
	}

	var atta_department_id=0;

	$(function(){

		$('#department_id').combobox({
			onSelect:function(record){
				department_id=record.id;
			}
		});


//		var url  ="<?php //echo site_url("/admin/archive/get_archive_limit_json"); ?>//";
		var url  ="<?php echo site_url("/admin/archive/archive_index_json"); ?>";
		var ht= $(window).height()-16;
		$('#btn_action').on('change','#uploads_file_anjuan',function(){
			//上传操作
			uploads('anjuan',$('#uploads_file_anjuan'));
		});
		$('#btn_action').on('change','#uploads_file_mulu',function(){
			//上传操作
			uploads('mulu',$('#uploads_file_mulu'));
		});
		$('#dg').datagrid({
			height:ht,
			data:[],
			url:url,
			queryParams:{
				department_id:-1
			},
			fit:'true',
			toolbar: "#tb",
			striped:true,
			method: 'post',
			rownumbers: true,
			idField: 'id',
			singleSelect:true,
			fitColumns: true,
			pagination: true,
			pageSize: 20,
			pageList:[20,50,100,200],
			columns:[[
				{field:'word_no',title:'公证书字号',align:'center',width:200,fixed:true},
				{field:'c_name',title:'类别',align:'center',width:75,fixed:true},
				{field:'n_name',title:'公证事项',align:'center',width:100,fixed:true},
				{field:'litigant_name',title:'当事人姓名',align:'center',width:100,fixed:true},
				{field:'litigant_card',title:'当事人身份证号码',align:'center',width:150,fixed:true},
				{field:'undertaker',title:'承办人',align:'center',width:120,fixed:true},
				{field:'receive_time',title:'受理日期',align:'center',width:80,fixed:true,
					formatter:function(value,row,index){
						return time_format(value).toString().substr(0,10);
					}
				},
				{field:'over_time',title:'办结日期',align:'center',width:80,fixed:true,
					formatter:function(value,row,index){
						return time_format(value).toString().substr(0,10);
					}
				},
				{field:'archive_time',title:'归档日期',align:'center',width:80,fixed:true,
					formatter:function(value,row,index){
						return time_format(value).toString().substr(0,10);
					}
				},
				{field:'storage_time_id',title:'保存期限',align:'center',width:60,fixed:true,
					formatter:function(value,row,index){
						if(value==1){
							return '永久';
						}else if(value==2){
							return '长期';
						}else if(value==3){
							return '短期';
						}else{
							return '未知';
						}
					}
				},
				{field:'box_number',title:'盒号',align:'center',width:40,fixed:true},
				{field:'volume_number',title:'卷号',align:'center',width:40,fixed:true},
				{field:'greffier',title:'公证员',align:'center',width:60,fixed:true},
				{field:'d_name',title:'所属公证处',align:'center',width:100,fixed:true},
				{field:'attachment',title:'附件',width:40,align:'center',fixed:'true',
					formatter:function(value,row,index){
						atta_department_id=row.department_id;
						var a = '<a   class="attachment" href="javascript:void(0);" onclick="load_attachment('+row.id+')"></a>';
						return a;
					}
				},
//			{field:'info',title:'信息',width:40,align:'center',fixed:'true',
//				formatter:function(value,row,index){
//					var a = '<a   class="msg" href="javascript:void(0);" onclick="load_info('+row.id+')"></a>';
//
//				 return a;
//				}
//			},
			{field:'edit',title:'编辑',width:40,align:'center',fixed:'true',
				formatter:function(value,row,index){
					var a = '<a   class="myedit" href="javascript:void(0);" onclick="edit('+row.id+')"></a>';
					return a;
				}
			},
			{field:'del',title:'删除',width:40,align:'center',fixed:'true',
				formatter:function(value,row,index){
					var b = '<a   class="mydel" href="javascript:void(0)"  onClick="del('+row.id+',\''+row.word_no+'\')"></a>';
					return b;
				}
			}]],
			onLoadSuccess:function(data){
				$('.attachment').linkbutton({plain:true,iconCls:'icon-add_attachment',height:20});
				$('.msg').linkbutton({plain:true,iconCls:'icon-add_msg',height:20});
				$('.myedit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
				$('.mydel').linkbutton({plain:true,iconCls:'icon-mydelete',height:20});
			},
			onDblClickRow: function(index,row){
				var url  = "<?php echo site_url('/admin/archive/detail') ?>/"+row.id;
				var ind=parent.layer.open({
				type: 2,
				title: '编辑案卷',
				maxmin: true,
				shadeClose: false,  // 点击空白页不关闭窗口
				area : ['1024px' , '95%'],
				shade: [0.2,'#fff'],
				content: url
			});
				parent.layer.full(ind);
		}
	});

		load_attachment(0);
});

function add() {
	var url  = "<?php echo site_url('admin/archive/add') ?>";
	var ind= parent.layer.open({
		type: 2,
		title: '添加案卷',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['1024px' , '95%'],
		shade: [0.2,'#fff'],
		content: url
	});

	parent.layer.full(ind);
}
function edit(id) {
	var url  = "<?php echo site_url('admin/archive/edit') ?>/"+id;
	var ind=parent.layer.open({
		type: 2,
		title: '修改案卷',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['1024px' , '95%'],
		shade: [0.2,'#fff'],
		content: url
	});
	parent.layer.full(ind);
}

function detail(id) {
	var url  = "<?php echo site_url('admin/archive/detail') ?>/"+id;
	parent.layer.open({
		type: 2,
		title: '案卷详细',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['1024px' , '95%'],
		shade: [0.2,'#fff'],
		content: url
	});
}

function del(id,word_no){
	$.messager.confirm('确认提示','你确定要删除字号为 '+word_no+' 的记录吗？',function(r){
		if (r){
			var url="<?php echo site_url('/admin/archive/delete_archive') ?>";
			$.ajax({
				type: "POST",
				url:url ,
				data: "id="+id,
				dataType:"json",
				success: function(rt){
					parent.layer.msg(rt.msg);
					if(rt.status=='ok'){
						$('#dg').datagrid('reload');
						load_attachment(0);
					}
				}
			});

		}
	});
}

	function my_search() {
//		var url = "<?php //echo site_url('/admin/archive/search') ?>///?"+$("#form_search").serialize();
//		$('#dg').datagrid({
//			url:url,
//			pageNumber:1
//		}).datagrid("reload");
		$('#dg').datagrid("load",{
			litigant_name:$('#litigant_name').textbox('getText'),
			litigant_card:$('#litigant_card').textbox('getText'),
			word_no:$('#word_no').textbox('getText')
		});
	}

	var file_pre='/uploads/attachment/pdf/';
	function load_attachment(id){
		var url  ="<?php echo site_url("/admin/archive/get_archive_pdf_json"); ?>";
		var ht= $(window).height()-40;
		$('#tg').datagrid({
		    height:ht/2,
            url:url,
			fit:'true',
			striped:true,
			method: 'post',
			queryParams:{
				id:id
			},
			toolbar: "#tgb",
			rownumbers: true,
			idField: 'id',
			singleSelect:true,
			fitColumns: true,
//			pageSize: 50,
//			pageList:[50],
			columns:[[
				{field:'file_name',title:'案卷文件名称',width:300,align:'left',editor:'text'},
				{field:'new_number',title:'案卷文件',width:300,fixed:true,align:'left',editor:'text'},
				{field:'show',title:'PDF浏览',width:80,align:'center',fixed:'true',
					formatter:function(value,row,index){
						var a = '<a class="myshow" href="javascript:void(0);" onclick="show_file(\''+row.department_id+'\',\''+row.new_number+'\')"></a>';
						return a;
					}
				},
				{field:'edit',title:'上传文件',width:80,align:'center',fixed:'true',
					formatter:function(value,row,index){
						var a = '<a   class="editfile" href="javascript:void(0);" onclick="edit_file('+id+',\''+row.name+'\')"></a>';
						return a;
					}
				}
				]],
				onLoadSuccess:function(data){
							$('.editfile').linkbutton({plain:true,iconCls:'icon-editfile',height:20});
							$('.myshow').linkbutton({plain:true,iconCls:'icon-myshow',height:20});
				},
				onDblClickRow: function(index,row){

				}
		});

		$("#tb_btn_add").remove();


}

//function load_info(id){
//    var url  ="<?php //echo site_url("/admin/archive/get_archive_info_limit_json"); ?>///"+id;
//	var ht= $(window).height()-16;
//	$('#tg').datagrid({
//		height:ht/2,
//		url:url,
//		fit:'true',
//		striped:true,
//		toolbar: "#tgb",
//		method: 'post',
//		rownumbers: true,
//		idField: 'id',
//		singleSelect:true,
//		fitColumns: true,
//		pagination: true,
//		pageSize: 10,
//		pageList:[10,20],
//		columns:[[
//			{field:'id',title:'id',width:$(this).width() * 0.1,align:'left',editor:'text'},
//			{field:'relation_name',title:'与当事人关系',width:$(this).width() * 0.1,align:'left',editor:'text'},
//			{field:'related_name',title:'相关人',width:$(this).width() * 0.1,align:'left',editor:'text'},
//			{field:'related_card',title:'相关人身份证',width:$(this).width() * 0.1,align:'left',editor:'text'},
//			{field:'related_type',title:'是否本人',width:$(this).width() * 0.1,align:'left',editor:'text'},
//			{field:'edit',title:'编辑文件',width:80,align:'center',fixed:'true',
//				formatter:function(value,row,index){
//
//						var a = '<a   class="myedit" href="javascript:void(0);" onclick="edit_info('+row.id+')"></a>';
//
//					 return a;
//				}
//			},
//			{field:'del',title:'删除',width:40,align:'center',fixed:'true',
//					formatter:function(value,row,index){
//							var b = '<a   class="mydel" href="javascript:void(0)"  onClick="del_msg('+row.id+',\''+row.title+'\')"></a>';
//
//					 return b;
//				}
//			}
//			]],
//			onLoadSuccess:function(data){
//						$('.myedit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
//						$('.mydel').linkbutton({plain:true,iconCls:'icon-mydelete',height:20});
//			},
//			onDblClickRow: function(index,row){
//
//			}
//	   	});
//
//		$("#archive_id").attr("value",id);
//		$("#tb_btn_add").remove();
//		var targetObj = $("<a href='javascript:void(0);'id='tb_btn_add' class='easyui-linkbutton'    onclick='add_info()'>添加案卷信息</a>").appendTo("#tb_btn");
//		$.parser.parse(targetObj);
//		$('#tb_btn_add').linkbutton({plain:true,iconCls:'icon-add_archive_msg',height:20});
//}
function  edit_file(id,name){
//	alert(atta_department_id);
	var str='<input type="file" id="uploads_pdf" accept="application/pdf" onchange="file_change()" style="display: none;">';
	$('#uploads_pdf').remove();
	$('#btn_action').append(str);
	$('#uploads_pdf').trigger('click');
}

function file_change(){
	parent.layer.confirm('是否确定上传文件？', {icon: 2, title:'提示'}, function(ind){

		parent.layer.close(ind);
		var pdf_cover=parent.layer.load(1);
		var file=$('#uploads_pdf');
		//上传操作
		var formData = new FormData();
		formData.append('pdf_file', file[0].files[0]);
		formData.append('department_id', atta_department_id);
		var oReq = new XMLHttpRequest();
		oReq.open( "POST", "<?php echo site_url('/admin/archive/uploads_pdf')?>" , true );
		oReq.onload = function(oEvent) {
			if (oReq.status == 200) {
				var j=JSON.parse(oReq.responseText);
				if(j.status=='error'){
					parent.layer.msg(j.msg);
					parent.layer.close(pdf_cover);
				}else if(j.status=='ok'){
					//上传成功
					parent.layer.msg('上传成功');
					parent.layer.close(pdf_cover);
					//询问是否批量处理
//					var file= j.rows['filepath'];
//					parent.layer.close(index);
//					parent.layer.confirm('是否确定批量写入？', {icon: 2, title:'提示'}, function(ind){
//						uploads_cover=parent.layer.load(1);
//						$('#show_error').html('');
//						$('#show_error').hide();
//						$('#show_percent').show();
//						$('#show_percent').html('当前进度：0%');
//						get_excel_len(file,pre);
//						parent.layer.close(ind);
//					});
				}else{
					parent.layer.msg('未知错误');
					parent.layer.close(pdf_cover);
				}
			} else {
				parent.layer.msg('上传失败');
				parent.layer.close(pdf_cover);
			}
		};
		oReq.timeout = 30000;
		oReq.ontimeout = function(e) {
			parent.layer.msg('上传超时,请稍候再试');
			parent.layer.close(pdf_cover);
		};
		oReq.send(formData);


//		setTimeout(function(){
//			parent.layer.close(pdf_cover);
//			parent.layer.close(ind);
//		},2000);
	});
}

function add_info(){
	var id = $("#archive_id").attr("value");
	var url  = "<?php echo site_url('admin/archive/add_info') ?>/"+id;
	parent.layer.open({
		type: 2,
		title: '添加',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['900px' , '600px'],
		shade: [0.2,'#fff'],
		content: url
	});

}


function edit_info(id){
	var url  = "<?php echo site_url('admin/archive/edit_info') ?>/"+id;
	parent.layer.open({
		type: 2,
		title: '编辑',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['900px' , '600px'],
		shade: [0.2,'#fff'],
		content: url
	});
}

function del_msg(id,title){
	$.messager.confirm('确认提示','你确定要删除ID为 '+id+' 的记录吗？',function(r){
		if(r){
			var url="<?php echo site_url('/admin/archive/del_msg'.'') ?>";
			$.ajax({
				type: "POST",
				url:url ,
				data: "id="+id,
				dataType:"json",
				success: function(msg){
					layer.msg(msg.info, {
										icon: 1,  //1 成功 ,0 失败
										time: 2000
									  });
					$('#tg').datagrid('reload');
				}
			});
		}
	});
}

//展示pdf文件
function  show_file(department_id,new_number){
	var url = "<?php echo site_url('assets/plugins/pdfjs/generic/web/viewer.html')?>?file="+file_pre+department_id+'/'+new_number+'.pdf';
	parent.layer.open({
		type: 2,
		title: '浏览',
		maxmin: true,
		shadeClose: false,  // 点击空白页不关闭窗口
		area : ['1024px' , '90%'],
		shade: [0.2,'#fff'],
		content: url
	});
}

function  reload(){
	$('#dg').datagrid("reload");
}

function  info_reload(){
	$('#tg').datagrid("reload");
}

	/*
	* 批量上传案卷
	* */
	function uploads_anjuan(){

		//判断是否选择公证处
		if(department_id==0){
			parent.layer.msg('尚未选择公证处，请先选择');
			$('#department_id').combobox('showPanel');
			return false;
		}
		add_uploads_file();
		$('#uploads_file_anjuan').trigger('click');
	}

	/*
	* 批量上传目录
	* */
	function uploads_mulu(){

		//判断是否选择公证处
//		if(department_id==0){
//			parent.layer.msg('尚未选择公证处，请先选择');
//			$('#department_id').combobox('showPanel');
//			return false;
//		}
		add_uploads_file();
		$('#uploads_file_mulu').trigger('click');
	}

	/*
	* 删除上传的控件,重新添加
	* */
	function add_uploads_file(){
		var str='<input type="file" id="uploads_file_anjuan" style="display: none;"><input type="file" id="uploads_file_mulu" style="display: none;">';
		$('#uploads_file_anjuan').remove();
		$('#uploads_file_mulu').remove();
		$('#btn_action').append(str);
	}

</script>

</body>
</html>
﻿