<!DOCTYPE html>
<html>
<!--<head>-->
<!--    <meta charset="UTF-8">-->
<!--    <title></title>-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/default/easyui.css">-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">-->
<!--    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">-->
<!--    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>-->
<!--    <script type="text/javascript" src="/assets/js/comm.js?t=--><?php //echo rand(10000,99999)?><!--"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>-->
<!--    <script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>-->
<!--</head>-->
<body class="easyui-layout" style="overflow-y: hidden;"   scroll="no">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north'" style="height:50px;line-height: 25px;padding-left:5px;">
        <span style="word-break: break-all;">申请查阅理由:<?php echo $reason;?></span>
    </div>
    <div data-options="region:'west'" style="width:300px;padding:7px;">
        选择:<input title="全选/反选" type="checkbox" onClick="treeChecked(this)"/>全选/全不选
        <ul id="mytree" class="easyui-tree" data-options="animate:true,checkbox:true"></ul>
        <div style="margin:10px;">
            <a href="#" class="easyui-linkbutton" style="float:left;"  data-options="" onclick="pass()">通  过</a>
            <a href="#" class="easyui-linkbutton" style="float:left;margin-left:10px;"  data-options="" onclick="no_pass()">驳  回</a>
        </div>
    </div>
    <div data-options="region:'center'" style="width:400px;overflow: hidden;">
        <iframe id="content" name="content" scrolling="no" frameborder="0" src="" style="width:100%;height:100%"></iframe>
    </div>
</div>
<script>
    var tree_data='<?php echo $tree;?>';
    var id='<?php echo $id?>';
    var archive_id='<?php echo $archive_id?>';
    var apply_uid='<?php echo $apply_uid?>';
    var reload='<?php echo $reload?>';
    $('#mytree').tree({
        data:JSON.parse(tree_data),
        onClick: function(node){
            var filename =  node.attributes.url;
            var url = "<?php echo site_url('assets/plugins/pdfjs/generic/web/viewer.html')?>?file="+filename;
            $("#content").attr('src',url);
        }
    });

    /*
    * 通过
    * */
    function pass(){
        var nodes = $('#mytree').tree('getChecked','checked');
        if(nodes.length==0){
            alert('您尚未选择任何需要查阅的档案');
            return false;
        }
        var r=confirm('是否确定通过这个申请吗?');
        if(!r){
            return false;
        }
        var n_arr=new Array();
        for(var i =0;i<nodes.length;i++){
            n_arr.push(nodes[i].attributes.value);
        }
        $.post('<?php echo site_url("admin/archive/apply_pass")?>',{id:id,archive_id:archive_id,files:n_arr},function(rt){
            alert(rt.msg);
            if(rt.status=='ok'){
                var str='{"type":"audition","uid":"'+apply_uid+'","ispass":"true","archive_id":"'+archive_id+'"}';
                parent.parent.send_audition(str);
                if(reload==1){
                    $("#iframe_src", parent.document)[0].contentWindow.reload();
                }
                parent.layer.closeAll();
            }
        },'json');
    }


    function treeChecked(selected) {
        if(selected.checked){
            //全部选中
            var nodes = $('#mytree').tree('getChecked', 'unchecked');
            var s='check';
        }else{
            //全部不选中
            var nodes = $('#mytree').tree('getChecked','checked');
            var s='uncheck';
        }
        for(var i=0;i<nodes.length;i++){
            $('#mytree').tree(s,nodes[i].target)
        }
    }

    /*
    * 驳回
    * */
    function no_pass(){
        var r=confirm('是否确定驳回这个申请吗?');
        if(!r){
            return false;
        }
        $.post('<?php echo site_url("admin/archive/apply_no_pass")?>',{id:id,archive_id:archive_id},function(rt){
            alert(rt.msg);
            if(rt.status=='ok'){
                var str='{"type":"audition","uid":"'+apply_uid+'","ispass":"false","archive_id":"'+archive_id+'"}';
                parent.parent.send_audition(str);
                if(reload==1){
                    $("#iframe_src", parent.document)[0].contentWindow.reload();
//                    parent.frames["iframe_src"].window.reload();
                }
                parent.layer.closeAll();
            }
        },'json');
    }
</script>

</body>
</html>
