<script type="text/javascript" src="/assets/js/echarts.min.js"></script>
<script type="text/javascript" src="/assets/plugins/workerman/js/swfobject.js"></script>
<script type="text/javascript" src="/assets/plugins/workerman/js/web_socket.js"></script>
<style>
    html,body{
        width:100%;
        height:100%;
    }
</style>
<div id="main" style="width: 90%;height:90%;"></div>
<script type="text/javascript">
    WEB_SOCKET_SWF_LOCATION = "/assets/plugins/workerman/swf/WebSocketMain.swf";
    $(function(){
        var ws = new WebSocket("ws://<?php echo $_SERVER["SERVER_ADDR"]?>:2348");
        ws.onerror = function() {
            console.log("实时后台监控未打开,请联系系统管理员");
        };
        ws.onopen=function(){
            var uid="<?php echo $_SESSION['user_info']['id']?>";
            var str='{"uid":"'+uid+'","type":"real_time_login"}';
            ws.send(str);
        };
        ws.onmessage = function(e) {// Receives a message.
//            console.log(e);
            var add_data= JSON.parse(e.data);
            for(var i = 0;i<data_array.length;i++){
                if(data_type[i]==add_data.add_type){
                    data_total[i]+=parseInt(add_data.count);
                    data_pdf[i]+=parseInt(add_data.pdf);
                    break;
                }
            }
            option = {
                series : [
                    {
                        data:data_total
                    },
                    {
                        data:data_pdf
                    }
                ]
            };
            myChart.setOption(option);
        };

//        var kk=0;
//        var s=setInterval(function(){
//            var d='{"type":"real_time_send","add_type":3,"count":1,"pdf":5}';
//            kk++;
//            ws.send(d);
//            if(kk==8){
//                clearInterval(s);
//            }
//        },100);

        var data='<?php echo $data?>';
        var data_array=JSON.parse(data);
        var data_type=new Array();
        var data_str=new Array();
        var data_total=new Array();
        var data_pdf=new Array();
        for(var i = 0;i<data_array.length;i++){
            data_type.push(parseInt(data_array[i]['id']));
            data_str.push(data_array[i]['name']);
            data_total.push(parseInt(data_array[i]['total']));
            data_pdf.push(parseInt(data_array[i]['pdf_num']));
        }

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        // 指定图表的配置项和数据
        var option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data:['实时档案数', '实时影像数']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'value'
                }
            ],
            yAxis : [
                {
                    type : 'category',
                    axisTick : {show: false},
                    data :data_str
                }
            ],
            series : [
                {
                    name:'实时档案数',
                    type:'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'right'
                        }
                    },
                    data:data_total
                },
                {
                    name:'实时影像数',
                    type:'bar',
//                stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'right'
                        }
                    },
                    data:data_pdf
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);

        function test(){
//            option = {
//                series : [
//                    {
//                        data:data_total
//                    },
//                    {
//                        data:data_pdf
//                    }
//                ]
//            };
//            myChart.setOption(option);
        }
    });
</script>
</body>
</html>
<style>
    .module{
        width:400px;
        height:306px;
        border:1px solid #888888;
        float:left;
        margin:5px;
    }
    caption{
        height:60px;
        line-height: 60px;
    }
    td{
        border-top:1px solid #888888;
        text-align: center;
        height:40px;
        line-height: 40px;
    }
</style>
