<script src="/assets/js/my.js"></script>
<table id="dg"></table>
<div id="tb" class="dg_tb">
<!--    <div>年份：<input class="easyui-textbox" style="width:80px" type="text" name="receive_year"  id  ="receive_year" data-options="" value=""></div>-->
<!--    <div>类别：<input class="easyui-combobox" style="width:120px"  id="category_id" name="category_id" data-options="panelHeight:'auto',panelMaxHeight:200,editable:false,valueField:'id',textField:'text',url:'--><?php //echo site_url('admin/category/get_category_list')?><!--'"></div>-->
    <div>公证书字号：<input class="easyui-textbox"  style="width:180px" type="text" name="word_no" id  = "word_no" data-options="" value=""/></div>
    <div>公证员：<input class="easyui-textbox"  style="width:80px" type="text" name="greffier" id  = "greffier" data-options="" value=""/></div>
    <div>承办人：<input class="easyui-textbox"  style="width:80px" type="text" name="undertaker" id  = "undertaker" data-options="" value=""/></div>
<!--    <div>公证事项：<input class="easyui-combobox" style="width:120px"  id="notary_matter_id" name="notary_matter_id" data-options="panelHeight:'auto',panelMaxHeight:200,editable:false,valueField:'id',textField:'text',url:'--><?php //echo site_url('admin/notary_matter/get_notary_matter_list')?><!--'"></div>-->
    <div>公证事项：<input class="easyui-textbox" style="width:80px"  id="notary_matter" name="notary_matter" data-options=""></div>
    <div>当事人姓名：<input class="easyui-textbox" style="width:80px" type="text" name="litigant_name" id  = "litigant_name" data-options="" value=""/></div>
    <div>当事人身份证号码：<input class="easyui-textbox" style="width:150px" type="text" name="litigant_card" id  = "litigant_card" data-options="" value=""/></div>
    <br>
    <div>
        <input class="chkbox" type="checkbox" onchange="change_date_state(this)">受理日期：
        <input id="receive_time_from"  type= "text" class= "easyui-datebox" data-options="width:100,editable:false,disabled:true">
        -
        <input id="receive_time_to"  type= "text" class= "easyui-datebox" data-options="width:100,editable:false,disabled:true">
    </div><!--刚加的-->
    <div style="margin-left:20px;">
        <input class="chkbox" type="checkbox" onchange="change_date_state(this)">办结日期：
        <input id="over_time_from"  type= "text" class= "easyui-datebox" data-options="width:100,editable:false,disabled:true">
        -
        <input id="over_time_to"  type= "text" class= "easyui-datebox" data-options="width:100,editable:false,disabled:true">
    </div><!--刚加的-->
<!--    <div>保存期限：-->
<!--    <select id="storage_time_id" class="easyui-combobox" name="storage_time_id" style="width:100px;" data-options="panelHeight:'auto',panelMaxHeight:200,editable:false">-->
<!--        <option value="-1">全部</option>-->
<!--        <option value="0">永久</option>-->
<!--        <option value="1">10年</option>-->
<!--        <option value="2">20年</option>-->
<!--        <option value="3">30年</option>-->
<!--    </select></div>-->
<!--    <div>盒号：<input class="easyui-textbox"   style="width:80px" type="text" name="box_number" id  = "box_number" data-options="" value=""/></div>-->
<!--    <div>卷号：<input class="easyui-textbox"  style="width:80px" type="text" name="volume_number" id  = "volume_number" data-options="" value=""/></div>-->
    <span class="easyui-linkbutton" style="float:right;margin-right:20px;"  data-options="iconCls:'icon-search'" onclick="my_search()" >搜    索</span>
</div>
<style>
</style>
<script>
    var url  ="<?php echo site_url('admin/archive/archive_index_json')?>";
    $(function(){
        var ht= $(window).height()-12;
        $('#dg').datagrid({
            height:ht,
            queryParams:{
                department_id:-1
            },
            data:[],
            url:url,
            toolbar: "#tb",
            striped:true,
            method: 'post',
            fit:true,
            fitColumns:true,
            rownumbers: true,
            idField: 'id',
            singleSelect:true,
            pagination: true,
            pageSize: 50,
            pageList:[50,100,200],
            columns:[[
                {field:'word_no',title:'公证书字号',align:'center',width:200,fixed:true},
                {field:'c_name',title:'类别',align:'center',width:80,fixed:true},
                {field:'n_name',title:'公证事项',align:'center',width:100,fixed:true},
                {field:'litigant_name',title:'当事人姓名',align:'center',width:100,fixed:true},
                {field:'litigant_card',title:'当事人身份证号码',align:'center',width:150,fixed:true},
                {field:'undertaker',title:'承办人',align:'center',width:120,fixed:true},
                {field:'receive_time',title:'受理日期',align:'center',width:80,fixed:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'over_time',title:'办结日期',align:'center',width:80,fixed:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'archive_time',title:'归档日期',align:'center',width:80,fixed:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'storage_time_id',title:'保存期限',align:'center',width:60,fixed:true,
                    formatter:function(value,row,index){
                        if(value==1){
                            return '永久';
                        }else if(value==2){
                            return '长期';
                        }else if(value==3){
                            return '短期';
                        }else{
                            return '未知';
                        }
                    }
                },
                {field:'box_number',title:'盒号',align:'center',width:40,fixed:true},
                {field:'volume_number',title:'卷号',align:'center',width:40,fixed:true},
                {field:'greffier',title:'公证员',align:'center',width:60,fixed:true},
                {field:'d_name',title:'所属公证处',align:'center',width:100,fixed:true},
                {field:'request',title:'查阅',width:50,align:'center',fixed:'true',
                    formatter:function(value,row,index){;
                        var b = '<div class="edit" onclick="open_pdf('+row.id+')"></div>';
                        return b;
                    }
                },
            ]],
            onLoadSuccess:function(data){
                $('.edit').linkbutton({plain:true,iconCls:'icon-edit',height:20});
            },
            onDblClickRow: function(index,row){
                open_archive(row.id);
            }
        });
    });

    /*
    * 改变日期选择状态
    * */
    function change_date_state(t){
        var d=$(t).siblings(".easyui-datebox");
        if($(t)[0].checked==true){
            $(d[0]).textbox('enable');
            $(d[1]).textbox('enable');
        }else{
            $(d[0]).textbox('disable');
            $(d[1]).textbox('disable');
        }
    }

    /*
    * 申请档案
    * */
    function my_search(){
        if(!checkbox_check()){
            return false;
        }


        $('#dg').datagrid('load',{
//            receive_year:$('#receive_year').val(),
//            category_id:$('#category_id').combobox('getValue'),
            notary_matter:$('#notary_matter').textbox('getValue'),
            greffier:$('#greffier').textbox('getValue'),
            undertaker:$('#undertaker').textbox('getValue'),
            word_no:$('#word_no').val(),
            litigant_name:$('#litigant_name').val(),
            litigant_card:$('#litigant_card').val(),
            receive_time_chk:$('.chkbox')[0].checked,
            over_time_chk:$('.chkbox')[1].checked,
            receive_time_from:$('#receive_time_from').datebox('getText'),
            receive_time_to:$('#receive_time_to').datebox('getText'),
            over_time_from:$('#over_time_from').datebox('getText'),
            over_time_to:$('#over_time_to').datebox('getText'),
//            storage_time_id:$('#storage_time_id').combobox('getValue'),
//            box_number:$('#box_number').val(),
//            volume_number:$('#volume_number').val(),
            department_id:0
        });
    }

    /*
    * 判断checkbox
    * */
    function checkbox_check(){
        var chk=$('.chkbox');
        for(var i=0;i<chk.length;i++){
            if(chk[i].checked==true){
                //判断开始日期是否大于结束日期
                var d=$(chk[i]).siblings(".easyui-datebox");
                var date0=$(d[0]).datebox('getText');
                var date1=$(d[1]).datebox('getText');
                if(date0>date1&&date0!=''&&date1!=''){
                    parent.layer.msg('开始日期大于结束日期');
                    $(d[1]).datebox('showPanel');
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * 查询档案
     * */
    function open_pdf(id){
        var index = parent.layer.open({
            type: 2,
            title: '档案附件',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['1024px' ,'95%'],
            shade: [0.2,'#fff'],
            content: "<?php echo site_url('admin/archive/get_archive_status')?>?id="+id
        });
        parent.layer.full(index);
    }

    /*
     * 打开档案详情
     * */
    function open_archive(id){
        var url  = "<?php echo site_url('admin/archive/detail') ?>/"+id;
        var index = parent.layer.open({
            type: 2,
            title: '档案明细查询',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['800px' , '95%'],
            shade: [0.2,'#fff'],
            content: url
        });
    }

</script>