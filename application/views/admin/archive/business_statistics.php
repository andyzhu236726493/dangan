<script src="/assets/js/echarts.min.js"></script>
<style>
    html,body{
        width:100%;
        height:100%;
    }
</style>
<div id="main" style="width: 90%;height:90%;"></div>
<script>
    var myChart = echarts.init(document.getElementById('main'));
    var json=jQuery.parseJSON('<?php echo $statistics;?>');

    var data_year=new Array();
    var data_val=new Array();

    for(var i=0;i<json.length;i++){
        data_year.push(json[i]['receive_year']);
        data_val.push(json[i]['total']);
    }
    var option = {
        title: {
            text: '年度业务量统计报表',
            left:'center'
        },
        tooltip: {
            trigger: 'axis',
            formatter: "{b}年业务总量：{c}"
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {

        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: data_year
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name:'',
                type:'line',
                stack: '总量',
                data: data_val
            }
        ]
    };
    myChart.setOption(option);
</script>