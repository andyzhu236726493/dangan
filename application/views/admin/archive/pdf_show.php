<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/default/easyui.css">
    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/icon.css">
    <link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.3/themes/color.css">
    <script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/assets/js/comm.js?t=<?php echo rand(10000,99999)?>"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>
</head>
<body class="easyui-layout" style="overflow-y: hidden;"   scroll="no">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north'" style="height:50px;line-height: 40px;padding-left:5px;">
        <a href="#" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-search'">打印整个档案</a>
        <?php
            if($re_applicant==true){
                echo '<a href="#" onclick="re_applicant()" class="easyui-linkbutton" style=""  data-options="">申请更多档案</a>';
            }
        ?>
    </div>
    <div data-options="region:'west'" style="width:300px;padding:7px;">
        <ul id="mytree" class="easyui-tree" data-options="animate:true"></ul>
    </div>
    <div data-options="region:'center'" style="width:400px;overflow: hidden;">
        <iframe id="content" name="content" scrolling="no" frameborder="0" src="" style="width:100%;height:100%"></iframe>
    </div>
</div>
<script>
    var tree_data='<?php echo $tree;?>';
    $('#mytree').tree({
        data:JSON.parse(tree_data),
        onClick: function(node){
            var filename =  node.attributes.url;
            var url = "<?php echo site_url('assets/plugins/pdfjs/generic/web/viewer.html')?>?file="+filename;
            $("#content").attr('src',url);
        }
    });

    /*
    * 重新申请
    * */
    function re_applicant(){
        window.location.href="<?php echo site_url('admin/archive/re_applicant')?>?archive_id=<?php echo $archive['id']?>";
    }
</script>

</body>
</html>
