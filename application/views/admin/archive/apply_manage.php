<script src="/assets/js/my.js"></script>
<table id="dg"></table>
<div id="tb" class="dg_tb" style="height:40px;">
    字号：<input class="easyui-textbox"  style="width:80px" type="text" name="word_no" id  = "word_no" data-options="" value=""/>
    姓名：<input class="easyui-textbox" style="width:80px" type="text" name="litigant_name" id  = "litigant_name" data-options="" value=""/>
    证件号：<input class="easyui-textbox" style="width:150px" type="text" name="litigant_card" id  = "litigant_card" data-options="" value=""/>
    申请状态：
    <select id="review" class="easyui-combobox" name="review" style="width:100px;" data-options="panelHeight:'auto',panelMaxHeight:200,editable:false">
        <option value="0">待审核</option>
        <option value="1">已通过</option>
        <option value="2">已驳回</option>
        <option value="-1">全部</option>
    </select>
    &nbsp;&nbsp;<span class="easyui-linkbutton" style=""  data-options="iconCls:'icon-search'"    onclick="my_search()" >搜    索</span>
</div>
<style>
</style>
<script>
    $(function(){
        var url  ="<?php echo site_url('admin/archive/apply_manage_json')?>";
        var ht= $(window).height()-12;
        $('#dg').datagrid({
            height:ht,
            url:url,
            toolbar: "#tb",
            striped:true,
            method: 'post',
            rownumbers: true,
            idField: 'id',
            singleSelect:true,
            fitColumns: true,
            pagination: true,
            pageSize: 20,
            pageList:[20,50,100,200],
            columns:[[
                {field:'word_no',title:'字号',align:'center',width:180,fixed:true},
                {field:'c_name',title:'类别',align:'center',width:80,fixed:true},
                {field:'n_name',title:'公证事项',align:'center',width:160,fixed:true},
                {field:'litigant_name',title:'姓名',align:'center',width:150,fixed:true},
                {field:'litigant_card',title:'证件号',align:'center',width:120,fixed:true},
                {field:'undertaker',title:'承办人',align:'center',width:80,fixed:true},
                {field:'receive_time',title:'受理时间',align:'center',width:80,fixed:true,hidden:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'over_time',title:'办结时间',align:'center',width:70,fixed:true,hidden:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'archive_time',title:'归档日期',align:'center',width:70,fixed:true,hidden:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {field:'storage_time_id',title:'保存年限',align:'center',width:60,fixed:true,
                    formatter:function(value,row,index){
                        if(value==1){
                            return '永久';
                        }else if(value==2){
                            return '长期';
                        }else if(value==3){
                            return '短期';
                        }else{
                            return '未知';
                        }
                    }
                },
                {field:'box_number',title:'盒号',align:'center',width:40,fixed:true},
                {field:'volume_number',title:'卷号',align:'center',width:40,fixed:true},
                {field:'greffier',title:'公证员',align:'center',width:60,fixed:true},
                {field:'nickname',title:'申请人',align:'center',width:120,fixed:true},
                {field:'d_name',title:'申请人所属公证处',align:'center',width:100,fixed:true},
                {field:'apply_create_time',title:'申请时间',align:'center',width:80,fixed:true,
                    formatter:function(value,row,index){
                        return time_format(value).toString().substr(0,10);
                    }
                },
                {
                    field:'review',title:'申请状态',align:'center',width:60,fixed:true,
                    formatter:function(value,row,index){
                        switch (row.review){
                            case '0':
                                return '待审核';
                                break;
                            case '1':
                                return '已同意';
                                break;
                            case '2':
                                return '已驳回';
                                break;
                            default:
                                return '未知';
                                break;
                        }
                    }
                },
                {field:'request',title:'操作',width:60,align:'center',fixed:true,
                    formatter:function(value,row,index){;
                        var b = '<div class="edit" onclick="audition('+row.apply_id+')"></div>';
                        return b;
                    }
                },
            ]],
            onLoadSuccess:function(data){
                $('.edit').linkbutton({plain:true,iconCls:'icon-edit',height:20});
            },
            onDblClickRow: function(index,row){
                open_archive(row.id);
            }
        });
    });



    /*
     * 申请档案
     * */
    function my_search(){
        $('#dg').datagrid('load',{
            word_no:$('#word_no').val(),
            litigant_name:$('#litigant_name').val(),
            litigant_card:$('#litigant_card').val(),
            review:$('#review').combobox('getValue')
        });
    }

    /*
     * 审核档案
     * */
    function audition(id){
        var index = parent.layer.open({
            type: 2,
            title: '档案查阅审核',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['1024px' ,'95%'],
            shade: [0.2,'#fff'],
            content: "<?php echo site_url('admin/archive/apply_audition')?>?id="+id
        });
    }

    /*
     * 查询档案
     * */
    function open_archive(id){
        var url  = "<?php echo site_url('admin/archive/detail') ?>/"+id;
        var index = parent.layer.open({
            type: 2,
            title: '档案明细查询',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['800px' , '95%'],
            shade: [0.2,'#fff'],
            content: url
        });
    }

    function reload(){
        $('#dg').datagrid('reload');
    }

</script>