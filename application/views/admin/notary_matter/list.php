<script type="text/javascript">
       	var url  ="<?php  echo site_url('/admin/notary_matter/get_notary_matter_tree_limit_json') ?>";
       	//var url = "/uploads/treegrid_data2.json";

	    var ht= $(window).height()-16;
		$(function(){
			$('#tt').treegrid({
				height:ht,
				url:url,
				fit:'true',
				method: 'post',
				rownumbers: true,
				idField: 'id',
				fit:'true',
				treeField: 'name',
				toolbar: "#tb",
				animate: true,
				showFoote:true,
				collapsible: true,
				fitColumns: true,
				showFooter: true,
				columns:[[
				{field:'id',title:'id',width:30,align:'center',editor:'text'},
				{field:'name',title:'name',width:$(this).width() * 0.1,align:'left',editor:'text'},
				{field:'field',title:'field',width:$(this).width() * 0.1,align:'left',editor:'text'},
				{field:'level',title:'level',width:$(this).width() * 0.1,align:'center',editor:'text'},
				{field:'edit',title:'编辑',width:60,align:'center',fixed:'true',
					formatter:function(value,row,index){

						 	var a = '<a   class="myedit" href="javascript:void(0)" onclick="edit('+row.id+')"></a>';

						 return a;
						}
					},
					{field:'del',title:'删除',width:60,align:'center',fixed:'true',
					formatter:function(value,row,index){
						   		var b = '<a   class="mydel" href="javascript:void(0)"  onClick="del('+row.id+',\''+row.name+'\')"></a>';

						 return b;
					}
				  }
				 ]],
				onLoadSuccess:function(data){
							$('.myedit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
							$('.mydel').linkbutton({plain:true,iconCls:'icon-mydelete',height:20});
				}
			});


});



function add() {
	var url  = "<?php echo site_url('/admin/notary_matter/add') ?>";
	parent.layer.open({
        type: 2,
        title: '添加公证事项',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['610px' , '450px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function edit(id) {
	var url  = "<?php echo site_url('/admin/notary_matter/edit') ?>/"+id;
	parent.layer.open({
        type: 2,
        title: '修改公证事项',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['900px' , '640px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function del(id,pid,name){

	if(pid ==0){
 			parent.layer.msg("系统管理员不能删除", {
								    icon: 1,  //1 成功 ,0 失败
								    time: 2000　//２　秒
								});
	}else{
		$.messager.confirm('确认提示','你确定要删除账号为 '+name+' 的记录吗？',function(r){
			if (r){
				var url="<?php echo site_url('/admin/user/del') ?>";
				$.ajax({
				   type: "POST",
				   url:url ,
				   data: "id="+id,
				   dataType:"json",
				   success: function(msg){
					   layer.msg(msg.info, {
										    icon: 1,  //1 成功 ,0 失败
										    time: 2000
										  });
					   $('#tt').datagrid('reload');
				   }
				});

			}
		});
	}
}

</script>
<table id="tt"></table>
<div id="tb" style="padding:10px;">
	<table width="100%" border="0" cellspacing="3" cellpadding="0">
	  <tr>
	     <td width="85%" align="left">

		  </td>
		  <td width="200" align="right">

		  		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="add()">添加公证事项</a>

		  </td>

	  </tr>
	</table>
</div>

</body>
</html>