<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
<p style="margin:20px">
<form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
    <table cellpadding="5" border="0">
        <tr >
            <td align="left" >原始密码:</td>
            <td align="left" width="200">
                <input class="easyui-textbox easyui-validatebox"  name="old_password"  id="old_password" type="password" data-options="required:true,novalidate:true" validType="length[2,10]"  missingMessage="请输入密码"    invalidMessage="密码要求大于4个字符，不超过15字符！"  value="" >
                <span style="color:#ff2e2e">*</span>

            </td>
        </tr>
        <tr >
            <td align="left" >密&emsp;&emsp;码:</td>
            <td align="left" width="200">
                <input class="easyui-textbox easyui-validatebox"  name="password"  id="password" type="password" data-options="required:true,novalidate:true" validType="length[2,10]"  missingMessage="请输入密码"    invalidMessage="密码要求大于4个字符，不超过15字符！"  value="" >
                <span style="color:#ff2e2e">*</span>

            </td>
        </tr>
        <tr >
            <td align="left">确认密码:</td>
            <td align="left">
                <input class="easyui-textbox easyui-validatebox"  name="recpwd"  id="recpwd" type="password" data-options="required:true,novalidate:true" validType="eqPassword['#password']"  missingMessage="请输入确认密码" invalidMessage="两次密码输入不一样！"  value="">
                <span style="color:#ff2e2e">*</span>

            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align:center;padding:15px">
                <span id="msg"></span>
            </td>
        </tr>
        <tr style="">
            <td  colspan="2" style="text-align:center;padding:5px">
                <a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">提            交</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align:center;padding:5px">
                <span id="msg"></span>
            </td>
        </tr>

    </table>
</form>

<script type="text/javascript">
    $(function(){


    });

    function submitForm(){

        $('#form1').form('submit',{
            onSubmit:function(){
                var arr=new Array();
                arr.push(new Array('old_password','textbox'));
                arr.push(new Array('password','textbox'));
                arr.push(new Array('recpwd','textbox'));
                if(validate_arr(arr)){
                    var url = "<?php echo $action_url ?>";
                    $.ajax({
                        type: "POST",
                        url:url,
                        dataType:"json",
                        data:$("#form1").serialize(),
                        success: function(rt){
                            // alert(msg);
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.layer.msg(rt.msg);
                            if(rt.status=='ok'){
                                parent.layer.close(index);
                            }

                        }
                    });
                }

            }
        });


    }
    //Ajax远程验证使用,/Aassets/js/comm.js
    var checkaccount_url  	= "<?php echo site_url('/admin/user/check_account') ?>";
    var checkaccount_name 	= "账号";
    var checkphone_url    	= "<?php echo site_url('/login/check_phone') ?>";
    var checkphone_name 	= "手机号码";

</script>

</body>
</html>