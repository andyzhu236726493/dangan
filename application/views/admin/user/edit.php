	<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
    <p style="margin:20px">
	<form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
		<table cellpadding="5" border="0">
			 <tr>
				<td style="width:80px" >用&ensp;户&ensp;名:</td>

				<td>
		   			  <input type="hidden" id="user_id"  name="id"    value="<?php echo $user['id'] ?>"  >
		   			  <input type="hidden"  id="oldaccount"   value="<?php echo $user['account'] ?>"  >

		   			   <input class="easyui-textbox easyui-validatebox"  type="text" id="account" name="account" data-options="required:true,novalidate:true" validType="checkaccount[2,15]"  missingMessage="请输入用户名"    value="<?php echo $user['account'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>
    		<tr style="">
			<td align="left" >密&emsp;&emsp;码:</td>
			<td align="left" width="200">
					 <input class="easyui-textbox easyui-validatebox"  name="password"  id="password" type="password" data-options="required:true,novalidate:true" validType="length[2,10]"  missingMessage="请输入密码"    invalidMessage="密码要求大于4个字符，不超过15字符！"  value="" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
				<?php
					if( $user['id']==''){
						echo '<span style="color:#ff2e2e">*</span>';
					}
				?>

             </td>
	    	</tr>
	    	<tr style="">
				<td align="left">确认密码:</td>
				<td align="left">
  					 <input class="easyui-textbox easyui-validatebox"  name="recpwd"  id="recpwd" type="password" data-options="required:true,novalidate:true" validType="eqPassword['#password']"  missingMessage="请输入确认密码" invalidMessage="两次密码输入不一样！"  value="" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
					<?php
					if( $user['id']==''){
						echo '<span style="color:#ff2e2e">*</span>';
					}
					?>
	             </td>
	    	</tr>
	    	  	<tr>

	    		<td>姓&emsp;&emsp;名:</td>
	    		 <td>  <input class="easyui-textbox easyui-validatebox"  type="text" name="nickname" id="nickname"  data-options="required:true,novalidate:true" validType="length[2,10]"  missingMessage="请输入姓名"    invalidMessage="大于2个字符，不超过10字符！" value="<?php echo $user['nickname'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                <span style="color:#ff2e2e">*</span>
                </td>
	    	</tr>

	    	<tr>
				<td>联系手机:</td>
				<td>
		   			  <input type="hidden"  id="oldphone"   value="<?php echo $user['tel'] ?>"  >

		   			   <input class="easyui-textbox easyui-validatebox"  type="text" name="tel"  id="tel" data-options="" validType="checkphone[11]"  missingMessage="请输入手机号"    value="<?php echo $user['tel'] ?>" <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>>
                		<span style="color:#ff2e2e"></span>

	             </td>
	    	</tr>
	    	<tr>
				<td>邮&emsp;&emsp;箱:</td>
				<td>
	    	   			  <input class="easyui-textbox  easyui-validatebox"  type="text" name="email"  id="email"  missingMessage="邮件必须填写" validType="email" invalidMessage="请填写正确的邮件格式"   value="<?php echo $user['email'] ?>" <?php echo $user['email'] ?>"  <?php if($is_readonly == '1'){ echo 'readonly="readonly"';} ?>></input></td>
	             </td>
	    	</tr>



	    	<tr >
				<td>所属公证处:</td>
				<td align="left">
  				<input  class="easyui-combobox" id="department_id"  name="department_id" style="width:150px"
							data-options="
										url:'<?php echo site_url('/admin/department/get_department_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $user['department_id'] ?>',
										panelHeight:'auto',
										editable:false,
										required:true,
										novalidate:true
									"
						><span style="color:#ff2e2e">*</span>
	             </td>
	    	</tr>

	    	<tr>
	    		<td>用&ensp;户&ensp;组:</td>
	    		<td>
 					<input  class="easyui-combobox" id="group_id"  name="group_id" style="width:150px"
							data-options="
										url:'<?php echo site_url('/admin/group/get_auth_group_json') ?>',
										method:'get',
										valueField:'id',
										textField:'text',
										value:'<?php echo $user['group_id'] ?>',
										panelHeight:'auto',
										editable:false,
										required:true,
										novalidate:true
									"
						><span style="color:#ff2e2e">*</span>
			  </td>
	    	</tr>
	    		<tr>
	    		<td>是否禁用:</td>
	    		 <td style="text-align:left">
		            <span class="radioSpan">
		                <input type="radio" name="status" value="0" checked>否</input>
		                <input type="radio" name="status" value="1">是</input>
		            </span>
	   			 </td>
	    	</tr>
			 <tr>
				<td></td>
				<td style="text-align:center;padding:15px">
		   				<span id="msg"></span>
	             </td>
	    	</tr>
		    <tr style="<?php if($is_readonly == '1'){ echo 'display:none;';} ?>">
				<td  colspan="2" style="text-align:center;padding:5px">
	   	   				<a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">提            交</a>
	             </td>
	    	</tr>
	    	 <tr>
				<td></td>
				<td style="text-align:center;padding:5px">
		   				<span id="msg"></span>

	             </td>
	    	</tr>

		</table>
   </form>

	<script type="text/javascript">
	 $(function(){


       });

       	function submitForm(){

       					$('#form1').form('submit',{
							onSubmit:function(){
								var arr=new Array();
						        arr.push(new Array('account','textbox'));
								if($('#user_id').val()==''){
									arr.push(new Array('password','textbox'));
									arr.push(new Array('recpwd','textbox'));
								}
						        arr.push(new Array('nickname','textbox'));
						        arr.push(new Array('email','textbox'));
						        arr.push(new Array('tel','textbox'));
						        arr.push(new Array('department_id','textbox'));
						        arr.push(new Array('group_id','textbox'));
						        if(validate_arr(arr)){
									var url = "<?php echo $action_url ?>";
								     $.ajax({
										   type: "POST",
										   url:url,
										   dataType:"json",
										   data:$("#form1").serialize(),
										   success: function(msg){
											     // alert(msg);
											      var index = parent.layer.getFrameIndex(window.name);
												  parent.layer.msg(msg.info, {
														    icon: 1,  //1 成功 ,0 失败
														    time: 2000　//２　秒
														});
												  $("#iframe_src", parent.document)[0].contentWindow.reload();
												  parent.layer.close(index);

										   }
									});
						        }

							}
					 	});


		}
		//Ajax远程验证使用,/Aassets/js/comm.js
		var checkaccount_url  	= "<?php echo site_url('/admin/user/check_account') ?>";
		var checkaccount_name 	= "账号";
		var checkphone_url    	= "<?php echo site_url('/login/check_phone') ?>";
		var checkphone_name 	= "手机号码";

</script>

</body>
</html>