<script>
$(function(){

	var url  ="<?php echo site_url("/admin/user/get_user_limit_json")?>";
	var ht= $(window).height()-16;

	$('#tt').datagrid({
		    height:ht,
            url:url,
			toolbar: "#tb",
			fit:'true',
			striped:true,
			method: 'post',
			rownumbers: true,
			idField: 'id',
			singleSelect:true,
			fitColumns: true,
			pagination: true,
			pageSize: 20,
			pageList:[20,50,100,200],
			columns:[[
				{field:'account',title:'用户名',width:100,align:'center',editor:'text'},
				{field:'nickname',title:'姓名',width:100,align:'center',editor:'text'},
				{field:'group_name',title:'用户组',width:100,align:'center',editor:'text'},
				{field:'department_name',title:'所属公证处',width:$(this).width() * 0.1,align:'center',editor:'text'},
				{field:'create_time',title:'创建时间',width:$(this).width() * 0.1,align:'center',editor:'text'},
				{field:'edit',title:'编辑',width:40,align:'center',fixed:'true',
					formatter:function(value,row,index){
						 	var a = '<a   class="myedit" href="javascript:void(0)" onclick="edit('+row.id+')"></a>';
						 return a;
					}
				},{field:'del',title:'删除',width:40,align:'center',fixed:'true',
					formatter:function(value,row,index){
						   		var b = '<a   class="mydel" href="javascript:void(0)"  onClick="del('+row.id+','+row.pid+',\''+row.account+'\')"></a>';
						 return b;
					}
				}
				]],
				onLoadSuccess:function(data){
						$('.myedit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
						$('.mydel').linkbutton({plain:true,iconCls:'icon-mydelete',height:20});
				}
	   });


});



function add() {
	var url  = "<?php echo site_url('/admin/user/add') ?>";
	parent.layer.open({
        type: 2,
        title: '添加管理员',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['610px' , '480px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function edit(id) {
	var url  = "<?php echo site_url('/admin/user/edit') ?>/"+id;
	parent.layer.open({
        type: 2,
        title: '修改管理员',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['610px' , '480px'],
        shade: [0.2,'#fff'],
        content: url
    });
}

function del(id,pid,name){

	if(pid ==0){
 			parent.layer.msg("系统管理员不能删除", {
								    icon: 1,  //1 成功 ,0 失败
								    time: 2000　//２　秒
								});
	}else{
		$.messager.confirm('确认提示','你确定要删除账号为 '+name+' 的记录吗？',function(r){
			if (r){
				var url="<?php echo site_url('/admin/user/del') ?>";
				$.ajax({
				   type: "POST",
				   url:url ,
				   data: "id="+id,
				   dataType:"json",
				   success: function(msg){
					   layer.msg(msg.info, {
										    icon: 1,  //1 成功 ,0 失败
										    time: 2000
										  });
					   $('#tt').datagrid('reload');
				   }
				});

			}
		});
	}
}

function my_search() {
	var url = "<?php echo site_url('/home/user/search') ?>/?"+$("#form_search").serialize();
	$('#tt').datagrid({
			url:url,
			pageNumber:1
	}).datagrid("reload");

}

function reload() {
	$('#tt').datagrid("reload");
}

</script>
<table id="tt"></table>
<div id="tb" style="padding:10px;">
	<table width="100%" border="0" cellspacing="3" cellpadding="0">
	  <tr>
	     <td width="85%" align="left">
		  	<form  id="form_search" >
		  	用户名：<input class="easyui-textbox"  type="text" name="account"  id  ="account" data-options="" value=""></input>
		   	姓名:<input class="easyui-textbox"  type="text" name="nickname" id  = "nickname" data-options="" value=""></input>

			&nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-mysearch'"    onclick="my_search()" >搜       索</a>
			</form>
		  </td>
		  <td width="200" align="right">

		  		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add_archive'"    onclick="add()">添加管理员</a>

		  </td>

	  </tr>
	</table>
</div>

</body>
</html>