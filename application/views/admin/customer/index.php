<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Test</title>
</head>
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/default/easyui.css">
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">
<script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>

<body>

<div class="easyui-layout" fit="true">
    <div data-options="region:'north'" style="height:50px">
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td width="85%" align="left">
                    <form  id="form_search" >
                        身份证：<input class="easyui-textbox"  type="text" name="cardId"  style="width:200px" id="cardId" data-options="" value=""></input>
                        &nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-mysearch'"    onclick="my_search()" >搜       索</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" style=""  data-options="iconCls:'icon-mysearch'"    onclick="my_relation_archive()" >查询关联案卷</a>
                    </form>
                </td>
                <td width="200" align="right">


                </td>

            </tr>
        </table>
    </div>

    <div data-options="region:'center',title:'',iconCls:'icon-ok'">


        <iframe id="iframe_src" name="iframe_src"  frameborder="0" style="width:100%; height:1920px"  scrolling="no"></iframe>

    </div>
</div>
<script>
    function my_search(){
        var cardId = $("#cardId").val();
        if(isCardNo(cardId)){
            return  false;
        };
        var url = "/admin/relation/relation/"+cardId+"/";
        $("#iframe_src").attr("src",url);
    }


    function my_relation_archive(){
        var cardId = $("#cardId").val();
        if(isCardNo(cardId)){
            return  false;
        };
        var ids  = "";
        var url = "/admin/Relation/archive_id/"+cardId;
        $.ajax({
            type: "POST",
            url:url,
            dataType:"json",
            async : false,
            data:{},
            success: function(data){
             if(data.status ==1){
                 ids = data.archive_ids;
             }
            }
        });
        relation(ids);
    }


    function relation(ids) {
        var url  = "<?php echo site_url('/admin/relation/getRelationDetail') ?>/"+ids;
        var index =parent.layer.open({
            type: 2,
            title: '关联案卷信息',
            maxmin: true,
            shadeClose: true,  // 点击空白页不关闭窗口
            area : ['100%' , '100%'],
            shade: [0.2,'#fff'],
            content: url
        });
        parent.layer.full(index);
    }


    function isCardNo(card)
    {
        // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(reg.test(card) === false)
        {
            parent.layer.msg("身份证输入不合法", {
                icon: 1,  //1 成功 ,0 失败
                time: 2000　//２　秒
            });
            return  true;
        }
    }

</script>
</body>
</html>