<script src="/assets/js/my.js"></script>
<script>
    $(function(){
        var url  ="<?php echo $action_url ?>";
        var ht= $(window).height()-16;
        $('#tt').datagrid({
            height:ht,
            url:url,
            fit:true,
            striped:true,
            method: 'post',
            rownumbers: true,
            idField: 'id',
            singleSelect:true,
            fitColumns: true,
            pagination: true,
            pageSize: 20,
            pageList:[20,50,100,200],
            columns:[[
                {field:'word_no',title:'公证书字号',align:'center',width:200,fixed:true},
                {field:'c_name',title:'类别',align:'center',width:80,fixed:true},
                {field:'n_name',title:'公证事项',align:'center',width:100,fixed:true},
                {field:'litigant_name',title:'当事人姓名',align:'center',width:100,fixed:true},
                {field:'litigant_card',title:'当事人身份证号码',align:'center',width:150,fixed:true},
                {field:'undertaker',title:'承办人',align:'center',width:120,fixed:true},
                {field:'receive_time',title:'受理日期',align:'center',width:120,fixed:true},
                {field:'over_time',title:'办结日期',align:'center',width:120,fixed:true},
                {field:'archive_time',title:'归档日期',align:'center',width:120,fixed:true},
                {field:'storage_time_id',title:'保存期限',align:'center',width:60,fixed:true,
                    formatter:function(value,row,index){
                        if(value==1){
                            return '永久';
                        }else if(value==2){
                            return '长期';
                        }else if(value==3){
                            return '短期';
                        }else{
                            return '未知';
                        }
                    }
                },
                {field:'box_number',title:'盒号',align:'center',width:40,fixed:true},
                {field:'volume_number',title:'卷号',align:'center',width:40,fixed:true},
                {field:'greffier',title:'公证员',align:'center',width:60,fixed:true},
                {field:'d_name',title:'所属公证处',align:'center',width:100,fixed:true},
                {field:'request',title:'查阅',width:50,align:'center',fixed:'true',
                    formatter:function(value,row,index){;
                        var b = '<div class="edit" onclick="open_pdf('+row.id+')"></div>';
                        return b;
                    }
                },
            ]],
            onLoadSuccess:function(data){
                $('.edit').linkbutton({plain:true,iconCls:'icon-myedit',height:20});
            },
            onDblClickRow: function(index,row){
                open_archive(row.id);
            }
        });


    });



    function add() {
        var url  = "<?php echo site_url('/admin/department/add') ?>";
        parent.layer.open({
            type: 2,
            title: '添加公证处',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['610px' , '450px'],
            shade: [0.2,'#fff'],
            content: url
        });
    }

    function edit(id) {
        var url  = "<?php echo site_url('/admin/department/edit') ?>/"+id;
        parent.layer.open({
            type: 2,
            title: '修改公证处',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['610px' , '450px'],
            shade: [0.2,'#fff'],
            content: url
        });
    }

    function del(id,name){

        $.messager.confirm('确认提示','你确定要删除名称为 '+name+' 的记录吗？',function(r){
            if (r){
                var url="<?php echo site_url('/admin/department/del') ?>";
                $.ajax({
                    type: "POST",
                    url:url ,
                    data: "id="+id,
                    dataType:"json",
                    success: function(msg){
                        layer.msg(msg.info, {
                            icon: 1,  //1 成功 ,0 失败
                            time: 2000
                        });
                    }
                });

            }
        });
    }
    /*
     * 查询档案
     * */
    function open_pdf(id){
        var index = parent.layer.open({
            type: 2,
            title: '档案附件',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['1024px' ,'95%'],
            shade: [0.2,'#fff'],
            content: "<?php echo site_url('admin/archive/get_archive_status')?>?id="+id
        });
        parent.layer.full(index);
    }

    /*
     * 打开档案详情
     * */
    function open_archive(id){
        var url  = "<?php echo site_url('admin/archive/detail') ?>/"+id;
        var index = parent.layer.open({
            type: 2,
            title: '档案明细查询',
            maxmin: true,
            shadeClose: false,  // 点击空白页不关闭窗口
            area : ['800px' , '95%'],
            shade: [0.2,'#fff'],
            content: url
        });
    }
    function my_search() {
        var url = "<?php echo site_url('/home/user/search') ?>/?"+$("#form_search").serialize();
        $('#tt').datagrid({
            url:url,
            pageNumber:1
        }).datagrid("reload");

    }


</script>
<table id="tt"></table>

</body>
</html>