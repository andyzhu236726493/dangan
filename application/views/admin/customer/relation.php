<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Test</title>
</head>
<script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/assets/plugins/echarts/echarts.js"></script>
<script type="text/javascript" src="/assets/plugins/echarts/force.js"></script>


<body>
<div id="main" style="width:100%; height: 860px"></div>
<div id="main" style="width: 100%; height: 1440px"></div>
<div id="ss" ></div>

<script type="text/javascript">
    var cardId = "<?php  echo $cardId; ?>";

    function getNodes(){
        var result  = [];
        var url = "/admin/Relation/getNodes/"+cardId;
        $.ajax({
            type: "POST",
            url:url,
            dataType:"json",
            async : false,
            data:{},
            success: function(data){
                $.each(data, function(i, item){
                    temp = {id:item.id,cardid:item.cardId,category:item.category,name:item.name,lable:item.lable,title:item.title,
                        symbolSize:item.symbolSize,ignore:false,flag:false };
                    result.push(temp);
                })
            }

        });
        return  result ;
    }

    function getLinks(){
        var result  = [];
        var url = "/admin/Relation/getLinks/"+cardId;
        $.ajax({
            type: "POST",
            url:url,
            dataType:"json",
            async : false,
            data:{},
            success: function(data){
                $.each(data, function(i, item){
                    temp =  {source : item.source, target : item.target,name : item.name,lable : item.lable,weight : 20,
                        itemStyle:{
                            normal:{
                                brushType: 'stroke',
                                lineWidth: 1,   // 根据数量动态调整

                                text: item.name,
                                textPosition: 'inside',
                                color: item.color, // 文字颜色
                                textFont: 'normal 12px 宋体'

                            }
                        }
                    };
                    result.push(temp);
                })
            }
        });
        return  result ;
    }


    require([ "echarts", "echarts/chart/force"], function(ec) {
        var myChart = ec.init(document.getElementById('main'), 'macarons');
        var option = {
            tooltip : {
                trigger: 'item',

                borderRadius : 0,
                borderWidth: 0,
                padding: 0,    // [5, 10, 15, 20]
                textStyle : {
                    color: 'white',
                    decoration: 'none',
                    fontFamily: 'Verdana, sans-serif',
                    fontSize: 20,
                    fontWeight: 'bold'
                },
                formatter: function (param) {
                    var data = param.data;
                    var links = option.series[0].links;
                    var nodes = option.series[0].nodes;
                    if ( data.source != null  && data.target != null ) { //点击的是边
                        var sourceNode = nodes.filter(function (n) {return n.name == data.source})[0];
                        var targetNode = nodes.filter(function (n) {return n.name == data.target})[0];
                        if(data.lable) {
                            return data.lable;
                        }else{
                            return null;
                        }
                        //console.log("选中了边 " + sourceNode.name + ' -> ' + targetNode.name + ' (' + data.weight + ')');
                    } else { // 点击的是点
                        return data.lable;
                    }

                }
            },
            series : [ {
                type : 'force',
                name : "人物关系",
                linkSymbol: 'arrow',
                steps: 10,
                coolDown: 0.9,
                itemStyle : {
                    normal : {
                        label : {show : true},
                        nodeStyle : {
                            brushType : 'both',
                            borderColor : 'rgba(255,215,0,0.4)',
                            borderWidth : 1
                        }
                    }
                },
                categories : [
                    {
                        name: '0',
                        itemStyle: {
                            normal: {
                                color : 'blue',
                                label: {
                                    textStyle: {
                                        color: 'blue',
                                        fontSize: 24,
                                    }
                                }
                            }
                        }
                    },
                    {name : '1',
                        itemStyle: {
                            normal: {
                                color : 'blue',
                                label: {
                                    textStyle: {
                                        color: 'blue',
                                        fontSize: 24,
                                    }
                                }
                            }
                        }
                    }, {name : '2',
                        itemStyle: {
                            normal: {
                                color : 'blue'
                            }
                        }
                    },{name : '3',
                        itemStyle: {
                            normal: {
                                color : 'blue',
                                label: {
                                    textStyle: {
                                        color: 'blue',
                                        fontSize: 24,
                                    }
                                }
                            }
                        }
                    }
                    ,{name : '4',
                        itemStyle: {
                            normal: {
                                color : '#ff0000',
                                label: {
                                    textStyle: {
                                        color: 'white',
                                        fontSize: 18,
                                    }
                                }
                            }
                        }
                    }
                ],
                nodes :getNodes(),
                links : getLinks()
            } ]
        };
        myChart.setOption(option);
        var ecConfig = require('echarts/config');
        function openOrFold(param) {
            var option = myChart.getOption();
            var nodesOption = option.series[0].nodes;
            var linksOption = option.series[0].links;
            var data = param.data;
            var linksNodes = [];

            var categoryLength = option.series[0].categories.length;

            if (data.category == (categoryLength - 1)) {
                alert(data.label);
            }

            if (data != null && data != undefined) {
                if (data.flag) {

                    for ( var m in linksOption) {

                        if (linksOption[m].target == data.id) {
                            linksNodes.push(linksOption[m].source);
                        }
                    }
                    if (linksNodes != null && linksNodes != undefined) {
                        for ( var p in linksNodes) {
                            nodesOption[linksNodes[p]].ignore = false;
                            nodesOption[linksNodes[p]].flag = true;
                        }
                    }
                    nodesOption[data.id].flag = false;
                    myChart.setOption(option);
                } else {

                    for ( var m in linksOption) {

                        if (linksOption[m].target == data.id) {
                            linksNodes.push(linksOption[m].source);
                        }
                        if (linksNodes != null && linksNodes != undefined) {
                            for ( var n in linksNodes) {
                                if (linksOption[m].target == linksNodes[n]) {
                                    linksNodes.push(linksOption[m].source);
                                }
                            }
                        }
                    }
                    if (linksNodes != null && linksNodes != undefined) {
                        for ( var p in linksNodes) {
                            nodesOption[linksNodes[p]].ignore = true;
                            nodesOption[linksNodes[p]].flag = true;
                        }
                    }
                    nodesOption[data.id].flag = true;
                    myChart.setOption(option);
                }
            }
        }
        var TimeFn = null;
        function openClick(param) {  //该事件会提示节点间关系
            clearTimeout(TimeFn);
            //执行延时
            var data = param.data;

            if ( data.source == null  && data.target == null ) { //点击的不是边
                TimeFn = setTimeout(function(){
                        detail(data.cardid,data.title);

                },300);
            }

        }
        function openDbClick(param) {  //该事件会提示节点间关系
            clearTimeout(TimeFn);
            var data = param.data;
            if ( data.source == null  && data.target == null ) { //点击的不是边
                var url = "/admin/relation/relation/" + data.cardid + "/";
                location.href = url;
            }
        }
        myChart.on(ecConfig.EVENT.DBLCLICK, openDbClick);
        myChart.on(ecConfig.EVENT.CLICK, openClick);
    });

    function detail(cardid,title) {

        var url  = "<?php echo site_url('/admin/relation/getDetail') ?>/"+cardid;
        var index = parent.parent.layer.open({
            type: 2,
            title: title+' 关联案卷信息',
            maxmin: true,
            shadeClose: true,  // 点击空白页不关闭窗口
            area : ['100%' , '100%'],
            shade: [0.2,'#fff'],
            content: url
        });
        parent.parent.layer.full(index);
    }

</script>

</body>
</html>