	<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
    <p style="margin:20px">
		<form id= "uploadForm" action= "" method= "post" enctype ="multipart/form-data">
		<table cellpadding="5" border="0">
			 <tr>
				<td style="width:80px" >选择文件:</td>

				<td>
		   			  <input type="hidden"  name="id"    value=""  >

					<input class="easyui-filebox" name="file" data-options="prompt:'请选择文件...'" style="width:100%">
                		<span style="color:#ff2e2e">*</span>

	             </td>
	    	</tr>

		    <tr style="">
				<td  colspan="2" style="text-align:center;padding:5px">
	   	   				<a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">提            交</a>
	             </td>
	    	</tr>


		</table>
   </form>

	<script type="text/javascript">

       	function submitForm(){
			var formData = new FormData($( "#uploadForm" )[0]);
			$.ajax({
				url: '/admin/customer/uploads' ,
				type: 'POST',
				data: formData,
				dataType:"json",
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function (data) {
					 if(data.status ==1){
						 parent.layer.msg("导入成功", {
							 icon: 1,  //1 成功 ,0 失败
							 time: 2000
						 });
					 };
					parent.layer.closeAll();
				},
				error: function (data) {
					layer.msg("导入失败", {
						icon: 1,  //1 成功 ,0 失败
						time: 2000
					});
				}
			});


		}


</script>

</body>
</html>