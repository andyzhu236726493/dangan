	<link 	type="text/css"	rel="stylesheet"	href="/assets/css/default.css" />
	    <p style="margin:20px">
		<div style="padding:10px 60px 20px 60px">
	     <form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
	    	<table cellpadding="5">
	    		<tr>
	    			<td>用户组:</td>
	    			<td>
	    			  <input type="hidden"  name="id"      value="<?php echo $group['id'] ?>"  >
    	   			  <input type="hidden"  id="oldname"   value="<?php echo $group['title'] ?>"  >
	    			  <input class="easyui-textbox easyui-validatebox" type="text"  id="title" name="title" validType="checkname[3,15]"   missingMessage="请输入用户组"  data-options="required:true"  value="<?php echo $group['title'] ?>" ></input></td>
	    		</tr>
				<tr>
		    		<td>上级用户组:</td>
		    		<td>
	                  <input id="pid"  name="pid" class="easyui-combotree easyui-validatebox" style=""    data-options="required:true,novalidate:true"  missingMessage="请选择父用户组"  url=""   valueField="id" textField="name" >
				  	</td>
	    		</tr>
	    		<tr>
	    		<td>是否禁用:</td>
	    		 <td style="text-align:left">
		            <span class="radioSpan">
		                <input type="radio" name="status" value="1" <?php  if($group["status"] ==1){echo 'checked';} ?>>否</input>
		                <input type="radio" name="status" value="0" <?php  if($group["status"] ==0){echo 'checked';} ?>>是</input>

		            </span>
       			 </td>
	    	   </tr>
	    		<tr>
	    			<td>备注:</td>
	    			<td><input class="easyui-textbox" name="remark" data-options="multiline:true" style="height:60px" value="<?php echo $group['remark'] ?>"></input></td>
	    		</tr>
	    		 <tr style="<?php if($is_readonly == '1'){ echo 'display:none;';} ?>">
    			<td  colspan="2" style="text-align:center;padding:5px">
       	   				<a href="javascript:void(0)" class="easyui-linkbutton c9" style="width:80%" onclick="submitForm()">提            交</a>
                 </td>
	    	    </tr>
	    	</table>
	    </form>

	    </div>

	  <script>

		 $(function(){
			var url  =  "<?php echo site_url('/home/common/get_auth_group_tree_json')."/".$group['id'] ?>";
			
			$('#pid').combotree({
				url:url,
				method:'get',
				valueField:'id',
				textField:'text',
				cascadeCheck:true,

			});

			<?php  if($edit_type == "edit") {?>   // 编辑
			    $("#pid").combotree('setValue',<?php echo $group['pid'] ?>);
			<?php  }  ?>

			$('#title').textbox('textbox').focus();

	      });

			function submitForm(){
				$('#form1').form('submit',{
					onSubmit:function(){
							var arr=new Array();
					        arr.push(new Array('title','textbox'));
					        arr.push(new Array('pid','textbox'));
					        if(validate_arr(arr)){
								var url = "<?php echo $action_url ?>";
			                     $.ajax({
									   type: "POST",
									   url:url,
									   dataType:"json",
									   data:$("#form1").serialize(),
									   success: function(msg){
										  var index = parent.layer.getFrameIndex(window.name);
										  parent.layer.msg(msg.info, {
												    icon: 1,  //1 成功 ,0 失败
												    time: 2000
										  });
										  parent.$("#myframe_35")[0].contentWindow.reload();
										  parent.layer.close(index);
									   }
								});
					        }

					}
				});

			}

			//Ajax远程验证使用 checkaccount(),/Aassets/js/comm.js
			var checkname_url  ="<?php echo site_url('/home/group/check_group') ?>";
			var checkname_name ="用户组";
	</script>
</body>
</html>