<script>
$(function(){
	 	var url  ="<?php echo site_url("home/group/get_auth_group_limit_json")?>";
	    var ht= $(window).height()-16;
		$(function(){
			$('#tg').treegrid({
				height:ht,
				url:url,
				fit:'true',
				method: 'post',
				rownumbers: true,
				idField: 'id',
				treeField: 'name',
				toolbar: "#tb",
				animate: true,
				collapsible: true,
				fitColumns: true,
				columns:[[
					{field:'name',title:'名称',width:$(this).width() * 0.3,align:'center',editor:'text'},
					{field:'company_name',title:'企业',width:$(this).width() * 0.3,align:'center',editor:'text'},
					{field:'status',title:'状态',width:$(this).width() * 0.3,align:'center',editor:'text'},
					{field:'edit',title:'编辑',width:40,align:'center',fixed:'true',
						formatter:function(value,row,index){
					    <?php if($auth["edit"])  { ?>
					     if(row.pid == 0) {
					     	var a = '<a   class="dis_edit" href="#"    onClick="edit('+row.id+',\''+row.pid+'\')"></a>';
					     }else{
					     	  var a = '<a   class="edit" href="#"    onClick="edit('+row.id+',\''+row.pid+'\')"></a>';
					     }
						 return a;
						 <?php } ?>
					  }
					},
					{field:'auth',title:'分配权限',width:60,align:'center',fixed:'true',
						formatter:function(value,row,index){;
						var uid =  "<?php echo $this->uid ?>";
						 <?php if($auth["auth"])  { ?>
							 if(row.pid == 0 && uid != 1) {
							 	var b = '<a   class="dis_auth" href="#" 	onclick="auth('+row.id+',\''+row.pid+'\')"></a>';
							 }else{
							 	var b = '<a   class="auth" href="#" 	onclick="auth('+row.id+',\''+row.pid+'\')"></a>';
							 }
							 return b;
						 <?php } ?>
					  }
					},
					{field:'del',title:'删除',width:50,align:'center',fixed:'true',
						formatter:function(value,row,index){
						  <?php if($auth["del"])  { ?>
							if(row.pid == 0) {
								var c = '<a   class="dis_del" href="#"  	onClick="del('+row.id+','+row.pid+',\''+row.name+'\')"></a>';
							}else{
							 	var c = '<a   class="del" href="#"  	onClick="del('+row.id+','+row.pid+',\''+row.name+'\')"></a>';
							}
							 return c;
						 <?php } ?>
					  }
					}
				 ]],
					onLoadSuccess:function(data){
								$('.edit').linkbutton({plain:true,iconCls:'icon-edit',height:20});
								$('.dis_edit').linkbutton({disabled:true,plain:true,iconCls:'icon-edit',height:20});
								$('.auth').linkbutton({plain:true,iconCls:'icon-auth',height:20});
								$('.dis_auth').linkbutton({disabled:true,plain:true,iconCls:'icon-auth',height:20});

								$('.del').linkbutton({plain:true,iconCls:'icon-remove',height:20});
								$('.dis_del').linkbutton({plain:true,iconCls:'icon-remove',height:20});
								$('.dis_del').linkbutton('disable','true');

					}
			});

		});

	   parent.layer.config({
    		extend: ['skin/seaning/style.css'], //加载新皮肤
    		skin: 'layer-ext-seaning' //一旦设定，所有弹层风格都采用此主题。
		});

});



function add() {
	var url  = "<?php echo site_url('home/group/add') ?>";
     parent.layer.open({
        type: 2,
        title: '添加用户组',
        maxmin: true,
        shadeClose: false,  // 点击空白页不关闭窗口
        area : ['510px' , '410px'],
        shade: [0.2,'#fff'],
        content: url
    });

}

function edit(id,pid) {
	var uid =  "<?php echo $this->uid ?>";
	if(pid ==0 && uid != 1){
 			parent.layer.msg("系统管理员不能编辑", {
								    icon: 1,  //1 成功 ,0 失败
								    time: 2000　//２　秒
								});
	}else{
		var url  = "<?php echo site_url('home/group/edit') ?>/"+id;
		parent.layer.open({
	        type: 2,
	        title: '修改用户组',
	        maxmin: true,
	        shadeClose: false,  // 点击空白页不关闭窗口
	        area : ['510px' , '410px'],
	        shade: [0.2,'#fff'],
	        content: url
    	});

	}
}

function auth(id,pid) {
	var uid =  "<?php echo $this->uid ?>";
	if(pid ==0 && uid != 1){
 			parent.layer.msg("系统管理员不能编辑权限", {
								    icon: 1,  //1 成功 ,0 失败
								    time: 2000　//２　秒
								});
	}else{
		var url  = "<?php echo site_url('home/group/auth') ?>/"+id;
		parent.layer.open({
		        type: 2,
		        title: '分配权限',
		        maxmin: true,
		        shadeClose: false,  // 点击空白页不关闭窗口
		        area : ['610px' , '610px'],
		        shade: [0.2,'#fff'],
		        content: url
	    });
	}

}

function del(id,pid,name){
	if(pid ==0){
 			parent.layer.msg("系统管理员不能删除", {
								    icon: 1,  //1 成功 ,0 失败
								    time: 2000　//２　秒
								});
	}else{
		$.messager.confirm('确认提示','你确定要删除账号为 '+name+' 的记录吗？',function(r){
			if (r){
				var url="<?php echo site_url('home/group/del') ?>";
				$.ajax({
				   type: "POST",
				   url:url ,
				   data: "id="+id,
				   dataType:"json",
				   success: function(msg){
					     layer.msg(msg.info, {
										    icon: 1,  //1 成功 ,0 失败
										    time: 2000
										  });
						   reload();

				   }
				});

			}
		});
	}
}

function my_search() {
	var url = "<?php echo site_url('home/group/search') ?>/?"+$("#form_search").serialize();
	$('#tt').datagrid({
			url:url,
			pageNumber:1
	}).datagrid("reload");

}

function reload() {
	$('#tg').treegrid('reload');
}

</script>
<table id="tg"></table>
<div id="tb" style="padding:5px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	     <td width="85%" align="left">
		  </td>
		  <td width="200" align="right">
		  <?php if($auth["add"])  { ?>
		  <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-addgroup'"    onclick="add()">添加用户组</a>
		  <?php } ?>
		  </td>

	  </tr>
	</table>
</div>
</body>
</html>