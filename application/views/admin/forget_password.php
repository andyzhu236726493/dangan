<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/default/easyui.css">
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/icon.css">
<link 	type="text/css"	rel="stylesheet" 	href="/assets/plugins/jquery-easyui-1.4.5/themes/color.css">
<script type="text/javascript" src="/assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/assets/js/comm.js?t=<?php echo time()?>"></script>
<script type="text/javascript" src="/assets/plugins/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/assets/plugins/layer/layer.js"></script>
<script type="text/javascript" src="/assets/js/ajaxfileupload.js"></script>

<title>公证数据应用服务平台</title>
<link rel="shortcut icon" type="image/x-icon" href="/assets/images/ico/favicon.ico" />

<style type="text/css">
body {background:url(/assets/images/login.jpg)}
.container {
	border:0px solid #d60;
	height: 600px;
	width: 100%;
	margin-top: -300px;
	margin-left: -10px;
	position: absolute;
	top: 50%;
}

.main {
	border:0px solid #000000;
	height: 600px;
	width:  1000px;
	margin-top: -300px;
	margin-left: -500px;
	position: absolute;
	top: 50%;
	left: 50%;
}


.main_top {
	border:0px solid blue;
	height: 120px;
	width:  1000px;
	position: relative;
	top: 5px;
	left: 0px;
}

.logo{
	border:0px solid blue;
	height: 30px;
	width:  300px;
	margin-left: -180px;
	position: absolute;
	left: 50%;
	top: 40%;
}
.register_logo{
	border:0px solid red;
	height:60px;
	width: 1000px;
	position: absolute;
	top: 120px;
	left: 0px;
}

.register_logo_left{
	border:0px solid green;
	height:30px;
	width: 300px;
	position: absolute;
	top: 25px;
	left: 30px;
	float:left;
}
.register_logo_right{
	border:0px solid green;
	height:30px;
	width: 300px;
	padding-top:6px;
	position: absolute;
	top: 25px;
	left: 750px;
	float:left;
	z-index:10
}

.register_logo_right .login{
    font-weight:800;
    color:#B4B4B4
}

.main_center{
	border:0px solid yellow;
	height: 450px;
	width:  1000px;
	margin-top: 5px;
	margin-left:0px;
	position: relative;
	top: 15px;
	left:0px;
	z-index:2;
	overflow: hidden;
	background:url(/assets/images/registerbox.png)
}


.main_left{

	border:0px solid yellow;
	height: 400px;
	width:  500px;
	margin-top: -230px;
	margin-left: 10px;
	position: absolute;
	top: 50%;
	left: 0%;
}
.main_right1{
	background:url(/assets/images/forget.jpg);
	border:0px solid yellow;
	height: 350px;
	width:  580px;
	margin-top: -180px;
	margin-left: -90px;
	position: absolute;
	top: 50%;
	left: 50%;
}




.line {
	border:0px solid blue;
	height: 1px;
	width: 100%;
	margin-top: 180px;
	position: absolute;
	top: 50%;
	background:url(/assets/images/line.png);
}
.footer {
	border:0px solid blue;
	height: 150px;
	width: 900px;
	margin-top: 190px;
	margin-left: -450px;
	position: absolute;
	top: 50%;
	left: 50%;
}

.c10,.c10:hover{
	color: #333;
	border-color: #77C0F3;
	background: #77C0F3;
	background: -webkit-linear-gradient(top,#77C0F3 0,#77C0F3 100%);
	background: -moz-linear-gradient(top,#77C0F3 0,#77C0F3 100%);
	background: -o-linear-gradient(top,#77C0F3 0,#77C0F3 100%);
	background: linear-gradient(to bottom,#77C0F3 0,#77C0F3 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#ffab2e,endColorstr=#77C0F3,GradientType=0);
}
a.10:hover{
	background: #77C0F3;
	filter: none;
}


a{
  color: #77C0F3;
  text-decoration:none
}
td{
	 font-weight:800;
     color:#B4B4B4
}

/*
.validatebox-invalid{
    border-color: #B4B4B4;
    background-color: #ffffff
}
.textbox-invalid{
	border-color: #B4B4B4;
    background-color: #ffffff
}
*/
</style>
</head>

<body>
<div class="container">

		<div class="main">
				<div class="main_top">
					<div class="logo">
					  	<img  width="460"; height="80" src="/assets/images/logo.png">
					  </div>
					<div class="register_logo">
						<div class="register_logo_left">
								<img width="143"; height="27" src="/assets/images/forget_password_log.png">
						</div>
						<div class="register_logo_right">
						  <span class="login">如你已拥有账户，即可在此<a href="<?php  echo  site_url('/admin') ?>"  >登录</a></span>
						</div>
					</div>
			  </div>

			  <div class="main_center">
					<div class="main_left">
			  			<div style="margin:50px 0;"></div>
						<form id="form1" name="form1" class="easyui-form" method="post" data-options="novalidate:true" action ="">
						<table cellpadding="5">
							 <caption style="font-size:20px"></caption>

							<tr>
					    		<td align="left">手机号:</td>
					    		<td align="left">
			                	<input class="easyui-textbox easyui-validatebox"  id="phone"  name="phone"   style="width:240px;height:28px"  data-options="required:true" validType="mobile"   value="" ></input>
							  	<font style="color:#ff2e2e">*</font>
							  	</td>
					    	</tr>
					    	<tr >
				    			<td align="left" >新密码:</td>
				    			<td align="left" >
				    	   				<input name="password"  id="password" type="password" class="easyui-textbox easyui-validatebox"  style="width:240px;height:28px" data-options="required:true"    validType="length[4,30]" invalidMessage="大于4个字符，不超过10字符！" />
										<font style="color:#ff2e2e">*</font>
				                 </td>
					    	</tr>
					    	<tr >
				    			<td align="left">确认密码:</td>
				    			<td align="left">
				    	   			 <input name="recpwd"  id="recpwd" type="password" class="easyui-textbox easyui-validatebox"  style="width:240px;height:28px" data-options="required:true"    validType="eqPassword['#password']" />
				                	<font style="color:#ff2e2e">*</font>
				                </td>
					    	</tr>
							<tr>
					    		<td align="left">验证码:</td>
					    		<td align="left">
				                 <input class="easyui-textbox"  id="code"  name="code" style="width:120px;height:28px"  value="" ></input>
								 <a id="btnSendCode" href="javascript:void(0)" style="width:110px; font-weight:800;color:#fff" class="easyui-linkbutton c10" data-options=""    onclick="sendMessage()"">发送验证码</a>

							    </td>
					    	</tr>
						   <tr>
				    			<td  colspan="2" style="text-align:center;">

				       	   				<a href="javascript:void(0)" style="width:80%; font-weight:800;color:#fff" class="easyui-linkbutton c10" data-options=""    onclick="register()">提            交</a>
				       	   				<!--
				       	   				<a href="javascript:void(0)" style="width:80%" class="easyui-linkbutton" data-options=""    onclick="get_code()">获取code</a>
				       	   				-->
				                 </td>
					    	</tr>


						</table>
					   </form>

			  	 </div>
				 <div class="main_right"> </div>
			  </div>

	    </div>
</div>

<script type="text/javascript">
 $(function(){
      $('#phone').textbox('textbox').focus();
      //$('.validatebox-text').bind('blur', function () { $(this).validatebox('enableValidation').validatebox('validate'); });

 });


var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数

function sendMessage() {
				var arr=new Array();
		        arr.push(new Array('phone','textbox'));
		        arr.push(new Array('password','textbox'));
		        arr.push(new Array('recpwd','textbox'));
				if(validate_arr(arr)){
					curCount = count;
					var url   = "<?php echo site_url('/admin/login/send_msg') ?>";
					var phone = $("#phone").val();
					$('#btnSendCode').linkbutton({disabled:true,text:curCount + "秒内重发"});
					//$("#btnSendCode").attr("disabled", "true");
	     			//$("#btnSendCode").val( curCount + "秒内重发");
					InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
					 $.ajax({
						   type: "POST",
						   url:url,
						   dataType:"json",
						   data:{phone:phone},
						   success: function(msg){
									if(msg.status ==1 ){
										 show_msg("验证码发送成功");
										 addCookie("code",msg.info,60);//添加cookie记录,有效时间60s
									}else{
										show_msg("验证码发送失败");
									}
						   }
					});
				}

}

function clearCode() {
	$("#mycode").attr("value","");
}

//timer处理函数
function SetRemainTime() {
            if (curCount == 0) {
                window.clearInterval(InterValObj);//停止计时器
                 $('#btnSendCode').linkbutton({disabled:false,text:"重新发送验证码"});
            }
            else {
                curCount--;
                $('#btnSendCode').linkbutton({text:curCount + "秒内重发"});
            }
 }
 function addCookie(name,value,expiresHours){
	  var cookieString=name+"="+escape(value);
	  //判断是否设置过期时间,0代表关闭浏览器时失效
	  if(expiresHours>0){
	    var date=new Date();
	    date.setTime(date.getTime()+expiresHours*1000);
	    cookieString=cookieString+";expires=" + date.toUTCString();
	  }
	    document.cookie=cookieString;
}
//修改cookie的值
function editCookie(name,value,expiresHours){
	  var cookieString=name+"="+escape(value);
	  if(expiresHours>0){
	   var date=new Date();
	   date.setTime(date.getTime()+expiresHours*1000); //单位是毫秒
	   cookieString=cookieString+";expires=" + date.toGMTString();
	  }
	   document.cookie=cookieString;
}
//根据名字获取cookie的值
function getCookieValue(name){
	   var strCookie=document.cookie;
	   var arrCookie=strCookie.split("; ");
	   for(var i=0;i<arrCookie.length;i++){
		    var arr=arrCookie[i].split("=");
		    if(arr[0]==name){
		     return unescape(arr[1]);
		     break;
		    }else{
		       return "";
		       break;
		     }
	   }
}
</script>

<script>

function get_code(){

	var send_code  = getCookieValue("code");
	alert(send_code);
}




function register(){
				var input_code   = $("#code").val();
				var send_code    = getCookieValue("code");
			    $('#form1').form('submit',{
					onSubmit:function(){
						var arr=new Array();
				        arr.push(new Array('phone','textbox'));
				        arr.push(new Array('password','textbox'));
				        arr.push(new Array('recpwd','textbox'));
						if(validate_arr(arr)){
							if(input_code != "" &&  send_code != "" && input_code == send_code) {
									var url = "<?php echo site_url('/admin/login/reset_password') ?>";
				                     $.ajax({
										   type: "POST",
										   url:url,
										   dataType:"json",
										   data:$("#form1").serialize(),
										   success: function(msg){
												if(msg.status == 1) {
												     $.messager.confirm('提示', "密码修改成功,马上去登录", function(r){
														if (r){
															location.href = "<?php echo site_url('/admin') ?>";
														}
													});

												}else{
													 show_msg(msg.info);
												}
										   }
									});

							}else{
								show_msg("验证码错误");
							}
						}
					}
				});


}
</script>
</body>
</html>