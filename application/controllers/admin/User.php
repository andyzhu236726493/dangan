<?php
/**
 * Controller Approve
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends Admin_Controller {
	private   $list 	="";
	function __construct()
    {
         parent::__construct();
         $this->load->model("user_model");
         if($this->group_id !=1) {
         	echo "只有系统管理才可以访问";
         	die;
         }
    }


    public function index()

    {
       $this->load->view('admin/common/header');
       $this->load->view('admin/user/list');
    }

   public function ok()
    {

		$this->user_info  	= $this->session->userdata('user_info');
		echo $this->group_id;
		p($this->user_info);

    }


	public function get_user_limit_json($condition="")
	{
		$page 	= isset($_POST['page']) ? intval($_POST['page']) : 1;
    	$rows 	= isset($_POST['rows']) ? intval($_POST['rows']) : 20;
    	$sort 	= isset($_POST['sort']) ? strval($_POST['sort']) : "a.id";
    	$order 	= isset($_POST['order']) ? strval($_POST['order']) : 'desc';
    	$offset = ($page-1)*$rows;
    	$result 	= $this->user_model->get_user_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->user_model->get_user_rows($condition);
    	$arr 		= array(
			    			"total"=> $total,
			    			"rows" => $result
			    		);

    	$json_str = json_encode($arr);
    	echo $json_str;
	}


   public function add()
    {

       $data['title'] 			= "添加管理员";
	   $data['action_url']		= site_url("/admin/user/add");
	   $data['is_readonly']  	= 0;        //1 只读
       $data["user"] 			= $this->user_model->get_user_fields();
       $data["edit_type"] 		= "add";

       if($this->input->is_ajax_request()) {  // 是否ajax提交

       		$this->data = $this->input->post();
	        $this->data["password"] 	= md5($this->data["password"]);
			$this->data["create_time"]  = time();
			$group_id					= $this->data["group_id"];
	        unset($this->data["id"]);
	        unset($this->data["group_id"]);
	        unset($this->data["recpwd"]);
	        $uid 			=  $this->table_model->add("user",$this->data) ;

			$group_access      = array("uid"=>$uid,"group_id"=>$group_id);
	        $this->table_model->add("auth_group_access",$group_access) ;

	        if($uid ) {
	       	 	$this->success("添加成功");
	        }else{
	       	 	$this->error("添加失败");
	        }
       }else{
       	   $this->load->view('admin/common/header');
           $this->load->view('admin/user/edit',$data);

       }



    }

    public function edit($id = 0)
    {
	    $condition      		= array("a.id" => $id);
        $data["user"]  			= $this->user_model->get_user_info($condition);
        $data['title'] 			= "修改管理员";
		$data['action_url']		= site_url("/admin/user/edit");
		$data['is_readonly']  	= 0;        //1 只读
		$data["edit_type"] 		= "edit";
		if($this->input->is_ajax_request()) {  // 是否ajax提交
		    $this->data = $this->input->post();
	        $group_id					= $this->data["group_id"];
            $user_id            		= $this->data["id"];
			if($this->data["password"]==''){
				unset($this->data["password"]);
			}else{
				$this->data["password"] 	= md5($this->data["password"]);
			}
	        $condition  				= array("id" => $user_id);
			unset($this->data["id"]);
			unset($this->data["group_id"]);
			unset($this->data["recpwd"]);
	        $result 					= $this->table_model->save("user",$condition,$this->data) ;
	        $condition  				= array("uid" => $user_id);
	        $group_access      			= array("group_id"=>$group_id);
	        $this->table_model->save("auth_group_access",$condition,$group_access) ;
	        if($result) {
	       	 $this->success("修改成功");
	        }else{
	       	 $this->error("修改失败");
	        }
		}else{
			$this->load->view('admin/common/header');
       		$this->load->view('admin/user/edit',$data);
		}


    }


    public function detail($id = 0)
    {
        $this->check_access_ac("user/index");
        $condition      		= array("a.id" => $id);
        $data["user"]  			= $this->user_model->get_user_info($condition);
        $data['title'] 			= "修改管理员";
		$data['action_url']		= site_url("/home/user/edit");
		$data['is_readonly']  	= 1;        //1 只读
		$data["edit_type"] 		= "edit";
        $this->load->view('common/header');
        $this->load->view('user/edit',$data);

    }

    public function del()
    {
	    if($this->input->post("id")){
		    $condition      		= array("id" => $this->input->post("id"));
	        if($this->table_model->del("user",$condition)){
	        	 $this->success("删除成功");
	        }else{
	        	 $this->error("删除失败");
	        }
	    }else{
	    	$this->error("非法访问");
	    }

    }

    public function search(){

       $this->check_access_ac("user/index");
       $para  		= $this->input->get();
	   $condition 	= "";
       if($para) {
	   	   if($para["account"] !="") {
	        	$condition["where"]["a.account"]  		= trim($para["account"]);
	        }
	        if($para["nickname"] !="") {
	        	$condition["like"]["a.nickname"]  		= trim($para["nickname"]);
	        }
	       $this->get_user_limit_json($condition);
       }
     }

    public function check_account() {

		if(!$this->input->post("name")){
			die("error");
		}
		$condition  	=  array("account"=>trim($this->input->post("name")));
        $result = $this->table_model->get_array_one("user",$condition);
		if(empty($result)){
			echo "ok";
		}else{
			echo "error";
		}
    }






}

/* End of file auth.php */
/* Location: ./application/controllers/mobileservice/auth.php */
