<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/18
 * Time: 14:36
 * 档案类
 */
class 	Notary_matter extends MY_Controller{
   public function __construct(){
        parent::__construct();
        $this->load->model("notary_matter_model");
   }

   public function index()
   {
       $this->load->view('admin/common/header');
       $this->load->view('admin/notary_matter/list');

   }



	public function get_notary_matter_tree_limit_json($condition="")
	{

    	$result 	= $this->notary_matter_model->get_notary_matter_tree_limit_json($condition);

        //p($result);
    	$json_str = json_encode($result);
    	echo $json_str;
	}

	public function add(){

	}

   public function edit($id=1)
   {

   	    if($this->input->is_ajax_request()) {  // 是否ajax提交
   	    	$this->data = $this->input->post();
   	    	$field 		=  array();
   	    	$i			= 0;
   	    	foreach($this->data["name"]  as $row){
   	    		$field[] = array("name"=>$row,"text"=>$this->data["text"][$i],"length"=>$this->data["length"][$i]);
   	    		$i++;
   	    	}

            $field 				= serialize($field);
   	    	$condition			= array("id" => $this->data["id"]);
	        $data      			= array(
										"field"			=> $field,
										"single_select"	=> $this->data["single_select"],
										"is_person"		=> $this->data["is_person"]
										);

	        $result				= $this->table_model->save("notary_matter",$condition,$data) ;
	        if($result) {
	       	 	$this->success("修改成功");
	        }else{
	       	 	$this->error("修改失败");
	        }

   	    }else{

			$condition		= array("id" => $id);
   	    	$data 			= $this->table_model->get_array_one("notary_matter",$condition);
	   	   	if(is_null($data["field"])){
	   	   		$data["field"]  =  array();
	   	   	}else{
	   	   		$data["field"]	=  unserialize($data["field"]);
	   	   	}
	   	   	$data['action_url']		= site_url("/admin/notary_matter/edit");
	       	$this->load->view('admin/common/header');
	       	$this->load->view('admin/notary_matter/edit',$data);
   	    }
   }


     public function get_notary_matter_json()
    {
		$result = $this->notary_matter_model->get_notary_matter_json();
    	echo $result;
    }





    /*
     * 年度业务量统计
     * */
    public function business_statistics(){
        echo '年度业务量统计';
    }

    /*
     * 公证类别
     * */
    public function notarial_category(){
        echo '公证类别';
    }

    /*
     * 公证事由分析
     * */
    public function notarial_reason(){
        echo '公证事由分析';
    }

    /*
     * 实时数据监控
     * */
    public function real_time_data(){
        echo '实时数据监控';
    }


	/*
	 * 获取公证事由json
	 * */
	public function get_notary_matter_list(){
		$res=$this->notary_matter_model->get_notary_matter_list();
		array_unshift($res,array('id'=>0,'text'=>'全部'));
		echo json_encode($res);
	}

}