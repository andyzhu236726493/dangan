<?php
/**
 * Controller Auth
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group extends Admin_Controller {

	private   $list 	="";
	function __construct()
    {
         parent::__construct();
         $this->load->model("auth_group_model");
         $this->load->model("auth");
    }

    public function index()
    {

       $data  =  array();
       $rules =   array(
						"list"  => "group/index",
						"add"   => "group/add",
						"edit"  => "group/edit",
						"del"   => "group/del",
						"auth"  => "group/auth",
						);
       $data["auth"] = $this->check_access($rules);
       $this->load->view('/admin/common/header',$data);
       $this->load->view('/admin/group/list',$data);
    }

	public function get_auth_group_limit_json($condition="")
	{

		$page 	= isset($_POST['page']) ? intval($_POST['page']) : 1;
    	$rows 	= isset($_POST['rows']) ? intval($_POST['rows']) : 20;
    	$sort 	= isset($_POST['sort']) ? strval($_POST['sort']) : "a.id";
    	$order 	= isset($_POST['order']) ? strval($_POST['order']) : 'desc';
    	$offset = ($page-1)*$rows;

    	$temp_arr 	= $this->auth_group_model->get_auth_group_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->auth_group_model->get_auth_group_rows($condition);
        $result     =  array();
		foreach($temp_arr as $row){
			if( $row["pid"] ==0) {
				$temp =  array(
						"id" 			=> $row["id"],
						"name" 			=> $row["title"],
						"company_name" 	=> $row["company_name"],
						"pid" 			=> $row["pid"],
						"status" 		=> $row["status"]
				);
			}else {
				$temp =  array(
						"id" 			=> $row["id"],
						"name" 			=> $row["title"],
						"company_name" 	=> $row["company_name"],
						"status" 		=> $row["status"],
						"pid" 			=> $row["pid"],
						"_parentId"     => $row["pid"]
				);
			}
			$result []  = $temp;

		}
		$arr 	= array(
				"total"=>$total,
				"rows"=> $result
		);
		$json_str = json_encode($arr);
		echo  $json_str;
	}


   public function add()
    {

       $data['title'] 			= "添加用户组";
	   $data['action_url']		= site_url("/home/group/add");
	   $data['is_readonly']  	= 0;        //1 只读
	   $data["edit_type"] 		= "add";
       $data["group"] 		    = $this->auth_group_model->get_auth_group_fields();
       if($this->input->is_ajax_request()){
       		$this->data = $this->input->post();
            unset($this->data["id"]);
            $this->data["company_id"] = $this->company_id;
			$this->data["rules"]		  = "";
	        $group_id	  =  $this->table_model->add("auth_group",$this->data) ;
	        $condition   	= array("id" =>$this->data["pid"]);                       //客户系统管理员
			$auth_group   	= $this->table_model->get_array_one("auth_group",$condition);
            $rules			= $auth_group["rules"];

            $condition   	= array("id" =>$group_id);
            $group_data     = array("rules"=>$rules);
            $group_id	  	= $this->table_model->save("auth_group",$condition,$group_data) ;

	        if($group_id) {
	       	 	$this->success("添加成功");
	        }else{
	       	 	$this->error("添加失败");
	        }
       }
       $this->load->view('common/header');
       $this->load->view('group/edit',$data);

    }


    public function edit($id = 0)
    {

	    $condition      		= array("id" => $id);
        $data["group"] 			= $this->table_model->get_array_one("auth_group",$condition) ;
        $data['title'] 			= "修改用户组";
		$data['action_url']		= site_url("/home/group/edit");
		$data['is_readonly']  	= 0;        //1 只读
		$data["edit_type"] 		= "edit";
		if($this->input->is_ajax_request()) {  // 是否ajax提交
		    $this->data = $this->input->post();
	       	$validation = array(
	            			"title" => array('rules' => 'required|min_length[3]|max_length[15]'),
	         				);
            $this->check_validation($validation);
	        $condition  				= array("id" => $this->data["id"]);

	        $condition   	= array("id" =>$this->data["pid"]);                       //客户系统管理员
			$auth_group   	= $this->table_model->get_array_one("auth_group",$condition);
            $rules			= $auth_group["rules"];
	        $condition   	= array("id" =>$this->data["id"]);
	        $this->data["rules"] = $rules;
	        unset($this->data["id"]);
	        $result 					= $this->table_model->save("auth_group",$condition,$this->data) ;
	        if($result) {
	       	 $this->success("修改成功");
	        }else{
	       	 $this->error("修改失败");
	        }
		}else{
			$this->load->view('common/header');
        	$this->load->view('group/edit',$data);
		}


    }



    public function del()
    {
	    $this->check_access_ac("group/del");
	    if($this->input->post("id")){

		    $condition   	= array("group_id" =>$this->input->post("id"));                       //客户系统管理员
			$group_access  	= $this->table_model->get_array("auth_group_access",$condition);
			if(empty($group_access))	 {
			    $condition      		= array("id" => $this->input->post("id"));
		        if($this->table_model->del("auth_group",$condition)){
		        	 $this->success("删除成功");
		        }else{
		        	 $this->error("删除失败");
		        }
			}else{
				$this->error("该用户组下已有用户，不能删除");
			}
	    }else{
	    	$this->error("非法访问");
	    }

    }


 	public  function get_auth_group_json(){
    	    $this->load->model("auth_group_model");
    	    $reuslt 	= $this->auth_group_model-> get_auth_group_json();
    	    echo $reuslt;
    }
    public function search(){
       	$this->check_access_ac("group/index");
       	$para  		= $this->input->get();
		$condition 	= "";
        if($para) {
	   	   if($para["account"] !="") {
	        	$condition["where"]["a.account"]  		= trim($para["account"]);
	        }
	        if($para["nickname"] !="") {
	        	$condition["like"]["a.nickname"]  		= trim($para["nickname"]);
	        }
	        $this->get_auth_group_limit_json($condition);
        }
     }



 	public function auth($id = 0){
 		$this->check_access_ac("group/auth");
 		$data  =  array();
       	if($this->input->is_ajax_request()) {  // 是否ajax提交
        	$temp  					= $this->input->post();
	 	    $id 					= $temp["id"];
	 	    $this->data["rules"]	= $temp["auth"];
	 	    $rule   				=  explode(",",$temp["auth"]);
	 	    unset($data["id"]);
	 	    $condition  = array("id" => $id);
	 	    $result = $this->table_model->save("auth_group",$condition,$this->data);
		    if($result) {
	       	 	$this->success("权限分配成功");
	        }else{
	       	 	$this->error("权限分配失败");
	        }

       	}
       	$condition  	=  array("id"=> $id);
        $info  			= $this->table_model->get_array_one("auth_group",$condition);
        $data["group"]  = array(
								"id"=>$id,
								"name"=>$info["title"]
								);
        $data["rules"]  = $info["rules"];
 		$this->load->view('common/header_s');
        $this->load->view('group/auth',$data);
 	}



    public function check_account() {
		if(!$this->input->post("name")){
			die("error");
		}
		$condition  	=  array("account"=>trim($this->input->post("name")));
        $result = $this->table_model->get_array_one("auth_group",$condition);
		if(empty($result)){
			echo "ok";
		}else{
			echo "error";
		}

    }

	function get_auth_tree_json($id=0) {
		$this->check_access_ac("group/auth");
		$result  = $this->auth->get_auth_tree_json($id);
		echo $result;
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/mobileservice/auth.php */
