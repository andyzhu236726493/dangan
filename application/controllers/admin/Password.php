<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/3
 * Time: 0:06
 */
class Password extends Admin_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model("user_model");
    }

    /*
	 * 修改密码页面
	 * */
    public function change_password(){
        $data['action_url']=site_url('/admin/password/do_change_password');
        $this->load->view('admin/common/header');
        $this->load->view('admin/user/change_password',$data);
    }

    /*
     * 更新密码
     * */
    public function do_change_password(){
        $old_password=$_POST['old_password'];
        $password=$_POST['password'];
        $recpwd=$_POST['recpwd'];
        if($password!=$recpwd){
            json_return('error','二次密码不相同');
        }
        $uid=$_SESSION['user_info']['id'];
        $where['id']=$uid;
        $res=$this->user_model->get_user_info_one($where,'password');
        if($res==NULL){
            json_return('error','用户信息不存在');
        }elseif(md5($old_password)!=$res['password']){
            json_return('error','原始密码输入错误');
        }else{
            //写入
            $data['password']=md5($password);
            $data['update_time']=time();
            $r=$this->user_model->save_password($where,$data);
//			p($data);
            if($r===FALSE){
                json_return('error','修改密码失败');
            }else{
                json_return('ok','修改密码成功');
            }
        }
    }
}