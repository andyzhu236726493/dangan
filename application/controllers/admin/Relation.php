<?php
class Relation extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("customer_model");
        $this->load->model("user_model");

    }

    /*
     * 人口关系分析
     * */
    public function index($cardId=101){
        //$this->load->view('admin/common/header');
        $data =  array("cardId" =>$cardId);
        $this->load->view('admin/customer/index',$data);
    }


    public function relation($cardId=101){
        //$this->load->view('admin/common/header');
        $data =  array("cardId" =>$cardId);
        $this->load->view('admin/customer/relation',$data);
    }

    public function getNodes($cardId=101){

        $temp           = $this->getInfo($cardId);
        $level          = $temp["level"];
        $temp_cardIds   = $temp["ids"];
        $cardIds        = array();
        $key            = array();
        foreach($temp_cardIds  as $key=>$row){
            array_push($cardIds,$key);
        }
        $key = array_flip($cardIds);



        $this->db->select('cardId,name,oldname,category,lable,symbolSize,ignore,flag,is_dead,dead_time');
        $this->db->from('customer');
        $this->db->where_in('cardId', $cardIds);
        $query = $this->db->get();
        $temp =  $query->result_array();
        $SymbolSize = $this->geSymbolSize();

        $result = array();

        foreach($temp  as $row){
            if($row["is_dead"]) {
                $row["category"] = 4;
            }else {
                $row["category"] = $level[$row["cardId"]];
            }
            $row["id"]           = $key[$row["cardId"]];
            $row["symbolSize"]  = $SymbolSize[$row["id"]];
            if($row["oldname"] != ""){
                $row["title"]  =  $row["name"]."(".$row["oldname"].")";
                $row["name"]  =  $row["name"]."\n(".$row["oldname"].")";

            }else{
                $row["name"]  =  $row["name"];
                $row["title"]  =  $row["name"];
            }

            if($row["is_dead"]){
                $row["lable"]  = $row["name"]."<br>\n"."已于".$row["dead_time"]."去世";
            }
            $result[] = $row;
        }

        $list = $this->list_sort_by($result, 'id', 'asc');
        echo json_encode($list);
    }

    function list_sort_by($list, $field, $sortby = 'asc')
    {
        if (is_array($list))
        {
            $refer = $resultSet = array();
            foreach ($list as $i => $data)
            {
                $refer[$i] = &$data[$field];
            }
            switch ($sortby)
            {
                case 'asc': // 正向排序
                    asort($refer);
                    break;
                case 'desc': // 逆向排序
                    arsort($refer);
                    break;
                case 'nat': // 自然排序
                    natcasesort($refer);
                    break;
            }
            foreach ($refer as $key => $val)
            {
                $resultSet[] = &$list[$key];
            }
            return $resultSet;
        }
        return false;
    }

    public function getLinks($cardId=101){
        $temp   = $this->getInfo($cardId);
        $links  = $temp["links"];
        echo json_encode($links);
    }

    public function getRelationArchive($cardId=101){
        $temp           = $this->getInfo($cardId);
        $archive_ids    = $temp['archive_ids'];
        $ids            =  join("-",$archive_ids);
        echo            $ids;
    }


    public function getInfo($cardId=101){
        $condition  	=  array("source"=>$cardId);
        $one            = $this->table_model->get_array("customer_links",$condition);
        $links          = array();
        $ids[$cardId]   = 0;
        $level[$cardId]   = 0;
        $i              = 1;
        foreach($one  as $one_row){
            $links[]   = $one_row;
            $ids[$one_row['target']]   = $i;
            $level[$one_row['target']]   =1;
            $i++;
            $condition  	=  array("source"=>$one_row["target"]);
            $two            = $this->table_model->get_array("customer_links",$condition);

            foreach($two  as $two_row){
                $links[]       = $two_row;
                $ids[$two_row['target']]   = $i;
                $level[$two_row['target']]   =2;
                $i++;
                $condition  	=  array("source"=>$two_row["target"]);
                $three          = $this->table_model->get_array("customer_links",$condition);
                foreach($three  as $three_row){
                    $links[]       = $three_row;
                    $ids[$three_row['target']]   = $i;
                    $level[$three_row['target']]   =3;
                    $i++;

                }
            }
        }


        $temp_cardIds   = $ids;
        $cardIds        = array();
        $key            = array();
        foreach($temp_cardIds  as $key=>$row){
            array_push($cardIds,$key);
        }
        $key = array_flip($cardIds);
        $temp = array();
        $archive_ids  = array();
        foreach($links  as &$row){
            $row["source"]  =   $key[$row["source"]];
            $row["target"]  =   $key[$row["target"]];
            if( $row["relation_stop"] !="") {
                $row["lable"] = "关系已于".$row["relation_stop"]."终止";
                $row["color"] = "red";
            }else{
                $row["color"]  = "blue";
            }
            $archive_ids[]  = $row["archive_id"];
            unset($row["id"]);
        }

        $archive_ids = array_unique($archive_ids);

        $result =  array(
            "ids"           =>$ids,
            "key"           =>$key,
            "level"         =>$level,
            "links"         =>$links,
            "archive_ids"  =>$archive_ids
        );
        return     $result;
    }

    public  function archive_id($cardId=101){
        $info = $this->getInfo($cardId);
        $temp   = $info["archive_ids"];
        $archive_ids = join("-",$temp);
        $result =  array("status"=>1,"msg"=>"","archive_ids"=> $archive_ids);
        $json_str = json_encode($result);
        echo $json_str;
    }

    public function getDetail($cardId=101){
        $data["action_url"] =  site_url('admin/relation/get_archive_limit_json')."/".$cardId;
        $this->load->view('admin/common/header');
        $this->load->view('admin/customer/detail',$data);
    }

    public function getRelationDetail($ids=''){
        $data["action_url"] =  site_url('admin/relation/get_relation_archive_limit_json')."/".$ids;
        $this->load->view('admin/common/header');
        $this->load->view('admin/customer/detail',$data);
    }


    public function get_archive_limit_json($cardId=101){
        $page 	= isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows 	= isset($_POST['rows']) ? intval($_POST['rows']) : 20;
        $sort 	= isset($_POST['sort']) ? strval($_POST['sort']) : "a.id";
        $order 	= isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $offset = ($page-1)*$rows;
        $condition["where"]  = array("litigant_card"=>$cardId);
        $result = $this->customer_model->get_archive_array_limit($offset,$rows,$sort,$order,$condition);
        $total  = $this->customer_model->get_archive_rows($condition);
        $arr 		= array(
            "total"=> $total,
            "rows" => $result
        );

        $json_str = json_encode($arr);
        echo $json_str;
    }

    public function get_relation_archive_limit_json($archive_ids=''){
        $page 	= isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows 	= isset($_POST['rows']) ? intval($_POST['rows']) : 20;
        $sort 	= isset($_POST['sort']) ? strval($_POST['sort']) : "a.id";
        $order 	= isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $offset = ($page-1)*$rows;
        $archive_ids  =  explode("-",$archive_ids);
        $condition["wherein"]  = $archive_ids;
        $result = $this->customer_model->get_archive_array_limit($offset,$rows,$sort,$order,$condition);
        $total  = $this->customer_model->get_archive_rows($condition);
        $arr 		= array(
            "total"=> $total,
            "rows" => $result
        );

        $json_str = json_encode($arr);
        echo $json_str;
    }

    public function geSymbolSize(){
        $result =  array(
            "0"=>60,
            "1"=>35,
            "2"=>35,
            "3"=>35,
            "4"=>35,
            "5"=>35,
            "6"=>35,
            "7"=>35,
        );
        return $result;
    }

}