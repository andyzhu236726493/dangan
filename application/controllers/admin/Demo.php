<?php

//require_once(APPPATH . 'third_party/pdfmerge/jurosh/PDFMerge/PDFMerger.php');
//ssrequire_once(APPPATH . 'third_party/pdfmerge/jurosh/PDFMerge/PDFObject.php');
require_once(APPPATH . 'third_party/pdf/fpdf/fpdf.php');
require_once(APPPATH . 'third_party/pdf/fpdi/fpdi.php');

class Demo extends Admin_Controller{
   public function __construct(){
        parent::__construct();

   }


   //  PDF 自动合并
    public function index()
    {
// define some files to concatenate
        $files = array(
            'f0.pdf',
            'f1.pdf',
            'f4.pdf'
        );

// initiate FPDI
        $pdf = new FPDI();

// iterate through the files
        foreach ($files AS $file) {
            // get the page count
            $path = "d:/uploads/";
            $pageCount = $pdf->setSourceFile($path.$file);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);

                // create a page (landscape or portrait depending on the imported page size)
                if ($size['w'] > $size['h']) {
                    $pdf->AddPage('L', array($size['w'], $size['h']));
                } else {
                    $pdf->AddPage('P', array($size['w'], $size['h']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);

                $pdf->SetFont('Helvetica');
                $pdf->SetXY(5, 5);
                $pdf->Write(8, 'hello world');
            }
        }

// Output the new PDF
        $pdf->Output();

    }

    public function index1()
    {

    $pdf = new \Jurosh\PDFMerge\PDFMerger;
        $path = "uploads/";

		$pdf->addPDF($path.'f0.pdf', 'all')
		    ->addPDF($path.'f1.pdf', 'all')
            ->addPDF($path.'f3.pdf', 'all','auto')
		    ->addPDF($path.'f2.pdf', 'all');

      try
 		{
		$result = @$pdf->merge('file', $path.'merged.pdf');
		echo "ok";

 		}catch(Exception $e){
		 echo 'Message: ' .$e->getMessage();
 		}

    }

//目录遍历

        function read_dir($dir){
            $files=array();
            $dir_list=scandir($dir);
            foreach($dir_list as $file){
                if($file!='..' && $file!='.'){
                    if(is_dir($dir.'/'.$file)){
                        $files[]=read_dir($dir.'/'.$file);
                    }else{
                        $files[]=$file;
                    }
                }
            }
            return $files;
        }

    /**
     * Goofy 2011-11-30
     * getDir()去文件夹列表，getFile()去对应文件夹下面的文件列表,二者的区别在于判断有没有“.”后缀的文件，其他都一样
     */

//获取文件目录列表,该方法返回数组
    function getDir($dir) {
        $dirArray[]=NULL;
        if (false != ($handle = opendir ( $dir ))) {
            $i=0;
            while ( false !== ($file = readdir ( $handle )) ) {
                //去掉"“.”、“..”以及带“.xxx”后缀的文件
                if ($file != "." && $file != ".."&&!strpos($file,".")) {
                    $dirArray[$i]=$file;
                    $i++;
                }
            }
            //关闭句柄
            closedir ( $handle );
        }
        return $dirArray;
    }

//获取文件列表
    function getFile($dir) {
        $fileArray[]=NULL;
        if (false != ($handle = opendir ( $dir ))) {
            $i=0;
            while ( false !== ($file = readdir ( $handle )) ) {
                //去掉"“.”、“..”以及带“.xxx”后缀的文件
                if ($file != "." && $file != ".."&&strpos($file,".")) {
                    $fileArray[$i]="./imageroot/current/".$file;
                    if($i==100){
                        break;
                    }
                    $i++;
                }
            }
            //关闭句柄
            closedir ( $handle );
        }
        return $fileArray;
    }

    function showXml($filename="F80-2015-11-0001.xml") {
        $file_path = base_url()."/uploads/xml/".$filename;
        redirect($file_path);
    }


}
