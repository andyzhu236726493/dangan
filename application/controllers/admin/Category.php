<?php
/**
 * Controller Approve
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MY_Controller
{
    private $list = "";

    function __construct()
    {
        parent::__construct();
        $this->load->model("category_model");
    }


    public function index()
    {
        $data  = array();
      	$rules =   array(
				"list"  => "category/index",
				"add"   => "category/add",
				"edit"  => "category/edit",
				"del"   => "category/del",
				"export"   => "category/export_file",
				"import"   => "category/import_file",
				);
       	$data["auth"] = $this->check_access($rules);
        $this->load->view('common/header', $data);
        $this->load->view('category/list', $data);
    }


	public function get_category_json()
    {
		$result = $this->category_model->get_category_json();
    	echo $result;
    }

    /*
     * 获取分类列表
     * */
    public function get_category_list(){
        $res=$this->category_model->get_category_list();
        array_unshift($res,array('id'=>0,'text'=>'全部'));
        echo json_encode($res);
    }



}