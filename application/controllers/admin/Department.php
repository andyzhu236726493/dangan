<?php
/**
 * Controller Approve
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Department extends Admin_Controller
{
    private $list = "";

    function __construct()
    {
        parent::__construct();
        $this->load->model("department_model");
    }


    public function index()
    {

        $data  = array();

        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/department/list', $data);
    }


   public function get_department_tree_limit_json($condition="") {
		$result= 	$this->department_model->get_department_limit_json($condition);
   		echo $result;
   }


    public function add($pid=0)
    {

       $data['title'] 				= "添加公证处";
	   $data['action_url']			= site_url("/admin/department/add");
	   $data["department"] 			= $this->department_model->get_department_fields();

       if($this->input->is_ajax_request()) {  // 是否ajax提交
       		$this->data = $this->input->post();
            unset($this->data["id"]);
	        $id 			=  $this->table_model->add("department",$this->data) ;
	        if($id) {
	       	 	 $this->success("添加成功");
	        }else{
	       	 	$this->error("添加失败");
	        }
       }else{
       	   $this->load->view('admin/common/header');
           $this->load->view('admin/department/edit',$data);

       }

    }

    public function  edit($id=0) {
    	if($this->input->is_ajax_request()) {  // 是否ajax提交
		    $this->data = $this->input->post();
		    unset($this->data["id"]);
	       	$condition  	=  array("id"	     =>	trim($this->input->post("id")));
	        $result 					= $this->table_model->save("department",$condition,$this->data) ;

	        if($result) {
	       	 $this->success("修改成功");
	        }else{
	       	 $this->error("修改失败");
	        }
		}else{
 			$data['action_url']			= site_url("/admin/department/edit");
 			$data['department']		= $this->department_model->get_department_info($id);
			$this->load->view('admin/common/header');
       		$this->load->view('admin/department/edit',$data);
		}
    }

    public function del()
    {
	    if($this->input->post("id")){
		    $condition      		= array("id" => $this->input->post("id"));
	        if($this->table_model->del("department",$condition)){
	        	 $this->success("删除成功");
	        }else{
	        	 $this->error("删除失败");
	        }
	    }else{
	    	$this->error("非法访问");
	    }

    }

   public function get_department_json()
    {
		$result = $this->department_model->get_all_department_json();
    	echo $result;
    }

   public function get_all_department_list_json()
    {
		$result = $this->department_model->get_all_department_json();
    	echo $result;
    }

	/*
	 * 获取所有的公证处
	 * */
	public function get_all_department_json(){
		$where['status']=1;
		$select='id,name';
		$res=$this->department_model->get_all_department($where,$select);
		echo json_encode($res);
	}




}