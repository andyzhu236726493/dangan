<?php
/**
 * Controller Approve
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends Ci_Controller {

	private   $list 	="";
	function __construct()
    {
         parent::__construct();
//         $this->load->model("goods_model");
    }


    public function index()
    {

        $data  = array();
          $this->load->view('admin/main',$data);

    }


    public function welcome()
    {

        $data  = array();
          $this->load->view('admin/welcome',$data);

    }


}

/* End of file auth.php */
/* Location: ./application/controllers/mobileservice/auth.php */
