<?php


/**
 * 微信企业转账工具类
 */
class WxTransfers
{
	// 企业转账请求地址
	const TRANSFERS_URL = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';

	//获取转账信息地址
	const GETINFO_URL='https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo';



	public function __construct($config)
	{

	}




  function curl_post($data,$config, $second = 30, $aHeader = array())
	{
		$sign = $this->getSign($data,$config['key']);
		$data = $this->create_xml($data,$sign);

		$ch = curl_init();
		// 超时时间
		curl_setopt($ch, CURLOPT_TIMEOUT, $second);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 这里设置代理，如果有的话
		curl_setopt($ch, CURLOPT_URL, self::TRANSFERS_URL);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		// cert 与 key 分别属于两个.pem文件
		curl_setopt($ch, CURLOPT_SSLCERT, $config['api_cert']);
		curl_setopt($ch, CURLOPT_SSLKEY, $config['api_key']);
		//curl_setopt($ch, CURLOPT_CAINFO, $this->_cert['rootca']);
		if (count($aHeader) >= 1) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$data = curl_exec($ch);
		if ($data) {
			curl_close($ch);
			return $data;
		} else {
			curl_close($ch);
			return curl_errno($ch);
		}
	}




   public  function create_xml($data,$sign){
   	 $xml  ="<xml>
			<mch_appid>".$data['mch_appid']."</mch_appid>
			<mchid>".$data['mchid']."</mchid>
			<nonce_str>".$data['nonce_str']."</nonce_str>
			<partner_trade_no>".$data['partner_trade_no']."</partner_trade_no>
			<openid>".$data['openid']."</openid>
			<check_name>".$data['check_name']."</check_name>
			<re_user_name>".$data['re_user_name']."</re_user_name>
			<amount>".$data['amount']."</amount>
			<desc>".$data['desc']."</desc>
			<spbill_create_ip>".$data['spbill_create_ip']."</spbill_create_ip>
			<sign>".$sign."</sign>
			</xml>";
	  return $xml;
   }


	public function formatBizQueryParaMap($paraMap, $urlencode)
	{
		$buff = "";
		ksort($paraMap);
		foreach ($paraMap as $k => $v)
		{
			if($urlencode)
			{
				$v = urlencode($v);
			}
			//$buff .= strtolower($k) . "=" . $v . "&";
			$buff .= $k . "=" . $v . "&";
		}

		if (strlen($buff) > 0)
		{
			$reqPar = substr($buff, 0, strlen($buff)-1);
		}

		return $reqPar;
	}

	/**
	 * 	作用：生成签名
	 */
	public function getSign($Obj,$key)
	{
		foreach ($Obj as $k => $v)
		{
			$Parameters[$k] = $v;
		}
		//签名步骤一：按字典序排序参数
		ksort($Parameters);
		$String = $this->formatBizQueryParaMap($Parameters, false);
		//echo '【string1】'.$String.'</br>';
		//签名步骤二：在string后加入KEY
		$String = $String."&key=".$key;
		//echo "【string2】".$String."</br>";
		//签名步骤三：MD5加密
		$String = md5($String);
		//echo "【string3】 ".$String."</br>";
		//签名步骤四：所有字符转为大写
		$result_ = strtoupper($String);
		//echo "【result】 ".$result_."</br>";
		return $result_;
	}





	/**
	 * 获取服务器ip
	 *
	 * @return string
	 */
	public function getServerIp()
	{
		$server_ip = '127.0.0.1';
		if (isset($_SERVER)) {
			if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR']) {
				$server_ip = $_SERVER['SERVER_ADDR'];
			} elseif (isset($_SERVER['LOCAL_ADDR']) && $_SERVER['LOCAL_ADDR']) {
				$server_ip = $_SERVER['LOCAL_ADDR'];
			}
		} else {
			$server_ip = getenv('SERVER_ADDR');
		}
		return $server_ip;
	}

	/**
	 * 设置日志目录文件
	 *
	 * @param unknown $file
	 */
	public function setLogFile($file)
	{
		$this->log_file = $file;
	}

	/**
	 * 写日志
	 *
	 * @param $msg 写入的信息
	 * @param $type 日志类型作为查询标示
	 */
	public function log($msg, $type)
	{
		if ($this->log_file) {
			$log = str_replace(array(
				"\r\n",
				"\r",
				"\n"
			), array(
				"",
				"",
				""
			), $msg);
			s($log);
			//error_log($type . ' ' . date('Y-m-d H:i:s') . ' ' . json_encode($log,JSON_UNESCAPED_UNICODE) . "\r\n", 3, $this->log_file);
		}
	}

}