
<?php

/**
 * 微信oAuth认证示例
 */
require_once(DIR_APPLICATION . 'controller/extension/wx/wechat.class.php');

class AUTH {

    private $options;
    public $open_id;
    public $wxuser;
    public $redirectUrl;
    public function __construct($options ,$redirectUrl = '') {
        $this->options = $options;
        $this->redirectUrl = $redirectUrl;
        $this->wxoauth();
    }

    public function wxoauth() {

        $scope = 'snsapi_base';
        $code = isset($_GET['code']) ? $_GET['code'] : '';
        $token_time = isset($_SESSION['token_time']) ? $_SESSION['token_time'] : 0;

        if (!$code && isset($_SESSION['open_id']) && isset($_SESSION['user_token']) && $token_time > time() - 3600) {
             $this->wxuser = $_SESSION['wxuser'];
             return $_SESSION['open_id'];
        } else {
            $options = array(
                'token' => $this->options["token"], //填写你设定的key
                'appid' => $this->options["appid"], //填写高级调用功能的app id
                'appsecret' => $this->options["appsecret"] //填写高级调用功能的密钥
            );
            $we_obj = new Wechat($options);
            if ($scope == 'snsapi_base') {
                $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $_SESSION['wx_redirect'] = $url;
            } else {
                $url = $this->redirectUrl;
            }
            if (!$url) {
                $url = $this->url->link('wx/customer/regCustomer', '', 'SSL');
            }
            $oauth_url = $we_obj->getOauthRedirect($url, "wxbase", $scope);
            if ($code) {
                $json = $we_obj->getOauthAccessToken();
                if (!$json) {
                    unset($_SESSION['wx_redirect']);
                    Y::a()->request->redirect($oauth_url);
                }
                $_SESSION['open_id'] = $this->open_id = $json["openid"];
                $access_token = $json['access_token'];
                $_SESSION['user_token'] = $access_token;
                $_SESSION['token_time'] = time();
                $userinfo = $we_obj->getUserInfo($this->open_id);
                if ($userinfo && !empty($userinfo['nickname'])) {
                    $this->wxuser = array(
                        'open_id' => $this->open_id,
                        'nickname' => $userinfo['nickname'],
                        'sex' => intval($userinfo['sex']),
                        'location' => $userinfo['province'] . '-' . $userinfo['city'],
                        'avatar' => $userinfo['headimgurl']
                    );
                } elseif (strstr($json['scope'], 'snsapi_userinfo') !== false) {
                    $userinfo = $we_obj->getOauthUserinfo($access_token, $this->open_id);
                    if ($userinfo && !empty($userinfo['nickname'])) {
                        $this->wxuser = array(
                            'open_id' => $this->open_id,
                            'nickname' => $userinfo['nickname'],
                            'sex' => intval($userinfo['sex']),
                            'location' => $userinfo['province'] . '-' . $userinfo['city'],
                            'avatar' => $userinfo['headimgurl']
                        );
                    }
                }else {
                    return $this->open_id;
                }
                if ($this->wxuser) {
                    $_SESSION['wxuser'] = $this->wxuser;
                    $_SESSION['open_id'] = $json["openid"];
                    unset($_SESSION['wx_redirect']);
                    return $this->open_id;
                }
                $scope = 'snsapi_userinfo';
            }

            header('Location: ' . $oauth_url);
        }
    }
}