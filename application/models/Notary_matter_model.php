<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class Notary_matter_model extends  Db_model
{

  	function __construct(){
  		parent::__construct();
	}

	 public function get_notary_matter_tree_limit_json($condition=""){
		$page 		= isset($_REQUEST['page']) 			? intval($_REQUEST['page']) 	: 1;
		$rows		= isset($_REQUEST['pagerows']) 		? intval($_REQUEST['pagerows']) : 100;
		$sort 		= isset($_REQUEST['sort']) 			? strval($_REQUEST['sort']) 	: "id";
		$order 		= isset($_REQUEST['order']) 		? strval($_REQUEST['order']) 	: 'asc';
		$offset 	= ($page-1)*$rows;
    	$temp_arr 	= $this->get_notary_matter_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->get_notary_matter_rows($condition);

		$result     =  array();
		$arr 		=  array();
		foreach($temp_arr as $row){
			 $arr	=   array("id"=>$row["id"],"name"=>$row["name"],"field"=>$row["field"]);
			 if( $row["pid"] != 0) {
				$arr["_parentId"]  = $row["pid"];
			  }
			$result[]		= $arr;
		}
		$temp 	= array(
				"total"=>$total,
				"rows"=> $result
		);
	    return  $temp;
	}

	public  function get_notary_matter_array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('notary_matter as a');
		$sql = 'a.*';
		$this->db->select($sql);

        if($condition){
	        foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
        }
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$result =   $this->db->get()->result_array();
        return $result;
	}



	public  function get_notary_matter_rows($condition){
       $this->db->from('notary_matter as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
       return $this->db->count_all_results();
   }
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_notary_matter_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "a.*";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("notary_matter as a");
		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->notary_matter_info = $result[0];
		}else{
			$this->notary_matter_info =  array();
		}
		return $this->notary_matter_info;
	}



    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_notary_matter($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("notary_matter as a");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->notary_matter_info = $result[0];
		}else{
			$this->notary_matter_info =  array();
		}
		return $this->notary_matter_info;
	}



	public function exists() {
		if(empty($this->notary_matter_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function get_notary_matter_json() {

			$fields 	= "id,name,pid";
			$department	= $this->table_model->get_array("notary_matter");
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["name"];
				$arr[]				= $temp;
			}

			$json_str 	= json_encode($arr);
			return   $json_str;

	}



   public  function get_notary_matter_fields(){
   	     $notary_matter_array  =  array(
  							"id"				=>"",
  						);
   		return $notary_matter_array;
   }



   public  function get_person_fields(){
   	     $notary_matter_array  =  array(
  							"fullname"		=>"姓名",
  							"sex"			=>"性别",
  							"id_card"		=>"身份证号",
  							"birthdate"		=>"出生日期",
  							"bealive"		=>"在世"
  						);
   		return $notary_matter_array;

   }

	/*
	 * 获取公证事由列表
	 * */
	public function get_notary_matter_list(){
		$where=array();
		$res=$this->db->where($where)->select('id,name as text')->order_by('sort desc')->get('notary_matter')->result_array();
		return $res;
	}

	/*
	 * 获取一条记录
	 * */
	public function get_notary_matter_one($where){
		$res=$this->db->where($where)->select('id')->get('notary_matter')->row_array();
		return $res;
	}

	/*
	 * 写入
	 * */
	public function add_notary_matter($data){
		$res=$this->db->insert('notary_matter',$data);
		return $res;
	}
}
?>
