<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class Department_model extends  Db_model
{

  private  $id 			= 0;
  private  $user_info 	= array();

  function __construct(){
  		parent::__construct();
	}


  function get_department_tree_json($company_id=0) {

			$condition  = array("company_id"=> $company_id);
			$fields 	= "id,name,pid";
			$department	= $this->table_model->get_array("department",$condition,$fields);
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["name"];
				$temp["parent_id"] 	= $row["pid"];
				$arr[]				= $temp;
			}

			$result 	= arr_tree($arr);
			$json_str 	= json_encode($result);
			return   $json_str;

	}



   public function get_department_limit_json($condition=""){
		$page 		= isset($_REQUEST['page']) 			? intval($_REQUEST['page']) 	: 1;
		$rows		= isset($_REQUEST['pagerows']) 		? intval($_REQUEST['pagerows']) : 20;
		$sort 		= isset($_REQUEST['sort']) 			? strval($_REQUEST['sort']) 	: "id";
		$order 		= isset($_REQUEST['order']) 		? strval($_REQUEST['order']) 	: 'asc';
		$offset 	= ($page-1)*$rows;
    	$result 	= $this->get_department_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->get_department_rows($condition);
		$arr 	= array(
				"total"=>$total,
				"rows"=> $result
		);
		$json_str = json_encode($arr);
		echo  $json_str;
	}

	public  function get_department_array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('department as a');
		$sql = 'a.*';
		$this->db->select($sql);

        if($condition){
	      		$this->db->where($condition);
        }
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		return $this->db->get()->result_array();
		//echo $this->db->last_query();
	}


   public  function get_department_rows($condition){
       $this->db->from('department as a');
		if($condition){
			$this->db->where($condition);
		 }
       return $this->db->count_all_results();
   }


     public  function get_department_info($id=0){
     	$this->db->from('department as a');
		$sql = 'a.*';
		$this->db->select($sql);
	    $this->db->where("a.id", $id);
		$this->db->limit(1,0);
		$result = $this->db->get()->result_array();
        if(!empty($result)){
        	return $result[0];
        }else{
        	return array();
        }

     }

     public  function get_department_array($like =""){
     	$this->db->from('department as a');
		$sql = 'a.*';
		$this->db->select($sql);

		$result = $this->db->get()->result_array();
  		return $result;
     }




    public  function get_department_fields(){
   	     $user_array  =  array(
  							"id"			=>"",
  							"name"			=>"",
  							"status"		=>""
  						);
   		return $user_array;
   }


   function get_department_json() {

	        $condition  = array("id"=>$this->department_id);
			$department	= $this->table_model->get_array("department",$condition);
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["name"];
				$arr[]				= $temp;
			}

			$json_str 	= json_encode($arr);
			return   $json_str;

	}

   function get_all_department_json() {

	        $condition  = array("status"=>1);
			$department	= $this->table_model->get_array("department",$condition);
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["name"];
				$arr[]				= $temp;
			}

			$json_str 	= json_encode($arr);
			return   $json_str;

	}

	/*
	 * 获取所有的部门
	 * */
	public function get_all_department($where,$select){
		$res=$this->db->where($where)->select($select)->order_by('id asc')->get('department')->result_array();
		return $res;
	}


}
?>
