<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20
 * Time: 21:59
 */
class Archive_attachment_pdf_model extends MY_Model{
    public function __construct(){
        parent::__construct();
    }

    /*
     * 获取pdf
     * */
    public function get_pdf($where,$select='*',$where_in=array()){
        $this->db->where($where);
        if(count($where_in)!=0){
            $this->db->where_in('file_name',$where_in);
        }
        $res=$this->db
            ->select($select)
            ->order_by('order_id asc,new_number asc')
            ->get('archive_attachment_pdf')
            ->result_array();
        return $res;
    }
}