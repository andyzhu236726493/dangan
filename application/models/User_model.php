<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class User_model extends  Db_model
{

  private  $id 			= 0;
  private  $user_info 	= array();



  function __construct(){
  		parent::__construct();
        //$this->load->model("table_model");
	}




	public  function get_user_array_limit($offset,$rows,$sort,$order,$condition)
	{


		$this->db->from('user as a');
		$sql = 'a.*,c.title as group_name,d.name as department_name ';
		$this->db->select($sql);

        if($condition){
	        foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
        }
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$this->db->join('department as d', 'd.id = a.department_id '    ,"left");
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$temp =   $this->db->get()->result_array();

		 foreach($temp as $row){
			$row["create_time"]    		= date('Y-m-d H:i:s',$row["create_time"]);

			$result[]		= $row;
        }
        return $result;
	}


   public  function get_user_rows($condition){
       $this->db->from('user as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
       return $this->db->count_all_results();
   }
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_user_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "a.*,b.group_id,c.rules,d.name  as department_name";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("user as a");
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$this->db->join('department as d', 'd.id = a.department_id '    ,"left");

		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->user_info = $result[0];
		}else{
			$this->user_info =  array();
		}
		return $this->user_info;
	}
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_user($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules,d.name  as department_name";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("user as a");
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$this->db->join('department as d', 'd.id = a.department_id '    ,"left");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->user_info = $result[0];
		}else{
			$this->user_info =  array();
		}
		return $this->user_info;
	}



	public function exists() {
		if(empty($this->user_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}


   public  function get_user_fields(){
   	     $user_array  =  array(
  							"id"				=>"",
  							"account"			=>"",
  							"nickname"			=>"",
  							"email"				=>"",
  							"remark"			=>"",
  							"status"			=>"",
  							"tel"				=>"",
  							"number"			=>"",
  							"group_id"			=>"",
  							"department_id"		=>"",
  						);
   		return $user_array;

   }

   	function save($condition,$data){
		$this->db->where($condition);
		$this->db->set('login_count','login_count+1',false);
		if($this->db->update("user", $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*
	 * 获取用户信息
	 * */
	public function get_user_info_one($where,$select='*'){
		$res=$this->db->where($where)->select($select)->get('user')->row_array();
		return $res;
	}

	/*
	 * 保存密码
	 * */
	public function save_password($where,$data){
		$res=$this->db->update('user',$data,$where);
		return $res;
	}
}
?>
