<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class User_log_model extends  Db_model
{

  private  $id 			= 0;
  private  $user_info 	= array();



  	function __construct(){
  		parent::__construct();
        //$this->load->model("table_model");
	}


	public  function get_user_log__array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('user_log as a');
		$sql = 'a.*,b.account as account';
		$this->db->select($sql);
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
		$this->db->where('company_id', $this->company_id);
		$this->db->join('user as b', 'b.id = a.user_id '    ,"left");
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$temp  =   $this->db->get()->result_array();
		$result =  array();
		foreach($temp as $row) {
			$row["login_time"]  =  date("Y-m-d",$row["login_time"]);
			$result[]    = $row;
		}
        return $result;
	}

    public  function get_user_log_rows($condition){
       $this->db->from('user_log as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
	   $this->db->join('user as b', 'b.id = a.user_id '    ,"left");
	   $this->db->where('company_id', $this->company_id);
       return $this->db->count_all_results();
   }

}
?>
