<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_rule_model extends  Db_model
{
    function __construct()
    {
        parent::__construct();
    }


    public function get_auth_rule_array_limit()
    {
        $this->db->from('auth_rule as a');
        $this->db->select();
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $result;
    }


    /*public function get_auth_rule_rows($condition)
    {
        $this->db->from('auth_rule as a');
        return $this->db->count_all_results();
    }*/
    public function get_meuns_list($meun_id=""){
        $this->db->select('*');
        $this->db->from('auth_rule');
        if($meun_id!=""){
            $this->db->where(array('parent_id'=>$meun_id));
        }else{
            $this->db->where(array('level'=>'1'));
        }
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function get_fields(){
            $auth_array=array(
                "module"=>"",
                "name"=>"",
                "title"=>"",
                "icon"=>"add",
                "status"=>"1",
            );
            return $auth_array;
    }
}