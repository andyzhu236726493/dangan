<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/1/22
 * Time: 14:16
 */
class System_model extends Db_model{
    public function __construct(){
        parent::__construct();
    }

   public function get_system_para(){
			$para =  array(
						"department_number_prefix"  		=> "D",            //部门编号前缀
						"department_number_length"  		=> "4",            //部门编号数字部分长度
						"employee_number_prefix"  			=> "E",            //员工编号前缀
						"employee_number_length"  			=> "4",            //员工编号数字部分长度
					);
		    return $para;

   }

   	public function add_system_para($company_id){

 		$condition  	=  array("company_id" => $company_id);
        $para 			= $this->get_system_para();
        $data 			=  array(
        					"company_id" 		=> 	$company_id,
        					"system_para"				=> 	serialize($para),
        				    "department_number"	=> 	1,
        				    "employee_number"	=>	1
        				);

        $temp 			= $this->table_model-> add("system",$data);
	}

}