<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class archive_model extends  Db_model
{

	function __construct(){
  		parent::__construct();

	}



	public  function get_archive_array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('archive as a');
//		$sql = 'a.*,b.name  as category_name,c.name  as notary_matter_name';
		$sql = 'a.*,b.name  as category_name,a.notary_matter_name  as notary_matter_name';
		$this->db->select($sql);
		$this->db->where("department_id",$this->department_id);
        if($condition){
	        foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
        }
        $this->db->join('archive_category as b', 'b.id = a.category_id '    ,"left");
//        $this->db->join('notary_matter as c', 'c.id = a.notary_matter_id '    ,"left");
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$temp =   $this->db->get()->result_array();
		$result	=  array();
		$storage_time	= $this->get_storage_time_array();
		foreach($temp as $row){
		 	$row["receive_time"]  		= date("Y-m-d H:i",$row["receive_time"]);
		 	$row["archive_time"]  		= date("Y-m-d H:i",$row["archive_time"]);
		 	$row["over_time"]  			= date("Y-m-d H:i",$row["over_time"]);
		 	$row["storage_time"]  		= $storage_time[$row["storage_time_id"]];
		 	$row["litigant_date"]  		= date("Y-m-d H:i",$row["litigant_date"]);

			$row["create_time"]    		= date('Y-m-d H:i:s',$row["create_time"]);
			$row["update_time"]    		= date('Y-m-d H:i:s',$row["update_time"]);
			$result[]		= $row;
        }
        return $result;
	}


	public  function get_archive_rows($condition){
       $this->db->from('archive as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
		$this->db->where("department_id",$this->department_id);
       return $this->db->count_all_results();
   }
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "*";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("archive");
		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->archive_info = $result[0];
		}else{
			$this->archive_info =  array();
		}
		return $this->archive_info;
	}
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("archive as a");
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->archive_info = $result[0];
		}else{
			$this->archive_info =  array();
		}
		return $this->archive_info;
	}



	public function exists() {
		if(empty($this->archive_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}

  public  function get_storage_time_array(){

        $result 	= array(
        					1 	=>"永久",
        					2 	=>"长期",
        					3	=>"短期",
        				);

		return $result;
   }

   public  function get_archive_fields(){
   	     $archive_array  =  array(
  							"id"				=>"",
  							"word_no"			=>"",
  							"category_id"		=>"",
  							"notary_matter_id"	=>"",
  							"litigant_type"		=>1,
  							"litigant_org"		=>"",
  							"litigant_name"		=>"",
  							"litigant_card"		=>"",
  							"litigant_sex"		=>1,
  							"litigant_date"		=>"",
  							"undertaker"		=>"",
  							"receive_time"		=>"",
  							"department_id"		=>"",
  							"receive_time"		=>"",
  							"archive_time"		=>"",
  							"greffier"			=>"",
  							"notarial_reason"	=>"",
  							"department_id"		=>"",
  							"over_time"			=>"",
  							"storage_time_id"	=>"1",
  							"box_number"		=>"",
  							"volume_number"		=>""
  						);
   		return $archive_array;

   }

   	function save($condition,$data){
		$this->db->where($condition);
		$this->db->set('login_count','login_count+1',false);
		if($this->db->update("archive", $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*
	 * 年度业务量统计
	 * */
	public function business_statistics($where='1=1'){
		$res=$this->db->where($where)->select('receive_year,count(id) as total')->order_by('receive_year asc')->group_by('receive_year')->get('archive')->result_array();
		return $res;
	}

	/*
	 * 公证类别分析
	 * */
	public function notarial_category($w=array()){
		$where['c.status']=1;
		if(count($w)!=0){
			$where=array_merge($w,$where);
		}

		$res=$this->db->from('archive as a')
			->join('archive_category as c','a.category_id=c.id')
			->select('c.name as category,count(a.id) as total')
			->where($where)
			->group_by('a.category_id')
			->order_by('c.order desc')
			->get()
			->result_array();
		return $res;
	}

	/*
     * 公证事由分析
     * */
	public function notary_matter($where='1=1'){
//		$res=$this->db->from('notary_matter as n')
//			->join('archive as a','n.id=a.notary_matter_id','left')
//			->select('n.name,count(a.id) as total')
//			->group_by('n.id')
//			->order_by('n.sort desc')
//			->get()
//			->result_array();
//		return $res;
		$res=$this->db->from('archive')
			->where($where)
			->select('notary_matter_name as name,count(id) as total')
			->group_by('notary_matter_name')
//			->order_by('n.sort desc')
			->get()
			->result_array();
		return $res;
	}

	/*
	 * 获取实时数据
	 * */
	public function real_time_data(){
		$where['c.status']=1;
		$res=$this->db->from('archive_category as c')
			->join('archive as a','c.id=a.category_id','left')
			->select('c.id,c.name,count(a.id) as total,sum(a.page) as pdf_num','left')
			->group_by('c.id')
			->order_by('c.order desc')//此处采用升序排列,页面输出时会自动反转
			->where($where)
			->get()
			->result_array();
//		p($res);
//		$respdf=$this->db->from('archive_category as c')
//			->join('archive as a','c.id=a.category_id','left')
//			->join('archive_attachment_pdf as p','p.archive_id=a.id')
//			->select('c.id,sum(p.page) as pdf_num','left')
//			->group_by('c.id')
//			->order_by('c.order desc')//此处采用升序排列,页面输出时会自动反转
//			->where($where)
//			->get()
//			->result_array();
//		return array($res,$respdf);
		return $res;

	}

	/*
	 * 获取实时数据
	 * */
	public function real_time_data_pdf(){
		$where['c.status']=1;
		$res=$this->db->from('archive_category as c')
			->join('archive as a','c.id=a.category_id','left')
			->select('c.id,c.name,count(a.id) as total','left')
			->group_by('c.id')
			->order_by('c.order asc')//此处采用升序排列,页面输出时会自动反转
			->where($where)
			->get()
			->result_array();
		return $res;
	}



	/*
	 * 获取档案列表
	 * */
	public function get_archive_list($where,$limit,$page,$order,$like,$where_in=array()){

		$this->db
			->from('archive as a')
			->join('archive_category as c','a.category_id=c.id')
//			->join('notary_matter as n','a.notary_matter_id=n.id')
			->join('department as d','a.department_id=d.id')
			->where($where);
		if(count($where_in)!=0){
			$this->db->group_start();
			$this->db->like('notary_matter_name',$where_in[0]);
			for($i=1;$i<count($where_in);$i++){
				$this->db->or_like('notary_matter_name',$where_in[$i]);
			}
			$this->db->group_end();
//			$this->db->where_in('notary_matter_name',$where_in);
		}
		$count=$this->db->like($like)
			->count_all_results('',FALSE);

		$res=$this->db
			->select('a.*,c.name c_name,a.notary_matter_name n_name,d.name d_name')
//			->select('a.*,c.name c_name,n.name n_name,d.name d_name')
			->limit($limit,($page-1)*$limit)
			->order_by($order)
			->get()
			->result_array();
//		$res=$this->db
//			->from('archive as a')
//			->join('archive_category as c','a.category_id=c.id')
//			->join('notary_matter as n','a.notary_matter_id=n.id')
//			->join('department as d','a.department_id=d.id')
//			->select('a.*,c.name c_name,n.name n_name,d.name d_name')
//			->where($where)
//			->like($like)
//			->limit($limit,($page-1)*$limit)
//			->order_by($order)
//			->get()
//			->result_array();
//		$count=$this->db
//			->from('archive as a')
//			->join('archive_category as c','a.category_id=c.id')
//			->join('notary_matter as n','a.notary_matter_id=n.id')
//			->join('department as d','a.department_id=d.id')
//			->where($where)
//			->like($like)
//			->count_all_results();
		return array('rows'=>$res,'total'=>$count);
	}

	/*
	 * 获取档案详情
	 * */
	public function get_archive_detail($condition,$select='*'){
		$res=$this->db->where($condition)->select($select)->get('archive')->row_array();
		return $res;
	}

	/*
	 * 获取申请查阅数据
	 * */
	public function get_apply_manage($where,$limit,$page,$order,$like){
		$res=$this->db
			->from('archive_applicant as apply')
			->join('archive as a','a.id=apply.archive_id')
			->join('archive_category as c','a.category_id=c.id')
			->join('user as u','u.id=apply.uid')
			->join('department as d','u.department_id=d.id')
			->select('a.*,apply.id as apply_id,u.nickname as nickname,apply.review,apply.create_time as apply_create_time,c.name as c_name,a.notary_matter_name as n_name,d.name as d_name')
			->where($where)
			->limit($limit,($page-1)*$limit)
			->order_by($order)
			->like($like)
			->get()
			->result_array();
		return $res;
	}

	/*
	 * 批量写入目录
	 * */
	public function add_archive_pdf($data){
		$res=$this->db->insert_batch('archive_attachment_pdf', $data);
		return $res;
	}

	/*
	 * 获取一条pdf
	 * */
	public function get_archive_pdf_one($where,$select="*"){
		$res=$this->db->where($where)->select($select)->get('archive_attachment_pdf')->row_array();
		return $res;
	}
	/*
	 * 写入目录
	 * */
	public function add_archive_pdf_one($data){
		$res=$this->db->insert('archive_attachment_pdf', $data);
		return $res;
	}

	public function update_archive_pdf_one($where,$data){
		$res=$this->db->where($where)->update('archive_attachment_pdf',$data);
		return $res;
	}

	/*
	 * 写入案卷
	 * */
	public function add_archive_one($data){
		$res=$this->db->insert('archive', $data);
		return $res;
	}

	/*
	 * 写入案卷
	 * */
	public function update_archive_one($where,$data){
		$res=$this->db->where($where)->update('archive', $data);
		return $res;
	}

	/*
	 * 删除目录
	 * */
	public function delete_archive_pdf($where){
		$res=$this->db->where($where)->delete('archive_attachment_pdf');
		return $res;
	}
	/*
	 * 删除案卷
	 * */
	public function delete_archive($where){
		$res=$this->db->where($where)->delete('archive');
		return $res;
	}


}
?>
