<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class Archive_info_model extends  Db_model
{

  	function __construct(){
  		parent::__construct();
	}

	 public function get_archive_info_limit_json($condition=""){
		$page 		= isset($_REQUEST['page']) 			? intval($_REQUEST['page']) 	: 1;
		$rows		= isset($_REQUEST['pagerows']) 		? intval($_REQUEST['pagerows']) : 20;
		$sort 		= isset($_REQUEST['sort']) 			? strval($_REQUEST['sort']) 	: "create_time";
		$order 		= isset($_REQUEST['order']) 		? strval($_REQUEST['order']) 	: 'desc';
		$offset 	= ($page-1)*$rows;
    	$result 	= $this->get_archive_info_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->get_archive_info_rows($condition);
		$arr 	= array(
				"total"=>$total,
				"rows"=> $result
		);
        return  $arr;
	}

	public  function get_archive_info_array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('archive_info  as a');
		$sql = 'a.id,a.person_relation_id,a.related_name,a.related_card,a.related_sex,a.related_type,a.create_time,b.name as relation_name';
		$this->db->select($sql);

        if($condition){
	      		$this->db->where($condition);
        }

       	$this->db->join('person_relation as b', 'b.id = a.person_relation_id '    ,"left");
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);

		$temp 	 =  $this->db->get()->result_array();
		$result =  array();
		foreach($temp  as  $row){
			$row["create_time"]    		= date('Y-m-d H:i:s',$row["create_time"]);
		    if($row["related_type"] ==1){
					$row["related_type"]  = "是";
		    }else{
					$row["related_type"]  = "否";
		    }
		    $result[] = $row;
		}
		return $result;
	}


   public  function get_archive_info_rows($condition){
       $this->db->from('archive_info as a');
		if($condition){
			$this->db->where($condition);
		 }
       return $this->db->count_all_results();
   }

    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive_info_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "*";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("archive_info");
		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->archive_info_info = $result[0];
		}else{
			$this->archive_info_info =  array();
		}
		return $this->archive_info_info;
	}
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive_info($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("archive_info as a");
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->archive_info_info = $result[0];
		}else{
			$this->archive_info_info =  array();
		}
		return $this->archive_info_info;
	}



	public function exists() {
		if(empty($this->archive_info_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}


   public  function get_archive_info_fields(){

   	     $arr  =  array(

  						);
   		return $arr;

   }

   	function save($condition,$data){
		$this->db->where($condition);
		$this->db->set('login_count','login_count+1',false);
		if($this->db->update("archive", $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*
	 * 年度业务量统计
	 * */
	public function business_statistics(){
		$res=$this->db->select('appl_year,count(id) as total')->order_by('appl_year asc')->group_by('appl_year')->get('archive')->result_array();
		return $res;
	}

	/*
	 * 公证类别分析
	 * */
	public function notarial_category(){
		$where['c.status']=1;
		$res=$this->db->from('archive as a')
			->join('archive_archive_category as c','a.category=c.id')
			->select('c.name as category,count(a.id) as total')
			->where($where)
			->group_by('a.category')
			->order_by('c.order desc')
			->get()
			->result_array();
		return $res;
	}

	/*
     * 公证事由分析
     * */
	public function notary_matter(){
		$res=$this->db->from('notary_matter as n')
			->join('archive as a','n.id=a.notary_matter','left')
			->select('n.name,count(a.id) as total')
			->group_by('n.id')
			->order_by('n.sort desc')
			->get()
			->result_array();
		return $res;
	}

	/*
	 * 获取实时数据
	 * */
	public function real_time_data(){
		$where['c.status']=1;
		$res=$this->db->from('archive as a')
			->join('archive_archive_category as c','c.id=a.category')
			->select('c.id,c.name,count(a.id) as total,sum(a.pdf_num) as pdf_num')
			->group_by('c.id')
			->order_by('c.order asc')//此处采用升序排列,页面输出时会自动反转
			->where($where)
			->get()
			->result_array();
		return $res;
	}
}
?>
