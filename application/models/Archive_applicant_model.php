<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/8
 * Time: 16:46
 */
class Archive_applicant_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    /*
     * 获取查询申请
     * */
    public function get_archive_applicant_detail($condition){
        $res=$this->db->where($condition)->get('archive_applicant')->row_array();
        return $res;
    }

    /*
     * 提交查阅申请
     * */
    public function sub_applicant($where,$data){
        //查询是否提交过请求
        $res=$this->db->where($where)->update('archive_applicant',$data);
        if($res===FALSE){
            return false;
        }elseif($this->db->affected_rows()==0){
            //更新失败,写入
            $r=$this->db->insert('archive_applicant',$data);
            if($r){
                return $this->db->insert_id();
            }
        }else{
            $r=$this->db->where($where)->get('archive_applicant')->row_array();
            return $r['id'];
        }
        return false;
    }

    /*
     * 操作查阅申请
     * */
    public function update_applicat($condition,$data){
        $res=$this->db->where($condition)->update('archive_applicant',$data);
        return $res;
    }

    /*
     * 获取档案归属的主任uid
     * */
    public function get_manage_uid($archive_id){
        $where['a.id']=$archive_id;
        $where['ga.group_id']=2;
        $res=$this->db
            ->from('user as u')
            ->join('archive as a','u.department_id=a.department_id')
            ->join('auth_group_access as ga','ga.uid=u.id')
            ->where($where)
            ->select('u.id')
            ->get()
            ->result_array();
        return $res;
    }
}