<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-05
 *
 */
class Table_model extends CI_Model
{
	function __construct(){
  		parent::__construct();
	}

	/**
	 * @param 功能 获取JSON数据,用于easyui datagrid
	 * @param array    	$condition
	 * @param id  排序字段，$type,返回类型
	 * @return json或数组
	 */
	public function get_table_limit_json($table,$condition="",$id="id",$type="json"){
		$page 	= isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows 	= isset($_POST['rows']) ? intval($_POST['rows']) : 20;
		$sort 	= isset($_POST['sort']) ? strval($_POST['sort']) : $id;
		$order 	= isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$offset = ($page-1)*$rows;
		$temp   = $this->get_array_limit($table,$offset,$rows,$sort,$order,$condition);
		$total  = $this->get_rows($table,$condition);
		$result = array(
				"total"	=> $total,
				"rows"	=> $temp
		);
	   if($type == "json") {
			$json_str = json_encode($result);
			return $json_str;
	   }else{
	   		return $result;
	   }

	}



    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	function get_array_one($table,$condition ="",$fields="") {
        if($condition){
		    $this->db->where($condition);
        }
		if($fields != "" ){
			$this->db->select($fields);
		}
        $this->db->limit(1,0);
		$this->db->from($table);
		$query = $this->db->get();
		$result = $query->result_array();
		if(!empty($result)) {
        	return $result[0];
		}else{
			return array();
		}
	}

    /**
	 * @功能  获取多记录集
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @param $order 排序字段 $sort 排序方式
	 * @param $fields 以逗号分隔的字段 id，name
	 * @return 多记录数组
	 */
	function get_array($table,$condition ="",$fields="",$sort="",$order ="desc") {
        if($condition){
		    $this->db->where($condition);
        }
        if($fields != "" ){
       	 	$this->db->select($fields);
        }
        if($sort != "" ){
       		$this->db->order_by($sort,$order);
        }
		$this->db->from($table);
		$query = $this->db->get();
        return $query->result_array();
	}

   /**
	 * @功能  获取记录集
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @param $start ,$length,limit 参数
	 * @param $sort,$order，排序参数
	 * @return 记录集
	 */
   function get_array_limit($table,$start,$length,$sort,$order,$condition="")
	{
		$this->db->from($table);
		$this->db->select('*');
	    if($condition){
		   $this->db->where($condition);
        }
		$this->db->limit($length,$start);
		$this->db->order_by($sort,$order);
		$query=$this->db->get();
		return  $query->result_array();
	}



    /**
	 * @功能  获取记录集
	 * @param $table 表名
	 * @param $condition where_in条件数组
	 * @return 记录集
	 */

	function get_array_where_in($table,$field="",$condition ="",$order ="id") {
		if($condition){
			$this->db->where_in($field, $condition);
		}
		$this->db->order_by($order,'asc');
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result_array();
	}

     /**
	 * @功能  获取记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 记录数
	 */

	function get_rows($table,$condition = ""){
		if ($condition)
			$this->db->where($condition);
		return $this->db->count_all_results($table);
	}


	 /**
	 * @功能  保存记录
	 * @param $table 表名
	 * @param $data 数据数组
	 * @return 布尔值
	 */

	function save($table,$condition,$data){
		$this->db->where($condition);
		if($this->db->update($table, $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	 /**
	 * @功能  添加记录
	 * @param $table 表名
	 * @param $data 数据数组
	 * @return 成功返回id,失败返回FALSE
	 */

   function add($table,$data)
	{
		if($this->db->insert($table, $data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

   	/**
	 * @功能  删除记录
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 布尔值
	 */
	function del($table,$condition){
		$this->db->where($condition);
		if($this->db->delete($table)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * 插入单条记录(Insert)
	 *
	 * @param array $params 插入字段及值数组
	 * @param string $table 插入表名，未指定的场景插入主表
	 * @param array $columns 插入表中所属字段名数组，若指定则按此进行过滤
	 * @param array $updateColumns 若唯一键冲突时需要更新的列数组
	 * @return mixed FALSE:没有插入的字段 int:插入记录id
	 *
	 * @throws \C_DB_Exception
	 */
	public function insert($params, $table = NULL, $columns = array(), $updateColumns = array())
	{
		// 如果未指定表名，则认为对本Model层的主表进行操作
		if (empty($table)) {
			$table = $this->table;
			$columns = $this->columns;
		}
		// 过滤掉不属于该表中的字段
		if ( ! empty($columns)) {
			foreach ($params as $column => $val) {
				if (in_array($column, $columns)) {
					$row[$column] = $val;
				}
			}
		}
		// 不过滤
		else {
			$row = $params;
		}
		// 没有插入字段，直接返回
		if ( ! isset($row)) {
			return FALSE;
		}

		// 封装SQL文，过滤不安全因素
		$sql = $this->db->insert_string($table, $row);
		// 若唯一键冲突时需要更新的列数组，则再次封装SQL文
		if ( ! empty($updateColumns)) {
			$updateValues = '';
			foreach ($updateColumns as $updateColumn) {
				if (isset($updateValues[0])) {
					$updateValues .= ',';
				}
				$updateValues .= $updateColumn . '=VALUES(' . $updateColumn . ')';
			}
			$sql = $sql . ' ON DUPLICATE KEY UPDATE ' . $updateValues;
		}
		// SQL执行
		$query = $this->db->query($sql);
		// SQL执行失败，抛出异常
		if (FALSE === $query) {
			$code = $this->db->_error_number();
			$msg = $this->db->_error_message();
			C_log::Error($code . ':' . $msg, $this->user_id, $sql, self::STATUS_DB_LOG);
			throw new C_DB_Exception('', C_DB_Exception::DB_ERR_CODE);
		}
		// SQL文写入DB日志
		if (LOG_UPDATE_SQL) {
			C_log::Info('', $this->user_id, $sql, self::STATUS_DB_LOG);
		}
		// 取得插入ID返回
		$id = $this->db->insert_id();
		return $id;
	}

}
?>
