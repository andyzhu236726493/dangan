<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_group
 * @Auth Andy zhu
 * @Create 2015-12-10
 *
 */
class Auth_group_model extends  Db_model
{

  private  $id 			= 0;
  private  $user_info 	= array();

  function __construct(){
  		parent::__construct();
	}



  public  function get_auth_group_array_limit($offset,$rows,$sort,$order,$condition)
  {
		$this->db->from('auth_group as a');
		$sql = 'a.id,a.title,a.pid,a.company_id,b.name as company_name,status';

		$this->db->select($sql);
        if($condition){
	              $this->db->where($condition);
        }

        $this->db->where("a.id !=",1);
        if($this->group_id != 1) {
        	$this->db->where("a.company_id=",$this->company_id);
        }
        $this->db->join('department as b', 'b.id = a.company_id '    ,"left");
		$this->db->order_by($sort,$order);
		$temp    =  $this->db->get()->result_array();
		foreach($temp as $row) {
			  if($row["status"] ==1) {
			  	$row["status"] = "启用";
			  }else if($row["status"] ==0){
			  	$row["status"]	="禁用";
			  }
			$result[] = $row;
		}
		return $result;
  }


  public  function get_auth_group_rows($condition){
       $this->db->from('auth_group as a');
	   if($condition){
 			$this->db->where($condition);
		}
       return $this->db->count_all_results();
   }

  public   function get_auth_group_json() {

			$fields 	= "id,title";
			$department	= $this->table_model->get_array("auth_group",$condition="",$fields);
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["title"];
				$arr[]				= $temp;
			}
			$json_str 	= json_encode($arr);
			return   $json_str;

	}

	 public  function get_auth_group_fields(){
	   	     $user_array  =  array(
	  							"id"				=>"",
	  							"title"				=>""
	  						);
	   		return $user_array;
   }
}
?>
