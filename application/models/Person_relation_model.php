<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class Person_relation_model extends  Db_model
{

  	function __construct(){
  		parent::__construct();
	}

	 public function get_person_relation_tree_limit_json($condition=""){
		$page 		= isset($_REQUEST['page']) 			? intval($_REQUEST['page']) 	: 1;
		$rows		= isset($_REQUEST['pagerows']) 		? intval($_REQUEST['pagerows']) : 100;
		$sort 		= isset($_REQUEST['sort']) 			? strval($_REQUEST['sort']) 	: "id";
		$order 		= isset($_REQUEST['order']) 		? strval($_REQUEST['order']) 	: 'asc';
		$offset 	= ($page-1)*$rows;
    	$temp_arr 	= $this->get_person_relation_array_limit($offset,$rows,$sort,$order,$condition);
    	$total 		= $this->get_person_relation_rows($condition);

		$result     =  array();
		$arr 		=  array();
		foreach($temp_arr as $row){
			 $arr	=   array("id"=>$row["id"],"name"=>$row["name"],"field"=>$row["field"]);
			 if( $row["pid"] != 0) {
				$arr["_parentId"]  = $row["pid"];
			  }
			$result[]		= $arr;
		}
		$temp 	= array(
				"total"=>$total,
				"rows"=> $result
		);
	    return  $temp;
	}

	public  function get_person_relation_array_limit($offset,$rows,$sort,$order,$condition)
	{
		$this->db->from('person_relation as a');
		$sql = 'a.*';
		$this->db->select($sql);

        if($condition){
	        foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
        }
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$result =   $this->db->get()->result_array();
        return $result;
	}



	public  function get_person_relation_rows($condition){
       $this->db->from('person_relation as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
       return $this->db->count_all_results();
   }
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_person_relation_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "a.*";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("person_relation as a");
		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->person_relation_info = $result[0];
		}else{
			$this->person_relation_info =  array();
		}
		return $this->person_relation_info;
	}



    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_person_relation($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("person_relation as a");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->person_relation_info = $result[0];
		}else{
			$this->person_relation_info =  array();
		}
		return $this->person_relation_info;
	}



	public function exists() {
		if(empty($this->person_relation_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function get_person_relation_json() {

			$fields 	= "id,name";
			$department	= $this->table_model->get_array("person_relation");
			$arr    	= array();
			$temp   	= array();
			foreach($department  as $row){
				$temp["id"] 		= $row["id"];
				$temp["text"] 		= $row["name"];
				$arr[]				= $temp;
			}

			$json_str 	= json_encode($arr);
			return   $json_str;

	}



   public  function get_person_relation_fields(){
   	     $person_relation_array  =  array(
  							"id"				=>"",
  						);
   		return $person_relation_array;
   }



   public  function get_person_fields(){
   	     $person_relation_array  =  array(
  							"fullname"		=>"姓名",
  							"sex"			=>"性别",
  							"id_card"		=>"身份证号",
  							"birthdate"		=>"出生日期",
  							"bealive"		=>"在世"
  						);
   		return $person_relation_array;

   }


}
?>
