<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auth_model
 * @Auth Andy zhu
 * @Create 2015-05-04
 *
 */
class Archive_attachment_model extends  Db_model
{

  	function __construct(){
  		parent::__construct();
	}

	public  function get_archive_attachment_limit_json($archive_id,$rows,$page)
	{
		$fields 			= $this->get_archive_attachment_fields();
		$condition 		= array("archive_id" => $archive_id);
		$attachment 	= $this->get_archive_attachment_info($condition);
		$result 		=  array();

		foreach($fields  as $key=>$row) {
			if(!empty($attachment)) {
				$result[] 	=  array("url" =>$attachment[$key],"name"=>$key,"title"=>$row);
			}else{
				$result[] 	=  array("url" =>"","name"=>$key,"title"=>$row);
			}
		}
		$arr 		= array(
			    			"total"=> count($fields),
			    			"rows" => $result
			    		);
		return $arr;

	}


	public  function get_archive_attachment_rows($condition){
       $this->db->from('archive_attachment as a');
		if($condition){
			foreach($condition   as $key=>$row){
	        	if($key == "where"){
	        		$this->db->where($row);
	        	}
	        	if($key == "like"){
					$this->db->like($row);
	        	}
	        }
		 }
       return $this->db->count_all_results();
   }
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive_attachment_info($condition ="") {
		if($condition != "" ){
			$this->db->where($condition);
		}
		$fields =  "*";
		$this->db->select($fields);

        $this->db->limit(1,0);
		$this->db->from("archive_attachment");
		$result = $this->db->get()->result_array();

		if(!empty($result)) {
        	$this->archive_attachment_info = $result[0];
		}else{
			$this->archive_attachment_info =  array();
		}
		return $this->archive_attachment_info;
	}
    /**
	 * @功能  获取单条记录数
	 * @param $table 表名
	 * @param $condition 条件数组
	 * @return 单记录数组
	 */
	public function get_archive_attachment($account ="",$password ="") {

	    $where = "a.password= '".$password."' AND ( a.account = '".$account."' OR a.tel ='".$account."')";
		$this->db->where($where);
		$fields =  "a.*,b.group_id,c.rules";
		$this->db->select($fields);
        $this->db->limit(1,0);
		$this->db->from("archive_attachment as a");
		$this->db->join('auth_group_access as b', 'b.uid = a.id '    ,"left");
		$this->db->join('auth_group as c', 'c.id = b.group_id '    ,"left");
		$result = $this->db->get()->result_array();
		if(!empty($result)) {
        	$this->archive_attachment_info = $result[0];
		}else{
			$this->archive_attachment_info =  array();
		}
		return $this->archive_attachment_info;
	}



	public function exists() {
		if(empty($this->archive_attachment_info)){
			return FALSE;
		}else{
			return TRUE;
		}
	}


   public  function get_archive_attachment_fields(){

   	     $arr  =  array(
			            'cover'							=>'封面',
			            'directory'						=>'目录',
			            'notary_content'				=>'公证书正文',
			            'notary_draft'					=>'公证书签发稿',
			            'notary_appform'				=>'公证书申请表',
			            'notary_notice'					=>'公证受理通知书',
			        	'notification'					=>'告知书',
			            'litigant_card'					=>'当事人的身份证明',
			            'litigant_prove'				=>'当事人提供的证明材料',
			            'conversation_record'			=>'谈话笔录',
			            'notary_sent_back'				=>'公证书送达回证',
			            'notary_fee_receipt'			=>'公证收费收据',
			            'reference_table'				=>'备考表',
			            'norary_translation'			=>'公证书译文',
			            'review_material'				=>'审查,调查所取得证明材料',
			            'audit_table'					=>'发证审核表',
			            'litigant_apply_prove_original'	=>'当事人申请证明文书材料原件',
			            'attachment_bag'				=>'附件袋',
			            'other_material'				=>'其它材料'
  						);
   		return $arr;

   }

   	function save($condition,$data){
		$this->db->where($condition);
		$this->db->set('login_count','login_count+1',false);
		if($this->db->update("archive", $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*
	 * 年度业务量统计
	 * */
	public function business_statistics(){
		$res=$this->db->select('appl_year,count(id) as total')->order_by('appl_year asc')->group_by('appl_year')->get('archive')->result_array();
		return $res;
	}

	/*
	 * 公证类别分析
	 * */
	public function notarial_category(){
		$where['c.status']=1;
		$res=$this->db->from('archive as a')
			->join('archive_archive_category as c','a.category=c.id')
			->select('c.name as category,count(a.id) as total')
			->where($where)
			->group_by('a.category')
			->order_by('c.order desc')
			->get()
			->result_array();
		return $res;
	}

	/*
     * 公证事由分析
     * */
	public function notary_matter(){
		$res=$this->db->from('notary_matter as n')
			->join('archive as a','n.id=a.notary_matter','left')
			->select('n.name,count(a.id) as total')
			->group_by('n.id')
			->order_by('n.sort desc')
			->get()
			->result_array();
		return $res;
	}

	/*
	 * 获取实时数据
	 * */
	public function real_time_data(){
		$where['c.status']=1;
		$res=$this->db->from('archive as a')
			->join('archive_archive_category as c','c.id=a.category')
			->select('c.id,c.name,count(a.id) as total,sum(a.pdf_num) as pdf_num')
			->group_by('c.id')
			->order_by('c.order asc')//此处采用升序排列,页面输出时会自动反转
			->where($where)
			->get()
			->result_array();
		return $res;
	}

	/*
	 * 获取档案pdf详情
	 * */
	public function get_attachment_detail($archive_id,$select=''){
		$where['archive_id']=$archive_id;
		$this->db->where($where);
		if($select!=""){
			$this->db->select($select);
		}
		$res=$this->db->get('archive_attachment')->row_array();
		return $res;
	}
}
?>
