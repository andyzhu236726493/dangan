<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/18
 * Time: 14:55
 */
class Customer_model extends MY_Model{
    public function __construct(){
        parent::__construct();
    }


    public  function get_customer_array_limit($offset,$rows,$sort,$order,$condition)
    {
        $this->db->from('customer as a');
        $sql = 'a.*';
        $this->db->select($sql);

        if($condition){
            foreach($condition   as $key=>$row){
                if($key == "where"){
                    $this->db->where($row);
                }
                if($key == "like"){
                    $this->db->like($row);
                }
            }
        }
        $this->db->limit($rows,$offset);
        $this->db->order_by($sort,$order);
        $temp =   $this->db->get()->result_array();

        foreach($temp as $row){
            $result[]		= $row;
        }
        return $result;
    }

    public  function get_customer_rows($condition){
        $this->db->from('customer as a');
        if($condition){
            foreach($condition   as $key=>$row){
                if($key == "where"){
                    $this->db->where($row);
                }
                if($key == "like"){
                    $this->db->like($row);
                }
            }
        }
        return $this->db->count_all_results();
    }



    public  function get_archive_array_limit($offset,$rows,$sort,$order,$condition)
    {
        $this->db->from('archive as a');
//		$sql = 'a.*,b.name  as category_name,c.name  as notary_matter_name';
        $sql = 'a.*,b.name c_name,a.notary_matter_name n_name,d.name d_name,b.name  as category_name,a.notary_matter_name  as notary_matter_name';
        $this->db->select($sql);

        if($condition){
            foreach($condition   as $key=>$row){
                if($key == "where"){
                    $this->db->where($row);
                }
                if($key == "like"){
                    $this->db->like($row);
                }
            }
            if($key == "wherein"){
                $this->db->where_in('a.id', $condition["wherein"]);
            }


        }

        $this->db->join('archive_category as b', 'b.id = a.category_id '    ,"left");
        $this->db->join('department as d','a.department_id=d.id');
//        $this->db->join('notary_matter as c', 'c.id = a.notary_matter_id '    ,"left");
        $this->db->limit($rows,$offset);
        $this->db->order_by($sort,$order);
        $temp =   $this->db->get()->result_array();
        $result	=  array();
        $storage_time	= $this->get_storage_time_array();
        foreach($temp as $row){
            $row["receive_time"]  		= date("Y-m-d ",$row["receive_time"]);
            $row["archive_time"]  		= date("Y-m-d ",$row["archive_time"]);
            $row["over_time"]  			= date("Y-m-d ",$row["over_time"]);
            $row["storage_time"]  		= $storage_time[$row["storage_time_id"]];
            $row["litigant_date"]  		= date("Y-m-d ",$row["litigant_date"]);

            $row["create_time"]    		= date('Y-m-d ',$row["create_time"]);
            $row["update_time"]    		= date('Y-m-d ',$row["update_time"]);
            $result[]		= $row;
        }
        return $result;
    }


    public  function get_archive_rows($condition){
        $this->db->from('archive as a');
        if($condition){
            foreach($condition   as $key=>$row){
                if($key == "where"){
                    $this->db->where($row);
                }
                if($key == "like"){
                    $this->db->like($row);
                }
            }
        }
        return $this->db->count_all_results();
    }

    public  function get_storage_time_array(){

        $result 	= array(
            1 	=>"永久",
            2 	=>"长期",
            3	=>"短期",
        );

        return $result;
    }


}