<?php
use Workerman\Worker;
require_once '/assets/plugins/workerman/Autoloader.php';

$global_uid = 0;
@$text_worker->uidConnections=array();
@$text_worker->realtime_uidConnections=array();

// 当客户端连上来时分配uid，并保存连接，并通知所有客户端
function handle_connection($connection)
{
    global $text_worker, $global_uid;
    // 为这个链接分配一个uid
    $connection->uid = ++$global_uid;
}

// 当客户端发送消息过来时，转发给所有人
function handle_message($connection, $data)
{
    global $text_worker;
//    $data=iconv( "UTF-8", "gb2312" , $data);
    $send_data=json_decode($data,true);
//    print_r($send_data);
    switch($send_data['type']){
        case 'real_time_login':
            $text_worker->realtime_uidConnections[]=$connection;
            break;
        case 'real_time_send':
            broadcast($data);
            break;
        case 'login':
            //登陆
            $text_worker->uidConnections[$send_data['uid']][]=$connection;
            break;
        case 'apply':
            //申请查阅
            $uids=explode(',',$send_data['uids']);//要发送的uid数组
//            $return_json['department_name']=$send_data['department_name'];
            $return_json['send_message']=$send_data['department_name'].'的'.$send_data['nickname'].'发来了查阅申请。';
            $return_json['apply_id']=$send_data['apply_id'];
            $return_json['send_time']=date('Y-m-d H:i',time());
            $return_json['type']='apply';
            for($i=0;$i<count($uids);$i++){
                sendMessageByUid($uids[$i],json_encode($return_json));
            }
            break;
        case 'audition':
            //审核
            $uid=$send_data['uid'];//要发送的uid数组
            if($send_data['ispass']=='true'){
                $str='您的查阅申请已经通过审核。';
            }else{
                $str='您的查阅申请已被驳回。';
            }
            $return_json['send_message']=$str;
            $return_json['archive_id']=$send_data['archive_id'];
            $return_json['send_time']=date('Y-m-d H:i',time());
            $return_json['type']='audition';
            sendMessageByUid($uid,json_encode($return_json));
            break;
    }
}

// 当客户端断开时，广播给所有客户端
function handle_close($connection)
{
//    global $text_worker;
//    foreach($text_worker->connections as $conn)
//    {
//        $conn->send("user[{$connection->uid}] logout");
//    }
}

// 针对uid推送数据
function sendMessageByUid($uid, $message)
{
    global $text_worker;
//    print_r($text_worker->uidConnections);
    if(isset($text_worker->uidConnections[$uid]))
    {
        for($i=0;$i<count($text_worker->uidConnections[$uid]);$i++){
            $connection = $text_worker->uidConnections[$uid][$i];
            $connection->send($message);
        }
//        $connection = $text_worker->uidConnections[$uid];
//        $connection->send($message);
    }
}

//向实时数据推送数据
function broadcast($message)
{
    global $text_worker;
    foreach($text_worker->realtime_uidConnections as $connection)
    {
        $connection->send($message);
    }
}


// 创建一个文本协议的Worker监听2348接口
$text_worker = new Worker("websocket://0.0.0.0:2348");

// 只启动1个进程，这样方便客户端之间传输数据
$text_worker->count = 1;

$text_worker->onConnect = 'handle_connection';
$text_worker->onMessage = 'handle_message';
$text_worker->onClose = 'handle_close';

Worker::runAll();
?>