$(document).ready( function (){
	//定位默认窗口


	var appheight = $(window).height() - 105;

	var aWi = $(window).width();
	var lWi = $(".left_box").width();
	var appWi =aWi - lWi - 1;
	var navleft = ($(window).width() -$(".dialog_box").width())/2;
	var navtop = ($(window).height() -$(".dialog_box").height())/2 + 20;
	$('.autoheight').css({'height':appheight});
	$('.autowidth').css({'width':appWi});
	$('.dialog_box').css({"left":navleft,'top':navtop});
});

$(window).resize(function() {
	//随窗口变化而变化


	var appheight = $(window).height() - 105;

	var aWi = $(window).width();
	var lWi = $(".left_box").width();
	var appWi =aWi - lWi - 1;
	var navleft = ($(window).width() -$(".dialog_box").width())/2;
	var navtop = ($(window).height() -$(".dialog_box").height())/2 + 20;
	$('.autoheight').css({'height':appheight});
	$('.autowidth').css({'width':appWi});
	$('.dialog_box').css({"left":navleft,'top':navtop});
});


$(document).ready( function (){
	// 左侧菜单移动效果
	$(".left_nav li").hover(
		function(){
			$(this).addClass("hover-bg");
		},
		function(){
			$(this).removeClass("hover-bg");
	});
	
	// 左侧菜单选项卡效果
	$("ul.sub_list li span").click(function(){
		$("ul.sub_list li span").filter(".current").removeClass("current");
		$(this).addClass("current");
	});
	
	$(".item_list a").click(function(){
		$(".item_list a").filter(".current").removeClass("current");
		$(this).addClass("current");
	});
	

	$('.left_nav .l_nav').click(function(){
		if($(this).children('.sub_box')){
			$(this).addClass('current').siblings().removeClass('current');
		}
		$(this).addClass('current').siblings().removeClass('current');
		$(this).children('.sub_box').slideDown().end().siblings().children('.sub_box').slideUp();
	});
	
	$('.sub_list li').click(function(){
		if($(this).children('.item_list')){
			$(this).addClass('current1').siblings().removeClass('current1');
		}
		$(this).addClass('current1').siblings().removeClass('current1');
		$('.item_list').hide();
		$(this).children('.item_list').slideDown(0).end().siblings().children('.item_list').slideUp();
	});
	
	
	// 表格隔行换色效果
	$(".tab_box tr").each(function(i) {  
         var className = ["odd", "even"][i % 2];  
         $(this).addClass(className);  
         $(this).hover(  
             function() {  
                 $(this).removeClass(className);  
                 $(this).addClass("highlight");  
            },  
             function() {  
                 $(this).removeClass("hightlight");  
                 $(this).addClass(className);  
             }  
         )  
    }); 
	
	// 文本点击获取焦点效果
	$(".input,textarea").focus(function(){
			  $(this).addClass("focus");
			  if($(this).val() ==this.defaultValue){  
                  $(this).val("");           
			  } 
		}).blur(function(){
			 $(this).removeClass("focus");
			 if ($(this).val() == '') {
                $(this).val(this.defaultValue);
             }
	});
	// 按钮移动效果
	$(".message_top input").hover(
		function(){
			$(this).addClass("hover-bg");
		},
		function(){
			$(this).removeClass("hover-bg");
		}
	);
	

	$('.add').click(function () {		
		$('.dialog_box').show();	
	});
	
	$('.close,.button').click(function () {		
		$('.dialog_box').hide();	
		return false;
	});
	
	// 回首页、最小化、关闭按下效果
	$('.window_box a').bind("mousedown",function(){
		$(this).addClass("press_bg");
	});
	$('.window_box a').bind("mouseup",function(){
		$(this).removeClass("press_bg");
	});
	
});



