/*
* arr 二维数组
* 二维数组第一个字段是要验证的id,第二个字段是要验证的类型,如textbox,numberbox
* */
function validate_arr(arr){
	for(var i=0;i<arr.length;i++){
		switch (arr[i][1]){
			case 'numberbox':
				if(!$('#'+arr[i][0]).numberbox('enableValidation').numberbox('isValid')){
					$('#'+arr[i][0]).siblings("span").find("input[type='text']").focus();
					return false;
				}
				break;
			case 'textbox':
				if(!$('#'+arr[i][0]).textbox('enableValidation').textbox('isValid')){
					$('#'+arr[i][0]).textbox("textbox").focus();
					return false;
				}
				break;
			case 'combobox':
				if(!$('#'+arr[i][0]).combobox('enableValidation').combobox('isValid')){
					return false;
				}
				break;
			case 'timespinner':
				if(!$('#'+arr[i][0]).timespinner('enableValidation').timespinner('isValid')){
					$('#'+arr[i][0]).next("span").find("input[type='text']").focus();
					return false;
				}
				break;
			case 'numberspinner':
				if(!$('#'+arr[i][0]).numberspinner('enableValidation').numberspinner('isValid')){
					$('#'+arr[i][0]).siblings("span").find("input[type='text']").focus();
					return false;
				}
				break;
			case 'datetimebox':
				if(!$('#'+arr[i][0]).datetimebox('enableValidation').datetimebox('isValid')){
					//$('#'+arr[i][0]).datetimebox('spinner').focus();
					return false;
				}
				break;
		}
	}
	return true;
}
function show_msg(msg){
 			$.messager.show({
				title:'友情提示',
				msg: msg,
				showType:'show',
				timeout:3000,
				style:{
					right:'',
					top:document.body.scrollTop+document.documentElement.scrollTop,
					bottom:''
				}
			});

}


$.extend($.fn.validatebox.defaults.rules, {
	   //移动手机号码验证
	    mobile: {//value值为文本框中的值
	        validator: function (value) {
	            var reg = /^1\d{10}$/;
	            return reg.test(value);
	        },
	        message: '输入手机号码格式不准确.'
	    },
	    tel: {
	        validator: function(value){
	        var rex=/^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
	        if(rex.test(value))
	        {
	          return true;
	        }else
	        {
	           return false;
	        }

	        },
	        message: '请输入正确电话格式 区号-电话，例子 0512-67682588'
	     },
	     //移动手机号码是否已使用验证
 	 	checkphone : {
 		 	validator : function(value, param) {
			  			var reg = /^1\d{10}$/;
	            		if(!reg.test(value)) {
	            			$.fn.validatebox.defaults.rules.checkphone.message = '输入手机号码格式不准确.';
			            	return false;
	            		}else{
	            			 var bol  = true;
							 var name = value;
							 if(name != $("#oldphone").val()) {
							 	$.ajax({
								   type: "POST",
								   async:false,
								   url:checkphone_url,
								   dataType:"text",
								   data:{phone:name},
								   success: function(msg){
									  if(msg =="ok"){     //账号可用
										  bol= true;
									  }else{
										  bol= false;
									  }
								    }
								});
							 }
			  				if(bol){
			  					return true;
			            	}else{
			            		$.fn.validatebox.defaults.rules.checkphone.message = checkphone_name+'已经被使用，请重新输入或联系管理员';
			            		return false;
			            	}
	            		}
  	    		},
 		    message : ''
 	  },
	    //国内邮编验证
	    zipcode: {
	        validator: function (value) {
	            var reg = /^[1-9]\d{5}$/;
	            return reg.test(value);
	        },
	        message: '邮编必须是非0开始的6位数字.'
	    },
		gtdate : { validator : function(value, param) {
			return value >= $(param[0]).datebox('getValue');
		},
			message : '结束日期要大于等于开始日期！'
		},
		gttime:{
			validator:function(value,param){
				return value>=$(param[0]).timespinner('getValue');
			},
			message:'结束时间要大于等于开始时间'
		},
		rule_gttime:{
			validator:function(value,param){
				return value>=$(param[0]).timespinner('getValue');
			},
			message:'下班最晚签到时间要大于上班时间'
		},
		rule_lttime:{
			validator:function(value,param){
				return value<=$(param[0]).timespinner('getValue');
			},
			message:'上班最早签到时间要小于上班时间'
		},
		ltdate : { validator : function(value, param) {
			return value < $(param[0]).datebox('getValue');
		},
			message : '开始日期不能大于结束日期！'
		},
	    sort: {
	    	        validator: function (value) {
	    	            var reg = /^[0-9]\d{0,9}$/;
	    	            return reg.test(value);
	    	        },
	    	        message: '排序必须是1到10位数字,数字越大越靠前.'
	    },
	    number: {
	    	        validator: function (value) {
	    	            var reg = /^[0-9]\d{0,9}$/;
	    	            return reg.test(value);
	    	        },
	    	        message: '必须是1到10位数字.'
	    },
	    price: {
	    	        validator: function (value) {
	    	            var reg = /^[0-9]\d{0,9}$/;
	    	            return reg.test(value);
	    	        },
	    	        message: '价格排序必须是1到10位数字.'
	    },

	    address_error: {
	    	        validator: function (value) {
	    	            var reg = /^[0-9]\d{0,2}$/;
	    	            return reg.test(value);
	    	        },
	    	        message: '误差范围必须是1到3位的数字.'
	    },
	    eqPassword:{
			validator:function(value,param){
				return value == $(param[0]).val();
			},
			message:'两次输入的密码不相同'
		},
	    checkname : {
		    	validator : function(value, param) {
				if (value.length < param[0] || value.length > param[1]) {
					$.fn.validatebox.defaults.rules.checkname.message = '该'+checkname_name+'长度必须在' + param[0] + '至' + param[1] + '范围';
	       		return false;
				}else {
					 var bol  = true;
					 var name = value;
					 if(name != $("#oldname").val()) {
						 $.ajax({
							   type: "POST",
							   async:false,
							   url:checkname_url,
							   dataType:"text",
							   data:{name:name},
							   success: function(msg){
								  if(msg =="ok"){
										  bol= true;
								  }else{
										  bol= false;   //已经被使用
								  }
							    }
							});
					 }
					 if(bol){
								return true;
			         }else{
			         		$.fn.validatebox.defaults.rules.checkname.message = '该'+checkname_name+'已经被使用，请重新输入';
			         		return false;
			         }
				  }
		    	},
	   		   message : ''
	    },
 	 checkaccount : {
 		 	validator : function(value, param) {
			  			if (value.length < param[0] || value.length > param[1]) {
			  				$.fn.validatebox.defaults.rules.checkaccount.message = checkaccount_name+'长度必须在' + param[0] + '至' + param[1] + '范围';
                		return false;
			  			//}else if (!/^[\w]+$/.test(value)) {
			                 //$.fn.validatebox.defaults.rules.checkaccount.message = checkaccount_name+'只能数字、字母、下划线组成.';
			                 //return false;
			  			}else {
							 var bol  = true;
							 var name = value;
							 if(name != $("#oldaccount").val()) {
							 	$.ajax({
								   type: "POST",
								   async:false,
								   url:checkaccount_url,
								   dataType:"text",
								   data:{name:name},
								   success: function(msg){
									  if(msg =="ok"){
										  bol= true;
									  }else{
										  bol= false;  //已经被使用
									  }
								    }
								});
							 }

			  				if(bol){
			  					return true;
			            	}else{
			            		$.fn.validatebox.defaults.rules.checkaccount.message = checkaccount_name+'已经被使用，请重新输入';
			            		return false;
			            	}
			  			}
  	    		},
 		    message : ''
 	  }
});