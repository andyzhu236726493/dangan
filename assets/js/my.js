/**
 * Created by Administrator on 2016/11/7.
 */
/*
* 根据时间戳格式化输出时间
* */
function time_format(time){
    var times = new Date(time*1000);
    var y=times.getFullYear();    //获取完整的年份(4位,1970-????)
    var m=times.getMonth()+1<10?"0"+(times.getMonth()+1):times.getMonth()+1;
    var d=times.getDate()<10?'0'+times.getDate():times.getDate();
    var h=times.getHours()<10?'0'+times.getHours().toString():times.getHours();
    var i=times.getMinutes()<10?'0'+times.getMinutes().toString():times.getMinutes();
    var s=times.getSeconds()<10?'0'+times.getSeconds().toString():times.getSeconds();
    return y+'-'+m+'-'+d+' '+h+':'+i+':'+s;
}